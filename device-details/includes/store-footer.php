 <div id="footer">
        <div class="footerchannel">
            <div id="footer_channel">
                <div class="itpgshadow" id="itpg">
                    <a class="close-x icon-cancel" href=
                    "javascript:itpSend('cancel')" style=
                    "font-style: italic"></a>

                    <h2 class="pink block"><span>Help us improve this
                    page</span></h2>

                    <form action="" id="itpForm" name="itpForm">
                        <div id="itpgcont">
                            <h4>How could we make this page better?</h4>

                            <p id="itpMax">1000 characters</p>
                            <textarea cols="65" id="itpMsg" name="itpMsg"
                            onkeyup="itpCounter()" placeholder=
                            "Please share your thoughts here..." rows="7">
</textarea> <a class="button grey fit margin-top"
                            href="javascript:itpSend('cancel')">Cancel</a>
                            <a class="button blue float-right margin-top" href=
                            "javascript:itpSend('submit')">send message</a>
                        </div>
                    </form>
                </div>

                <div id="mask"></div>

                <div class="help">
                    <a href="http://www.telecom.co.nz/rating/na/" id="itpLink"
                    onclick="itpg();return false"><i class="icon-chat"></i>Help
                    improve this page</a> <a href=
                    "javascript:window.print()"><i class=
                    "icon-doc-text-inv"></i>Print this page</a> <a href="#"
                    onclick="emailPage()" rel="popup"><i class=
                    "icon-mail-alt"></i>Email this page</a>
                </div><a class="top" href="#top"><i class=
                "icon-angle-circled-up"></i> Back to top</a>
            </div>
        </div>

        <div class="footerbreadcrumb">
            <div class="fullwidth-bg">
                <ul class="breadcrumb">
                    <li>
                        <a href="/home/">Personal</a>

                        <div class="arrow"></div>
                    </li>

                    <li>
                        <a href="/shop/">Shop</a>

                        <div class="arrow"></div>
                    </li>

                    <li>
                        <a href="/shop/mobile.html">Mobile</a>

                        <div class="arrow"></div>
                    </li>

                    <li>
                        <a href="/shop/mobile/phones.html">Phones</a>

                        <div class="arrow"></div>
                    </li>

                    <li>
                        <a href="">Samsung Galaxy S6 Edge 32GB Black
                        Sapphire</a>

                        <div class="arrow"></div>
                    </li>
                </ul>
            </div>
        </div>

        <div class="fullwidth-bg off-white">
            <div class="nav">
                <div class="footernav">
                    <div class='col'>
                        <h5><a href='/shop/'>Shop</a></h5>

                        <ul>
                            <li>
                                <a href="/shop/mobile.html">Mobile</a>
                            </li>

                            <li>
                                <a href="/shop/internet/">Internet</a>
                            </li>

                            <li>
                                <a href="/shop/landline/">Landline</a>
                            </li>
                        </ul>
                    </div>

                    <div class='col'>
                        <h5><a href='/discover/'>Discover</a></h5>

                        <ul>
                            <li>
                                <a href="/discover/samsunggalaxys5/">Samsung
                                Galaxy S5</a>
                            </li>

                            <li>
                                <a href="/discover/ultrabroadband/">Ultra
                                Broadband</a>
                            </li>

                            <li>
                                <a href="/discover/ultramobile/">Ultra
                                Mobile</a>
                            </li>

                            <li>
                                <a href=
                                "/discover/spotifymusicstreaming/">Spotify
                                Premium</a>
                            </li>

                            <li>
                                <a href="/discover/cutandpaste/">Cut &
                                Paste</a>
                            </li>
                        </ul>
                    </div>

                    <div class='col'>
                        <h5><a href='/help/'>Help</a></h5>

                        <ul>
                            <li>
                                <a href="/help/techinasec/">Tech in a sec</a>
                            </li>

                            <li>
                                <a href="/business/help/">Internet</a>
                            </li>

                            <li>
                                <a href="/business/help/">Mobile</a>
                            </li>

                            <li>
                                <a href="/help/landline/">Landline</a>
                            </li>

                            <li>
                                <a href=
                                "http://helpbusiness.spark.co.nz/app/answers/detail/a_id/22362">
                                Web Services</a>
                            </li>
                        </ul>
                    </div>

                    <div class='col'>
                        <h5><a href='/myspark/'>MySpark</a></h5>

                        <ul>
                            <li>
                                <a href="/myspark/">Sign in</a>
                            </li>

                            <li>
                                <a href="/myspark/">Register</a>
                            </li>

                            <li>
                                <a href="/myspark/mymobile/">My Mobile</a>
                            </li>

                            <li>
                                <a href="/myspark/mymobilebroadband/">My Mobile
                                Broadband</a>
                            </li>

                            <li>
                                <a href="/myspark/myinternet/">My Internet</a>
                            </li>

                            <li>
                                <a href="/myspark/mylandline/">My Landline</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="socialmedialinks">
                    <div class='col contact'>
                        <h5><a href="/contactus/">Contact</a></h5>

                        <p><a href="/contactus/storefinder/">Store
                        Finder</a></p>

                        <div class="footer-social">
                            <h5>Follow us</h5>

                            <ul>
                                <li class="social">
                                    <a class="icon-facebook-squared" href=
                                    "https://www.facebook.com/spark4nz" style=
                                    "font-style: italic"></a>
                                </li>

                                <li class="social">
                                    <a class="icon-twitter" href=
                                    "https://twitter.com/sparknz" style=
                                    "font-style: italic"></a>
                                </li>

                                <li class="social">
                                    <a class="icon-youtube" href=
                                    "http://www.youtube.com/user/sparknewzealand"
                                    style="font-style: italic"></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="bottomlinks">
                    <div class="bottom">
                        <div class="wrap">
                            <ul>
                                <li>
                                    <a href="http://www.sparknz.co.nz">Spark
                                    New Zealand</a>
                                </li>

                                <li>&nbsp;|&nbsp; <a href=
                                "http://www.sparkdigital.co.nz/">Spark
                                Digital</a>
                                </li>

                                <li>&nbsp;|&nbsp; <a href=
                                "http://www.sparkventures.co.nz">Spark
                                Ventures</a>
                                </li>

                                <li>&nbsp;|&nbsp; <a href=
                                "http://www.sparkfoundation.org.nz/">Spark
                                Foundation</a>
                                </li>
                            </ul>

                            <ul class="legal">
                                <li>
                                    <a href=
                                    "/business/help/other/legaldisclaimer/">Legal
                                    disclaimer</a>
                                </li>

                                <li>&nbsp;|&nbsp; <a href=
                                "http://www.telecom.co.nz/privacypolicy/">Privacy
                                Policy</a>
                                </li>

                                <li>&nbsp;|&nbsp; <a href=
                                "http://www.telecom.co.nz/accessibility/">Accessibility</a>
                                </li>

                                <li>&nbsp;|&nbsp; <a href=
                                "http://www.telecom.co.nz/terms/">Terms and
                                conditions</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--googleon: index-->
    <!-- Defect # 6490 end-->

    <div class="servicecomponents cloudservices">
        <div class="sitecatalyst cloudservice">
            <script src=
            "http://www.spark.co.nz/etc/clientlibs/foundation/sitecatalyst/sitecatalyst.js"
            type="text/javascript"></script> <script src=
"http://www.spark.co.nz/etc/clientlibs/foundation/sitecatalyst/util.js"
            type="text/javascript"></script> <script src=
"http://www.spark.co.nz/content/cms/sample/en/store/_jcr_content/analytics.sitecatalyst.js"
            type="text/javascript"></script> <script src="http://www.spark.co.nz/etc/clientlibs/mac/mac-sc.js"
            type="text/javascript"></script> <script src=
"http://www.spark.co.nz/etc/clientlibs/foundation/sitecatalyst/plugins.js"
            type="text/javascript"></script> <script type="text/javascript">
CQ_Analytics.Sitecatalyst.frameworkComponents = ['knowledgebase/components/pages/page','knowledgebase/components/pages/articlepage','knowledgebase/components/pages/partnersarticlepage','telecomcms/pages/basicPage','knowledgebase/components/pages/helphomepage','knowledgebase/components/pages/jbarticlepage','knowledgebase/componentshttp://www.spark.co.nz/content/rating','knowledgebase/components/pages/hnarticlepage','tnz/components/shop/checkout/confirmation','knowledgebase/components/pages/helparticlepage'];
            /**
            * Sets SiteCatalyst variables accordingly to mapped components. If <code>options<\/code> 
            * object is provided only variables matching the options.componentPath are set.
            *
            * @param {Object} options Parameter object from CQ_Analytics.record() call. Optional.
            */
            CQ_Analytics.Sitecatalyst.updateEvars = function(options) {
            this.frameworkMappings = [];
            this.frameworkMappings.push({scVar:"pageName",cqVar:"eventdata.pageName",resourceType:"knowledgebase/components/pages/page"});
            this.frameworkMappings.push({scVar:"visitorID",cqVar:"eventdata.user",resourceType:"knowledgebase/components/pages/page"});
            this.frameworkMappings.push({scVar:"pageName",cqVar:"eventdata.pageName",resourceType:"knowledgebase/components/pages/articlepage"});
            this.frameworkMappings.push({scVar:"visitorID",cqVar:"eventdata.user",resourceType:"knowledgebase/components/pages/articlepage"});
            this.frameworkMappings.push({scVar:"pageName",cqVar:"eventdata.pageName",resourceType:"knowledgebase/components/pages/partnersarticlepage"});
            this.frameworkMappings.push({scVar:"visitorID",cqVar:"eventdata.user",resourceType:"knowledgebase/components/pages/partnersarticlepage"});
            this.frameworkMappings.push({scVar:"pageName",cqVar:"eventdata.pageName",resourceType:"telecomcms/pages/basicPage"});
            this.frameworkMappings.push({scVar:"campaign",cqVar:"eventdata.ctcCampaignId",resourceType:"telecomcms/pages/basicPage"});
            this.frameworkMappings.push({scVar:"pageURL",cqVar:"eventdata.page.url",resourceType:"telecomcms/pages/basicPage"});
            this.frameworkMappings.push({scVar:"pageName",cqVar:"eventdata.pageName",resourceType:"knowledgebase/components/pages/helphomepage"});
            this.frameworkMappings.push({scVar:"pageName",cqVar:"eventdata.pageName",resourceType:"knowledgebase/components/pages/jbarticlepage"});
            this.frameworkMappings.push({scVar:"visitorID",cqVar:"eventdata.user",resourceType:"knowledgebase/components/pages/jbarticlepage"});
            this.frameworkMappings.push({scVar:"eVar99",cqVar:"eventdata.ratingItemValue",resourceType:"knowledgebase/componentshttp://www.spark.co.nz/content/rating"});
            this.frameworkMappings.push({scVar:"eVar100",cqVar:"eventdata.ratingPageName",resourceType:"knowledgebase/componentshttp://www.spark.co.nz/content/rating"});
            this.frameworkMappings.push({scVar:"prop74",cqVar:"eventdata.ratingItemValue",resourceType:"knowledgebase/componentshttp://www.spark.co.nz/content/rating"});
            this.frameworkMappings.push({scVar:"prop73",cqVar:"eventdata.ratingPageName",resourceType:"knowledgebase/componentshttp://www.spark.co.nz/content/rating"});
            this.frameworkMappings.push({scVar:"event1000",cqVar:"eventdata.events.ratingItemClicked",resourceType:"knowledgebase/componentshttp://www.spark.co.nz/content/rating"});
            this.frameworkMappings.push({scVar:"pageName",cqVar:"eventdata.pageName",resourceType:"knowledgebase/components/pages/hnarticlepage"});
            this.frameworkMappings.push({scVar:"visitorID",cqVar:"eventdata.user",resourceType:"knowledgebase/components/pages/hnarticlepage"});
            this.frameworkMappings.push({scVar:"eVar32",cqVar:"eventdata.productId",resourceType:"tnz/components/shop/checkout/confirmation"});
            this.frameworkMappings.push({scVar:"purchase",cqVar:"eventdata.purchaseId",resourceType:"tnz/components/shop/checkout/confirmation"});
            this.frameworkMappings.push({scVar:"product.quantity",cqVar:"eventdata.purchaseId",resourceType:"tnz/components/shop/checkout/confirmation"});
            this.frameworkMappings.push({scVar:"purchaseID",cqVar:"eventdata.purchaseId",resourceType:"tnz/components/shop/checkout/confirmation"});
            this.frameworkMappings.push({scVar:"event28",cqVar:"eventdata.events.purchase",resourceType:"tnz/components/shop/checkout/confirmation"});
            this.frameworkMappings.push({scVar:"campaign",cqVar:"eventdata.ctcCampaignId",resourceType:"tnz/components/shop/checkout/confirmation"});
            this.frameworkMappings.push({scVar:"products",cqVar:"eventdata.devices",resourceType:"tnz/components/shop/checkout/confirmation"});
            this.frameworkMappings.push({scVar:"pageName",cqVar:"eventdata.pageName",resourceType:"knowledgebase/components/pages/helparticlepage"});
            for (var i=0; i<this.frameworkMappings.length; i++){
            var m = this.frameworkMappings[i];
            if (!options || options.compatibility || (options.componentPath == m.resourceType)) {
            CQ_Analytics.Sitecatalyst.setEvar(m);
            }
            }
            }

            CQ_Analytics.CCM.addListener("storesinitialize", function(e) {
            var collect = true;
            var lte = s.linkTrackEvents;
            var productOrderNumber = "";
            if(productOrderNumber){
            productOrderNumber = productOrderNumber.replace("ONL","");
            }
            s.pageName="content:cms:sample:en:store:tnzProducts:mobile:phones:samsung-galaxy-s6-edge-32gb-black-sapphire";
            s.purchaseID=productOrderNumber;
            if(''){
            s.events="purchase,event28"
            }
            CQ_Analytics.Sitecatalyst.collect(collect);
            if (collect) {
            CQ_Analytics.Sitecatalyst.updateEvars();
            /************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
            var s_code=s.t();if(s_code)document.write(s_code);
            s.linkTrackEvents = lte;
            if(s.linkTrackVars.indexOf('events')==-1){delete s.events};
            $CQ(document).trigger("sitecatalystAfterCollect");
            }
            });
            </script> <noscript><img alt="" border="0" height="1" src=
            "http://http://www.spark.co.nz/b/ss/telecomnz-prd/1/H.25--NS/1433817184663?cdp=3&amp;gn=content%3Acms%3Asample%3Aen%3Astore%3AtnzProducts%3Amobile%3Aphones%3Asamsung-galaxy-s6-edge-32gb-black-sapphire"
            width="1"></noscript> <span data-tracking=
            "{event:'pageView', values:{}, componentPath:'foundation/components/page'}">
            </span>
        </div>
    </div><!-- Footer TagDynamic Tag Management - Start -->
    <script type="text/javascript">
_satellite.pageBottom();
    </script> <!-- Footer TagDynamic Tag Management - End -->
</body>
</html>