<!DOCTYPE html>

<html class="no-js" lang="en">
<head>
    <title>Mobile Re-Eng Buy Journey</title>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <link href=
    "http://www.spark.co.nz/etc/designs/telecomcms/favicon/favicon_32.png" rel=
    "icon">
    <link href=
    "http://www.spark.co.nz/etc/designs/telecomcms/favicon/favicon_32.png" rel=
    "icon" sizes="32x32">
    <link href=
    "http://www.spark.co.nz/etc/designs/telecomcms/favicon/favicon_48.png" rel=
    "icon" sizes="48x48">
    <link href=
    "http://www.spark.co.nz/etc/designs/telecomcms/favicon/favicon_64.png" rel=
    "icon" sizes="64x64">
    <link href=
    "http://www.spark.co.nz/etc/designs/telecomcms/favicon/favicon_128.png"
    rel="icon" sizes="128x128">
    <meta content="#FFFFFF" name="msapplication-TileColor">
    <meta content=
    "http://www.spark.co.nz/etc/designs/telecomcms/favicon/favicon_144.png"
    name="msapplication-TileImage">


    <link href="http://www.spark.co.nz/etc/designs/telecomcms/clientlib_tnz.css" rel="stylesheet" type="text/css">

    <script src="http://www.spark.co.nz/etc/clientlibs/granite/jquery.js" type="text/javascript"></script>
    <script src="http://www.spark.co.nz/etc/clientlibs/granite/utils.js" type="text/javascript"></script>
    <script src="http://www.spark.co.nz/etc/clientlibs/granite/jquery/granite.js" type="text/javascript"></script>
    <script src="http://www.spark.co.nz/etc/clientlibs/foundation/jquery.js" type="text/javascript"></script>
    <script src="http://www.spark.co.nz/etc/designs/telecomcms/clientlib.js" type="text/javascript"></script>
    <link href="http://www.spark.co.nz/content/dam/telecomcms/css/print.css" media="print" rel="stylesheet" type="text/css">

    <!-- Set the viewport width to device width for mobile -->
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <!-- <title>Samsung Galaxy S6 Edge 32GB Black Sapphire</title>
 -->
    <!--  <link rel="shortcut icon" href="https://static.telecom.co.nz/favicon.ico" type="image/x-icon" >
    <link rel="canonical" href=""/> -->


     <!-- Downloaded publish css - plese remove -->
     <link href="../css/remove-on-prod.css" rel="stylesheet" type="text/css">

    <!-- Custom CSS -->

    <link href="../css/mobile-re-eng.css" rel="stylesheet" type="text/css">

    <!-- Font addon remove when on production -->

    <link href="../css/font-addon.css" rel="stylesheet" type="text/css">

    <!-- End custom css -->

    <script src="http://www.spark.co.nz/etc/designs/tnz/publish.js" type=
    "text/javascript"></script>
    <script>
    if (window.nc === undefined) {
            window.nc = {};
        }
         
        nc.validLanguages = ['de', 'en', 'fr'];
    </script>
</head><!--[if IE 8]>
<body class="ie lt-ie9">
<![endif]-->
<!--[if IE]>
<body class="ie">
<![endif]-->
<!--[if gt IE 8]>
<body class="ie">
<![endif]-->

<body>
    <div data-tracking=
    "{event:'PageView','values':{'pageName':'telecom:personal:shop:mobile:phones:samsung-galaxy-s6-edge-32gb-black-sapphire','page.url':'/shop/mobile/phones/samsung-galaxy-s6-edge-32gb-black-sapphire.html'}, componentPath:'tnz/pages/devicedetailpage'}">

    <div class="parbase clientcontext">
        <script src=
        "http://www.spark.co.nz/etc/clientlibs/foundation/shared.js" type=
        "text/javascript"></script> <script src=
"http://www.spark.co.nz/etc/clientlibs/granite/underscore.js" type=
"text/javascript"></script> <script src=
"http://www.spark.co.nz/etc/clientlibs/foundation/personalization/kernel.js"
        type="text/javascript"></script> <script type="text/javascript">
$CQ(function() {
        CQ_Analytics.SegmentMgr.loadSegments("http://www.spark.co.nz/etc/segmentation");
        CQ_Analytics.ClientContextUtils.init("http://www.spark.co.nz/etc/clientcontext/default","http://www.spark.co.nz/content/cms/sample/en/store/tnzProducts/mobile/phones/samsung-galaxy-s6-edge-32gb-black-sapphire");


        });
        </script>
    </div><!-- Defect # 6490 Start-->
    <!--googleoff: index-->

    <div id="header">
        <!-- EOL Test -->
        <!-- EOL Test -->

        <div class="ui-helper-clearfix" id="header-nav-wrap">
            <div class="logo">
                <a class="current-parent" href="/home/" id="top-logo"><img src=
                "http://www.spark.co.nz/etc/designs/telecomcms/resources/images/Spark-Header-Logo1.png"></a>
                <script type="text/javascript">
var images = ['Spark-Header-Logo0.png', 'Spark-Header-Logo1.png', 'Spark-Header-Logo2.png'];
                var src = 'http://www.spark.co.nz/etc/designs/telecomcms/resources/images/Spark-Header-Logo' + Math.floor(Math.random() * images.length) + '.png';
                $('#top-logo img').attr('src',src);
                </script>
            </div>

            <div id="navigation">
                <div class="divisions">
                    <div class="toprightlinks">
                        <nav style="">
                            <ul>
                                <li>
                                    <a href="http://www.sparknz.co.nz/">Spark
                                    New Zealand</a>
                                </li>

                                <li>
                                    <a href=
                                    "http://www.sparkdigital.co.nz">Spark
                                    Digital</a>
                                </li>

                                <li>
                                    <a href=
                                    "http://www.sparkventures.co.nz/">Spark
                                    Ventures</a>
                                </li>

                                <li>
                                    <a href=
                                    "http://www.sparkfoundation.org.nz/">Spark
                                    Foundation</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>

                <div class="headertabs">
                    <nav class="type">
                        <ul>
                            <li class="current-parent menu-top-item active"
                            data-topmenu_id="1" id="menu-top-item1">
                                <a class="current-parent" href=
                                "/home/">Personal</a>
                            </li>

                            <li class="current-parent menu-top-item"
                            data-topmenu_id="2" id="menu-top-item2">
                                <a class="current-parent" href=
                                "/business/">Business</a>
                            </li>
                        </ul>
                    </nav>
                </div>

                <div class="parbase globalnavigationbar">
                    <nav class="primary show">
                        <ul class="primary-wrap">
                            <li class="current active">
                                <a class="current" href="/shop/">Shop</a>

                                <ul class="products">
                                    <li style=
                                    "list-style: none; display: inline">
                                        <div class="arrow-down"></div>
                                    </li>

                                    <li>
                                        <a class="icon circle ie-rounded" href=
                                        "/shop/mobile.html"><img alt=
                                        "mobile-icon.png" src=
                                        "http://www.spark.co.nz/etc/designs/telecomcms/resources/assets/Uploads/mobile-icon.png">

                                        <p>Mobile</p></a>

                                        <ul class="popular">
                                            <li>
                                                <a href=
                                                "/shop/mobile/phones.html">View
                                                phones</a>
                                            </li>

                                            <li>
                                                <a href=
                                                "/shop/mobile/mobile/plansandpricing.html">
                                                Plans &amp; pricing</a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li>
                                        <a class="icon circle ie-rounded" href=
                                        "/shop/internet/"><img alt=
                                        "cloud-icon.png" src=
                                        "http://www.spark.co.nz/etc/designs/telecomcms/resources/assets/Uploads/cloud-icon.png">

                                        <p>Internet</p></a>

                                        <ul class="popular">
                                            <li>
                                                <a href=
                                                "/shop/internet/datacalculator/">
                                                Which broadband?</a>
                                            </li>

                                            <li>
                                                <a href="/shop/internet/">Plans
                                                &amp; pricing</a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li>
                                        <a class="icon circle ie-rounded" href=
                                        "/shop/landline/"><img alt=
                                        "phoneicon.png" src=
                                        "http://www.spark.co.nz/content/dam/telecomcms/icons/phoneicon.png">

                                        <p>Landline</p></a>

                                        <ul class="popular">
                                            <li>
                                                <a href=
                                                "/shop/landline/homephones/">Our
                                                home phones</a>
                                            </li>

                                            <li>
                                                <a href=
                                                "/shop/landline/pricing/">Plans
                                                &amp; pricing</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li class="current">
                                <a class="current" href=
                                "/discover/">Discover</a>
                            </li>

                            <li class="current">
                                <a class="current" href="/myspark/">MySpark</a>
                            </li>

                            <li class="current">
                                <a class="current" href="/help/">Help</a>
                            </li>

                            <li class="search">
                                <form action="http://search.spark.co.nz/search"
                                id="site-search" method="get" name=
                                "site-search">
                                    <div class="input-append">
                                        <!-- Spark Search Vars -->

                                        <div class="hide" id=
                                        "telecom-search-vars">
                                            <input name="site" value=
                                            "spark_all_collection">
                                            <input name="client" value=
                                            "spark_frontend"> <input name=
                                            "output" value="xml_no_dtd">
                                            <input name="proxystylesheet"
                                            value="spark_frontend">
                                            <input name="filter" value="1">
                                        </div><!-- Search Field -->
                                        <input class="span2" id=
                                        "appendedInputButton" name="q"
                                        placeholder="Search topic" type="text">
                                        <!-- Search Button -->
                                         <button class="btn icon-search" style=
                                        "font-style: italic" type=
                                        "submit"></button>
                                    </div>
                                </form>
                            </li>

                            <li>
                                <a href="/contactus/">Contact</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div><!-- end header-nav-wrap -->

        <div class="secondnavigationbar">
            <nav class="secondary show ">
                <ul>
                    <div class="wrap">

                                        
                            <li class="active parent">                                  
                                            

                                                
                                                    <a href="/shop/mobile.html">Mobile</a>
                                                    
                                                    <h3><a href="/shop/mobile.html">Mobile</a></h3>
                                                    <span class="secondary-gradient"></span>
                                                        <ul>
                                                            
                                                                
                                                                
                                                                <li class="">
                                                                        <a href="/shop/mobile/whyultramobile.html">Why Ultra Mobile</a>
                                                                </li>
                                                            
                                                                
                                                                
                                                                <li class="active">
                                                                        <a href="/shop/mobile/phones.html">Phones</a>
                                                                </li>
                                                            
                                                                
                                                                
                                                                <li class="">
                                                                        <a href="/shop/mobile/mobile/plansandpricing.html">Plans &amp; pricing</a>
                                                                </li>
                                                            
                                                            
                                                            
                                                            
                                                                <li class="btn-group">
                                                                        <a class="dropdown-toggle" data-toggle="dropdown">
                                                                                More 
                                                                                <i class="icon-down-open"></i>
                                                                        </a>
                                                                        <ul class="dropdown-menu">
                                                                                
                                                                                    
                                                                                        
                                                                                        <li class="">
                                                                                    <a href="/shop/mobile/mobilebroadband/devices.html">Mobile Broadband &amp; Tablets</a>
                                                                            </li>
                                                                                
                                                                                    
                                                                                        
                                                                                        <li class="">
                                                                                    <a href="/shop/mobile/extras.html">Extras</a>
                                                                            </li>
                                                                                
                                                                                    
                                                                                        
                                                                                        <li class="">
                                                                                    <a href="/shop/mobile/internationalroaming.html">Roaming</a>
                                                                            </li>
                                                                                
                                                                        </ul>
                                                                </li>
                                                            
                                                        </ul>
                                                
                                                                    
                            </li>
                        
                            
                            
                                        
                            <li class=" parent">                                    
                                            
                                                
                                                
                                                    <a href="/shop/internet/">Internet</a>
                                                
                                                
                                                                    
                            </li>
                        
                            
                            
                                        
                            <li class=" parent">                                    
                                            
                                                
                                                
                                                    <a href="/shop/landline/">Landline</a>
                                                
                                                
                                                                    
                            </li>
                        
                    </div>
                </ul>
            </nav>
        </div>
    </div><!-- end header -->
    <!--googleon: index-->

    <div class=" off-canvas hide-extras">
        <script src=
        "http://www.spark.co.nz/etc/clientlibs/granite/jquery/noconflict.js"
        type="text/javascript"></script> <noscript>
        <div class="noscriptmsg">
            <h1>"You don't have JavaScript turned on. This website requires
            JavaScript to function properly."</h1><br>
        </div></noscript>
