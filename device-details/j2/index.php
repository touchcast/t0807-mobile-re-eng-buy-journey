<?php include("../includes/store-header.php"); ?>

        <div id="page">
            <input id="voucherApplied" type="hidden" value="false"> <input id=
            "campaignId" type="hidden" value=""> <input id="reloadValue" name=
            "reloadValue" type="hidden" value=""> <script>
    
             var  isInterestFree = "true";
             var  isInterestFree12Months = "false";
             var  isInterestFree24Months = "true";
            </script> <!-- Content -->

            <div class="row" id="content-container">
                <div class="eight columns">
                    <div class="row">
                        <div class="shop-product-name twelve columns">
                            <h3 class="">iPhone</h3>

                            <div class="clearer"></div>

                            <h4 class="">6 plus 16GB Space Grey</h4>

                            <div class="badge" id="postpaid-badge"></div>

                            <div class="badge hide" id="prepaid-badge"></div>
                        </div>
                        <!-- custom html component added as part of minor works-2 requirement -->

                        <div class="html_component">
                            <div class="parbase customhtml html_managed_area">
                            </div>
                        </div><!-- /custom html component end -->
                    </div>

                    <div class="row">
                        <div class="seven columns">
                            <!-- left column modules -->

                            <div class="row">
                                <div class="shop-product-name twelve columns">
                                    <!-- Product Details -->

                                    <h5>iPhone 6 Plus</h5>

                                    <p>iPhone 6 Plus isn't just bigger - is's better n every way. Larger, yet thinner. More powerful, yet power eficient. It's a new generation of iPhone.</p><!-- Select Buttons -->

                                    <form action="#">
                                        <input name="servicePlanId" type=
                                        "hidden" value="ServicePlan006">
                                        <input name="intfree" type="hidden"
                                        value="24m"> <!-- OPTION_COLOUR -->

                                        <div class="prodname_select_first">
                                            <span class=
                                            "select_option_hdl">Colour
                                            options</span> <select class=
                                            "select_option onsubmittevent"
                                            name="" size="1">
                                                <option data-color="#808080"
                                                data-link="#" selected=
                                                "selected" value="Space Grey">
                                                    Space Grey
                                                </option>

                                                <option data-color="#FFFFFF"
                                                data-link=
                                                "/shop/mobile/phones/samsung-galaxy-s6-edge-32gb-white-pearl.html"
                                                value="White">
                                                    White
                                                </option>
                                            </select>
                                        </div><!-- OPTION_SIZE -->

                                        <div class="prodname_select">
                                            <span class=
                                            "select_option_hdl">Memory
                                            options</span> <select class=
                                            "select_option onsubmittevent"
                                            name="" size="1">
                                                <!-- T959604: 2999: Added code to get a sorted list of sizes available -->

                                                <option data-link="#" selected=
                                                "selected" value="32GB">
                                                    16GB
                                                </option>

                                                <option data-link=
                                                "/shop/mobile/phones/samsung-galaxy-s6-edge-64gb-black-sapphire.html"
                                                data-size="64GB" value="64GB">
                                                    64GB
                                                </option>
                                                <!-- T959604: 2999: Ends here -->
                                            </select>
                                        </div>
                                    </form>

                                    <div class="clearer"></div>
                                    <!-- Social Counter Icons -->

                                    <div class=
                                    "socialcollaborationlist parbase list soco fb-soco">
                                    <div id="fb-root"></div><script>
(function(d, s, id) {
                                        var js, fjs = d.getElementsByTagName(s)[0];
                                        if (d.getElementById(id)) return;
                                        js = d.createElement(s); js.id = id;
                                        js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
                                        fjs.parentNode.insertBefore(js, fjs);
                                        }(document, 'script', 'facebook-jssdk'));
                                        </script>

                                        <div class="share">
                                            <span class="facebook"><span class=
                                            "fb-like" data-layout=
                                            "button_count" data-send="false"
                                            data-show-faces="true" data-width=
                                            "90"></span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="twelve columns top-margin">
                                    <div class=
                                    "parbase youtube list youtubegallery">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="five columns">
                            <!-- middle column modules -->

                            <div class="parbase imagegallery list">
                                <script type="text/javascript">
$(window).load(function() {
                                $("#featuredContent").orbit({
                                timer: false,
                                bullets: true,
                                bulletThumbs: true,
                                bulletThumbLocation: ''
                                });
                                });
                                </script>

                                <div class="product-images" id=
                                "featuredContent">
                                    <img alt="" src="../images/mobile.png" title="">

                                    <!-- <div data-thumb=
                                    "http://www.spark.co.nz/content/dam/tnz/mobile/phones/Samsung/SamsungGS6Edge_BlackSapphire_Front.tnzimage.width70.png">
                                    <img alt="" src="images/mobile.png" title="">
                                    </div>

                                    <div data-thumb=
                                    "http://www.spark.co.nz/content/dam/tnz/mobile/phones/Samsung/SamsungGS6Edge_BlackSapphire_Left3quarter.tnzimage.width70.png">
                                    <img alt="" src=
                                    "http://www.spark.co.nz/content/dam/tnz/mobile/phones/Samsung/SamsungGS6Edge_BlackSapphire_Left3quarter.png"
                                    title=""></div>

                                    <div data-thumb=
                                    "http://www.spark.co.nz/content/dam/tnz/mobile/phones/Samsung/SamsungGS6Edge_BlackSapphire_Side.tnzimage.width70.png">
                                    <img alt="" src=
                                    "http://www.spark.co.nz/content/dam/tnz/mobile/phones/Samsung/SamsungGS6Edge_BlackSapphire_Side.png"
                                    title=""></div>

                                    <div data-thumb=
                                    "http://www.spark.co.nz/content/dam/tnz/mobile/phones/Samsung/SamsungGS6Edge_BlackSapphire_Back.tnzimage.width70.png">
                                    <img alt="" src=
                                    "http://www.spark.co.nz/content/dam/tnz/mobile/phones/Samsung/SamsungGS6Edge_BlackSapphire_Back.png"
                                    title=""></div>

                                    <div data-thumb=
                                    "http://www.spark.co.nz/content/dam/tnz/mobile/phones/Samsung/SamsungGS6Edge_BlackSapphire_Right3quarter.tnzimage.width70.png">
                                    <img alt="" src=
                                    "http://www.spark.co.nz/content/dam/tnz/mobile/phones/Samsung/SamsungGS6Edge_BlackSapphire_Right3quarter.png"
                                    title=""></div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                                                    
                    <div class="row">
                        <div class="twelve columns top-margin">
                            <div><img alt="" src="../images/banner.png" title=""> </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="twelve columns top-margin">
                            <div class="parsys mainbanners"></div>
                            <hr>
                        </div>
                    </div>

                    <div class="row">
                        <div class="tabcontainer">
                            <div class="twelve columns top-margin">
                                <!-- second row left+middle column modules -->
                                <!-- Shop Product details tab module -->

                                <div class=
                                "shop-product-detail tabnavcontainer">
                                    <dl class="tabs contained two-up">
                                        <dt></dt>

                                        <dd class="active">
                                            <a class="a_tab" href=
                                            "#overview">Overview</a>
                                        </dd>

                                        <dt></dt>

                                        <dd>
                                            <span class="separator-bar"></span>
                                            <a class="a_tab" href=
                                            "#paymentoptions">Payment
                                            Options</a>
                                        </dd>
                                    </dl>

                                    <ul class="tabs-content contained">
                                        <li class="active" id="overviewTab">
                                            <div class="parsys">
                                                <div class=
                                                "text parbase richtext section">
                                                <div class="text-box">
                                                <img src="../images/iPhone6_Mockup.png" />
                                                        <h4>A groundbreaking
                                                        dual-edge display</h4>

                                                        <p>The metal bezel
                                                        cascades seamlessly
                                                        into sophisticated
                                                        glass to create
                                                        Samsung’s slimmest,
                                                        most lightweight device
                                                        yet.</p>

                                                       
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="clearer"></div>
                                        </li>

                                        <li id="techdetailsTab">
                                            <div class="tab_row">
                                                <h4>Technical details</h4>
                                            </div>

                                            <div class="tab_row">
                                                <div class="tab_clmn">
                                                    <div class="tab_clmn_info">
                                                        <h5 class=
                                                        "tab_clmn_hl">Battery
                                                        (estimated)</h5>

                                                        <p class=
                                                        "tab_clmn_txt_right">18
                                                        h talk time<br>
                                                        2600 mAh capacity</p>
                                                    </div>
                                                </div>

                                                <div class="tab_clmn">
                                                    <div class="tab_clmn_info">
                                                        <h5 class=
                                                        "tab_clmn_hl">Size &
                                                        weight</h5>

                                                        <p class=
                                                        "tab_clmn_txt_right">
                                                        70.1 mm Width<br>
                                                        142.1 mm Height<br>
                                                        7 mm Depth<br>
                                                        132 g Weight</p>
                                                    </div>
                                                </div>

                                                <div class="tab_clmn">
                                                    <div class="tab_clmn_info">
                                                        <h5 class=
                                                        "tab_clmn_hl">
                                                        Screen</h5>

                                                        <p class=
                                                        "tab_clmn_txt_right">
                                                        1440 px screen
                                                        width<br>
                                                        2560 px screen
                                                        height<br>
                                                        5.1 " Display size<br>
                                                        sAMOLED display
                                                        type</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab_row">
                                                <div class="tab_clmn">
                                                    <div class="tab_clmn_info">
                                                        <h5 class=
                                                        "tab_clmn_hl">
                                                        Memory</h5>

                                                        <p class=
                                                        "tab_clmn_txt_right">32
                                                        GB Storage</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab_row">
                                                <h4>Features</h4>
                                            </div>

                                            <div class="tab_row">
                                                <div class="tab_clmn">
                                                    <div class="tab_clmn_info">
                                                        <h5 class=
                                                        "tab_clmn_hl">
                                                        Entertainment</h5>

                                                        <p class=
                                                        "tab_clmn_txt_right">
                                                        Games<br>
                                                        3.5mm Headphone
                                                        socket<br>
                                                        Music player<br>
                                                        Video player</p>
                                                    </div>
                                                </div>

                                                <div class="tab_clmn">
                                                    <div class="tab_clmn_info">
                                                        <h5 class=
                                                        "tab_clmn_hl">
                                                        Camera/Video</h5>

                                                        <p class=
                                                        "tab_clmn_txt_right">
                                                        16MP Camera<br>
                                                        LED Camera flash<br>
                                                        5MP Front facing
                                                        camera<br>
                                                        Video recording</p>
                                                    </div>
                                                </div>

                                                <div class="tab_clmn">
                                                    <div class="tab_clmn_info">
                                                        <h5 class=
                                                        "tab_clmn_hl">
                                                        Connectivity</h5>

                                                        <p class=
                                                        "tab_clmn_txt_right">
                                                        Bluetooth<br>
                                                        NFC<br>
                                                        802.11a/b/g/n/ac
                                                        2.4G+5GHZ, HT MIMO
                                                        (2x2) Wi-Fi<br>
                                                        Wi-Fi hotspot</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab_row">
                                                <div class="tab_clmn">
                                                    <div class="tab_clmn_info">
                                                        <h5 class=
                                                        "tab_clmn_hl">
                                                        Calling</h5>

                                                        <p class=
                                                        "tab_clmn_txt_right">
                                                        Speakerphone<br>
                                                        Video Calling<br>
                                                        Voice dialing</p>
                                                    </div>
                                                </div>

                                                <div class="tab_clmn">
                                                    <div class="tab_clmn_info">
                                                        <h5 class=
                                                        "tab_clmn_hl">
                                                        Interface</h5>

                                                        <p class=
                                                        "tab_clmn_txt_right">
                                                        Android 5.0 Lollipop
                                                        Operating System<br>
                                                        Has touchScreen</p>
                                                    </div>
                                                </div>

                                                <div class="tab_clmn">
                                                    <div class="tab_clmn_info">
                                                        <h5 class=
                                                        "tab_clmn_hl">
                                                        Messaging</h5>

                                                        <p class=
                                                        "tab_clmn_txt_right">
                                                        POP3/IMAP/Microsoft
                                                        Exchange Email<br>
                                                        Instant messenger<br>
                                                        Predictive text</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab_row">
                                                <div class="tab_clmn">
                                                    <div class="tab_clmn_info">
                                                        <h5 class=
                                                        "tab_clmn_hl">
                                                        Navigation</h5>

                                                        <p class=
                                                        "tab_clmn_txt_right">
                                                        GPS<br>
                                                        Maps</p>
                                                    </div>
                                                </div>

                                                <div class="tab_clmn">
                                                    <div class="tab_clmn_info">
                                                        <h5 class=
                                                        "tab_clmn_hl">
                                                        Network</h5>

                                                        <p class=
                                                        "tab_clmn_txt_right">3G
                                                        up to 42.2Mbps and 4G
                                                        up to 300Mbps Data
                                                        download speed<br>
                                                        Flight mode<br>
                                                        Nano SIM card size</p>
                                                    </div>
                                                </div>

                                                <div class="tab_clmn">
                                                    <div class="tab_clmn_info">
                                                        <h5 class=
                                                        "tab_clmn_hl">
                                                        Tools</h5>

                                                        <p class=
                                                        "tab_clmn_txt_right">
                                                        Alarm clock<br>
                                                        Document edit<br>
                                                        Document viewer</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="parbase download">
                                            </div>

                                            <div class="clearer"></div>
                                        </li>

                                        <li id="whatsupintheboxTab">
                                            <div class="tab_clmn_1o2">
                                                <div class="tab_clmn_info">
                                                    <h3 class="tab_clmn_hl">
                                                    Your phone comes with</h3>

                                                    <div class=
                                                    "text parbase tab_clmn_txt_right richtext">
                                                    <div class="text-box">
                                                            <p>&nbsp;</p>

                                                            <p>Travel
                                                            Adaptor</p>

                                                            <p>&nbsp;</p>

                                                            <p>Data Cable</p>

                                                            <p>&nbsp;</p>

                                                            <p>Headset</p>

                                                            <p>&nbsp;</p>

                                                            <p><a href=
                                                            "http://www.spark.co.nz/content/dam/tnz/mobile/Phone%20manuals/Samsung%20GALAXY%20S6%20Edge%20-%20User%20Manual.pdf">
                                                            Download User
                                                            Manual</a><br></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab_clmn_2o2">
                                                <div class=
                                                "parbase image tab_clmn_image">
                                                    <div id=
                                                    "cq-image-jsp-http://www.spark.co.nz/content/cms/sample/en/store/tnzProducts/mobile/phones/samsung-galaxy-s6-edge-32gb-black-sapphire/jcr:content/tabcontainer/boximage">
                                                    <img alt="" class=
                                                    "cq-dd-image cq-image-placeholder"
                                                    src=
                                                    "http://www.spark.co.nz/etc/designs/default/0.gif"
                                                    title=
                                                    ""></div><script type="text/javascript">
(function() {
                                                    var imageDiv = document.getElementById("cq-image-jsp-http://www.spark.co.nz/content/cms/sample/en/store/tnzProducts/mobile/phones/samsung-galaxy-s6-edge-32gb-black-sapphire/jcr:content/tabcontainer/boximage");
                                                    var imageEvars = '{ imageLink: "/", imageAsset: "", imageTitle: "" }';
                                                    var tagNodes = imageDiv.getElementsByTagName('A');
                                                    for (var i = 0; i < tagNodes.length; i++) {
                                                    var link = tagNodes.item(i); 
                                                    link.setAttribute('onclick', 'CQ_Analytics.record({event: "imageClick", values: ' + imageEvars + ', collect: false, options: { obj: this }, componentPath: "tnz/components/base/image"})');
                                                    }

                                                    })();
                                                    </script>
                                                </div>
                                            </div>

                                            <div class="clearer"></div>
                                        </li>

                                        <li id="paymentoptionsTab">
                                            <div class="parsys">
                                                <div class=
                                                "text parbase richtext section">
                                                <div class="text-box">
                                                        <h4>Get a brand new
                                                        mobile, interest
                                                        free</h4>

                                                        <p>&nbsp;</p>

                                                        <p>If you’ve seen a
                                                        phone you really,
                                                        really like, you don’t
                                                        have to pay for it all
                                                        at once. You can split
                                                        the cost over 12 or 24
                                                        months instead. Just
                                                        join any Pay Monthly
                                                        plan, and if the cost
                                                        of the phone is $199
                                                        and over, you can pay
                                                        it off over a year or
                                                        two on your Spark
                                                        bill.</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="clearer"></div>
                                        </li>
                                    </ul>
                                </div>
                                <!-- End Shop Product details tab module -->
                            </div>
                        </div>
                    </div>
                </div>

                <div class="four columns">
                    <!-- right column modules -->
                    <!-- Compare & Basket -->

                    <div class="compare-basket" style="margin-left: 2em">
                        <div class="minicart">
                            <ul>
                                <li class="shop-filter-cart">
                                    <a class="shop-cart-icon" href=
                                    "#">&nbsp;</a>

                                    <div class="cart-container">
                                        <div class="cart-listing">
                                            <div class="warning_cart_empty">
                                                Your cart's empty.
                                            </div>

                                            <div class="cart-listing-buttons">
                                                <a class="cart-button-close"
                                                href="#">Close</a>

                                                <div class="clearer"></div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div><!-- Add to cart module -->

                    <!-- Add to cart module -->
				<div class="shop-product-addtocart">
					<dl class="tabs contained">
							<dd id="postpaid_tab" class="active">
								<a href="#Pay_Monthly" class="addtocart_tab">
										<span class="sliderleft">Pay Monthly</span>
									</a>
								</dd>
							<dd id="prepaid_tab" class=" addtocart_tab">
								<span class="separator-bar">&nbsp;</span>
									<a href="#Prepaid" class="addtocart_tab">
										<span class="sliderleft">Prepaid</span>
									</a>
								</dd>
					</dl>
					
					<ul class="tabs-content contained">
						
						<li class="active inStock" id="Pay_MonthlyTab">
							<!-- Content for default  -->
									<div class="addToCard-offer default active">
										<div class="addtocart_prices">
											<div class="addtocart_price">
												<div class="addtocart_price_top">
													<span class="addtocart_currency">$</span>
													<span class="addtocart_amount">399</span>
													<div class="addtocart_price_bottom">
															<span class="addtocart_uvp_rrp">
														
															</span>
															<span class="addtocart_uvp_amount"></span>
														</div>
													</div>
											</div>
												<div class="addtocart_uvp">
															<div class="addtocart_uvp_hl">Upfront on a 24 month Ultra Mobile $139</div>
														</div>
														<div class="clearer"></div>
														
															<div class="addtocart_oldprice">
																
																	rrp: <span class="addtocart_oldprice_amount">$1249</span>	
																</div>
													</div>
										
											<div class="addtocart_data">
											  <div class="addtocart_data_item">
								                  <p class="addtocart_data_item_hl">talk</p>
								                   <p class="addtocart_data_item_txt">Unlimited*</p>
									                  	<p class="addtocart_data_item_txt">to any NZ network</p>
									               </div>
								              <div class="addtocart_data_item">
								                  <p class="addtocart_data_item_hl">Text</p>
								                   <p class="addtocart_data_item_txt">Unlimited*</p>
								                  	<p class="addtocart_data_item_txt">to any NZ network</p>
								                  </div>
								              <div class="addtocart_data_item ">
								                  <p class="addtocart_data_item_hl">Data</p>
								                   <p class="addtocart_data_item_amount">10<span class="addtocart_data_item_txt">GB</span></p>
								                  </div>
								              </div>
								           
										</div>

									    <table class="detail-table">
									        <tbody>
									            <tr>
									                <th class="left"><img src=
									                "../images/wifi-pink.png"></th>

									                <td class="right_box"><span class="addtocart_data_item_hl_mobile">1GB WiFi/day</span><br>across New Zealand</td>
									            </tr>

									            <tr>
									                <th class="left"><img src=
									                "../images/music.png"></th>

									                <td class="right_box">
									                    <span class="addtocart_data_item_hl_mobile">Spotify Premium</span>
									                </td>
									            </tr>

									            <tr>
									                <th class="left"><img src=
									                "../images/heart.png"></th>

									                <td class="right_box"><span class="addtocart_data_item_hl_mobile">Thanks</span><br>Music / Movies / Perks</td>
									            </tr>

									        </tbody>
									    </table>
							<!-- Content for defered payment -->
										<a class="modal_lnk auc_cq" href="#" data-link="/content/cms/sample/en/store/tnzProducts/mobile/phones/iphone-6-plus-16gb-space-grey.additionalusage.html?plan=postpaid">Additional usage costs</a>
									<p>
										<a class="button_changethisplan_cq" data-link="/content/cms/sample/en/store/tnzProducts/mobile/phones/iphone-6-plus-16gb-space-grey.choosePlan.html?servicePlanId=ServicePlan004&currentPostPlanId=ServicePlan004&currentPrePaidId=valuepack100107" href="#">Change this plan</a>
									</p>
									<!-- Stock -->
									  <!-- Add to cart -->
								      	<form action="/shop/mobile/phones/iphone-6-plus-16gb-space-grey.commerce.addcartentrytnz.html" method="post">
										    <!-- bundleTemplate1 -->
										      			<!-- bundleTemplate2 -->
										      			<input type="hidden" name="isAuthor" value="false">
									       	<input type="hidden" name="productCode1" value="Device142">
									        <input type="hidden" name="productCode2" value="ServicePlan004">
									        <input type="hidden" name="bundleTemplate1" value="SMAR_Device">
									        <input type="hidden" name="bundleTemplate2" value="SMAR_ServicePlan">
									        <input type="hidden" name="redirect" value="/shop/checkout/options.html"/>
									        <input type="hidden" name="baseStore" value="personalshop">
		           							<input type="hidden" name="redirect-product-not-found" value="/shop/checkout/options.html"/>
									        <button type="submit" class="addtocart_button add_button_txt" title="Add to cart">Add to cart</button>
									   	</form>
									    
						</li>
						<!-- Prepaid -->
						<li class="inStock" id="PrepaidTab">
								<div>
									<div class="addtocart_price_prep">
										<span class="addtocart_currency_prep">$</span> 
												<span class="addtocart_amount_prep">1249</span>
											<div class="addtocart_price_bottom_prep">
				                            <span class="addtocart_uvp_rrp_prep">RRP</span>
				                            <span class="addtocart_uvp_amount_prep"></span>
				                        </div>
				                    	</div>
									<div class="addtocart_uvp_prep">
									   <!-- T959603: 5618: Added code to show the sim price as free if costs zero dollars -->
									    <span class="addtocart_uvp_hl_prep">SIM pack currently FREE</span>
											</div>

										<div class="clearer"></div>
									<!-- Dummy code to display the value pack items in prepaid as well. -->
									
									<!-- 1. If ServiceAddOnEntitlements are not present then show the casual rate Addons. -->
										 <!-- Checking whether ServiceAddOnEntitlementMap is empty -->
										 <div class="addtocart_headline non_casual_rate">Plus $19 Value Pack:</div>
										 	<div class="addtocart_data">
                                              <div class="addtocart_data_item">
                                                  <p class="addtocart_data_item_hl">talk</p>
                                                   <p class="addtocart_data_item_txt">Unlimited*</p>
                                                        <p class="addtocart_data_item_txt">to any NZ network</p>
                                                   </div>
                                              <div class="addtocart_data_item">
                                                  <p class="addtocart_data_item_hl">Text</p>
                                                   <p class="addtocart_data_item_txt">Unlimited*</p>
                                                    <p class="addtocart_data_item_txt">to any NZ network</p>
                                                  </div>
                                              <div class="addtocart_data_item ">
                                                  <p class="addtocart_data_item_hl">Data</p>
                                                   <p class="addtocart_data_item_amount">10<span class="addtocart_data_item_txt">GB</span></p>
                                                  </div>
                                              </div>

									
								            </div>
								<table class="detail-table">
                                            <tbody>
                                                <tr>
                                                    <th class="left"><img src=
                                                    "../images/wifi-pink.png"></th>

                                                    <td class="right_box"><span class="addtocart_data_item_hl_mobile">1GB WiFi/day</span><br>across New Zealand</td>
                                                </tr>

                                                <tr>
                                                    <th class="left"><img src=
                                                    "../images/music.png"></th>

                                                    <td class="right_box">
                                                        <span class="addtocart_data_item_hl_mobile">Spotify Premium</span>
                                                    </td>
                                                </tr>

                                                

                                            </tbody>
                                        </table>
							   
								
						</ul>
					
			</div>

            <div class="clearfix right-box">
                <a class="modal_lnk auc_cq" href="#">Buy this device only</a>
                   
                </div>
				<div class="clearfix right-box">
				        <img src="../images/Mini-Banner-iPhone-6-Plus.jpg" class="rightimage" style="" alt="">
						<div class="bannertext lefttext  tnregular" style="">
					</div>
			    </div>
                    <!-- <div class="parsys rhsbanners">
                        <div class="parbase customhtml section">
                            <div class="bannertext" style=
                            "border:10px solid #ffffff; background-color: #ffffff">
                            <span class="purpleText" style="font-size:16px">
 								<b>Stock Availability</b></span>

                                <p class="greyText" style=
                                "font-size: 12px; margin-top:10px">Please note stock is very limited. Sales open to pre request per customer. Delivery will be 2+ weeks. We'll stay in touch to let you know if your iphone deliver will take loonger than this due to stock availability</p>
                            </div>
                        </div>
                    </div> -->
                </div><!-- End Content -->

                <div class="row">
                    <div class="twelve columns">
                        <a href=
                        "http://http://www.spark.co.nz/digital-content/assets/html/webchat/form/?title=Live%20Chat&subject=Live%20Chat&p_prods=1749&p_cats=4012&omniture=con"
                        target="_blank"><span class="livechat" id="chatdetail"
                        style="display: none;">Live Chat <span class=
                        "livechat_bub"></span> <span class=
                        "clearer"></span></span></a>
                    </div>

                    <div id="chatdisable"></div>
                </div>
            </div>
        </div>
    </div><!--googleoff: index-->

    <?php include("../includes/store-footer.php"); ?>