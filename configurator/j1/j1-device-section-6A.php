<?php include("../includes/header.php"); ?>
<?php include("../includes/sidebar.php"); ?>

<div class="container-fluid global-style">

	<?php include("../includes/page-header.php"); ?>

	<div class="inner-container">

		<br />

		<div class="row content-container">
			<div class="col-xs-8">
				<?php include("../includes/j1/content-block-header.php"); ?>

				<!-- Device details -->
				<?php include("../includes/j1/device-details.php"); ?>
				<!-- End: Device details -->

				

		
				<!-- **********
				Your mobile number
				********** -->
				
				<?php include("../includes/j1/your-mobile-number-default.php"); ?>

				<!-- **********
				Connection date
				********** -->

				<?php include("../includes/j1/connection-date-default.php"); ?>


				<!-- **********
				Sim card
				********** -->


				<?php include("../includes/j1/sim-card-default.php"); ?>

			</div>
			<!-- End of left side of the page -->
		</div>
	</div>
</div>

<?php include("../includes/footer.php"); ?>