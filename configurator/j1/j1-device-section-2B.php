<?php include("../includes/header.php"); ?>
<?php include("../includes/sidebar.php"); ?>

<div class="container-fluid global-style">

	<?php include("../includes/page-header.php"); ?>

	<div class="inner-container">

		<br />

		<div class="row content-container">
			<div class="col-xs-8">
				<?php include("../includes/j1/content-block-header.php"); ?>

				<!-- Device details -->
				<?php include("../includes/j1/device-details.php"); ?>
				<!-- End: Device details -->

				

		
				<!-- **********
				Your mobile number
				********** -->
				
				<div class="card clearfix secondary">
					<div class="inner-card clearfix">
						<div class="inner-wrapper"> 
							<!-- <a href="#" class="float-right">Close <i class="icon-x"></i></a> -->
							<h2 class="card-title underlined">Your mobile number</h2>
							<div class="">
								<!-- <h4>Choose the contract you'd like</h4> -->

								<div class="row">
								<!-- Radio button -->
									<div class="col-xs-12">

										<input type="radio" value="radio0001" id="radio0001" name="radio000" checked class="normal">
										<label for="radio0001"><span></span>Use my Spark number</label>

										<span class="info-box white no-margin margin-bottom">
											
											<div class="number-selection">
												<div class="row ">
													<span class="col-xs-6">
														<input type="radio" value="radio0001" id="sparkNumber1" name="sparkNumber" checked class="normal">
														<label for="sparkNumber1" class="button slim blue"><span></span>027 123 4567</label>
													</span>
													
													<span class="col-xs-6">		
														<input type="radio" value="radio0002" id="sparkNumber2" name="sparkNumber"  class="normal">
														<label for="sparkNumber2" class="button slim blue"><span></span>027 123 4567</label>
													</span>

													<span class="col-xs-6">
														<input type="radio" value="radio0003" id="sparkNumber3" name="sparkNumber"  class="normal">
														<label for="sparkNumber3" class="button slim blue"><span></span>027 123 4567</label>
													</span>
													
													<span class="col-xs-12"><a href="#">Not the right number? Enter it here.</a></span>
												</div>
											</div>
											
											
											  	
										  	

										  	<hr>

										  	<span style="display:block; text-align:center; margin:60px auto;"><img id="spinner-image" style="width:25px;" src="http://spark.co.nz/content/dam/telecomcms/responsive/images/shop/internet/fibre-speed/loadingIcon_desktop.gif"><br><br> Checking... </span>

										</span>
									</div>
									<div class="col-xs-12">
										<input type="radio" value="radio0002" id="radio0002" name="radio000" class="normal">
										<label for="radio0002"><span></span>Give me a new number</label>
									</div>
									<div class="col-xs-12">
										<input type="radio" value="radio0003" id="radio0003" name="radio000" class="normal">
										<label for="radio0003"><span></span>Bring my number from another mobile provider</label>
									</div>

								</div>
							
							</div>
						</div>
					</div>
				</div>

				


				<!-- **********
				Connection date
				********** -->

				<?php include("../includes/j1/connection-date-default.php"); ?>


				<!-- **********
				Sim card
				********** -->

				<?php include("../includes/j1/sim-card-default.php"); ?>




			</div>
			<!-- End of left side of the page -->
			
			
		</div>

	</div>

</div>

<?php include("../includes/footer.php"); ?>