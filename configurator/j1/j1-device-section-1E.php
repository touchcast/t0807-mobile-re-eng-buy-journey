<?php include("../includes/header.php"); ?>
<?php include("../includes/sidebar.php"); ?>

<div class="container-fluid global-style">

	<?php include("../includes/page-header.php"); ?>

	<div class="inner-container">

		<br />

		<div class="row content-container">
			<div class="col-xs-8">
				<?php include("../includes/j1/content-block-header.php"); ?>

				<!-- Device details -->
				<?php include("../includes/j1/device-details.php"); ?>
				<!-- End: Device details -->

				

		
				<!-- **********
				Your mobile number
				********** -->
				
				<div class="card clearfix secondary">
					<div class="inner-card clearfix">
						<div class="inner-wrapper"> 
							<!-- <a href="#" class="float-right">Close <i class="icon-x"></i></a> -->
							<h2 class="card-title underlined">Your mobile number</h2>
							<div class="">
								<!-- <h4>Choose the contract you'd like</h4> -->

								<div class="row">
								<!-- Radio button -->
									<div class="col-xs-12">

										<input type="radio" value="radio0001" id="radio0001" name="radio000" checked class="normal">
										<label for="radio0001"><span></span>Use my Spark number</label>

										<span class="info-box white no-margin margin-bottom">
											
											
											<div class="row">
												<span class="info-box yellow no-margin  margin-bottom">
													Looks like 027 485 6879 is on a Pay Monthly plan. <a href="#">Edit number</a>
												</span>
												

												<div class="form-group">
											    	<div class="row">
											    		<div class="col-sm-4">
											    			 <label for="account_number">Spark Account number:</label>
											    		</div>
											    		<div class="col-sm-7">
											    			 <input type="text" class="form-control" name="account_number" placeholder="Enter account number here">

											    			 <div class="row margin-top no-margin">
															    <input type="checkbox" name="terms12" id="terms12" class="normal">
															    <label for="terms12"><span></span>I am the account holder</label>
															 </div>

													        <a href="#" class="button blue slim margin-top">Check number</a>
											    		</div>
											    	</div>										    	

											    	
											    </div>
										
											  	
											    <span class="col-xs-12">
													<p><a href="#">Back to my Spark number</a></p>
												</span>
													
											</div>

											  	
										  	

										</span>
									</div>
									<div class="col-xs-12">
										<input type="radio" value="radio0002" id="radio0002" name="radio000" class="normal">
										<label for="radio0002"><span></span>Give me a new number</label>
									</div>
									<div class="col-xs-12">
										<input type="radio" value="radio0003" id="radio0003" name="radio000" class="normal">
										<label for="radio0003"><span></span>Bring my number from another mobile provider</label>
									</div>

								</div>
							
							</div>
						</div>
					</div>
				</div>

				


				<!-- **********
				Connection date
				********** -->

				<?php include("../includes/j1/connection-date-default.php"); ?>


				<!-- **********
				Sim card
				********** -->

				<?php include("../includes/j1/sim-card-default.php"); ?>




			</div>
			<!-- End of left side of the page -->
			
			
		</div>

	</div>

</div>

<?php include("../includes/footer.php"); ?>