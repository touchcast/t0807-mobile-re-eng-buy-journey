<?php include("../includes/header.php"); ?>
<?php include("../includes/sidebar.php"); ?>

<div class="container-fluid global-style">

	<?php include("../includes/page-header.php"); ?>

	<div class="inner-container">

		<br />

		<div class="row content-container">
			<div class="col-xs-8">
				<?php include("../includes/j1/content-block-header.php"); ?>

				<!-- Device details -->
				<?php include("../includes/j1/device-details.php"); ?>
				<!-- End: Device details -->

				




		
				<!-- **********
				Your mobile number
				********** -->
				
				<div class="card clearfix secondary">
					<div class="inner-card clearfix">
						<div class="inner-wrapper"> 
							<!-- <a href="#" class="float-right">Close <i class="icon-x"></i></a> -->
							<h2 class="card-title underlined">Your mobile number</h2>
							<div class="">
								<!-- <h4>Choose the contract you'd like</h4> -->

								<div class="row">
								<!-- Radio button -->
								<div class="col-xs-12">

									<input type="radio" value="radio0001" id="radio0001" name="radio000" class="normal">
									<label for="radio0001"><span></span>Use my Spark number</label>

								
								</div>
								<div class="col-xs-12">
									<input type="radio" value="radio0002" id="radio0002" name="radio000" class="normal">
									<label for="radio0002"><span></span>Give me a new number</label>
								</div>
								<div class="col-xs-12">
									<input type="radio" value="radio0003" id="radio0003" name="radio000" checked class="normal">
									<label for="radio0003"><span></span>Bring my number from another mobile provider</label>

									<span class="info-box white no-margin margin-bottom">
											
											<div class="form-group">
										    	<div class="row">
										    		<div class="col-sm-4">
										    			 <label for="contact_number">Current mobile provider:</label>
										    		</div>
										    		<div class="col-sm-7">
										    			 <div class="phone-number row">
												            <div class="prefix col-xs-12">
												                <div class="dropdown">
												                	<input type="hidden" name="prefix-number" value="">
												                    <button class="dropdown-toggle" type="button" id="prefix-number" data-toggle="dropdown">
												                    Choose your provider
												                    <span class="caret"></span>
												                    </button>
												                    <ul class="dropdown-menu" role="menu" aria-labelledby="prefix-number">
												                        <li><a role="menuitem">Vodafone</a></li>
												                        <li><a role="menuitem">2Degrees</a></li>
												                        <li><a role="menuitem">Skinny</a></li>
												 
												                    </ul>
												           
												                </div>
												            </div>
												           
												        </div>  
												      
										    		</div>
										    	</div>

										    </div>

							

									</span>
								</div>

							</div>
							
							</div>
						</div>
					</div>
				</div>

				
				<!-- **********
				Connection date
				********** -->

				<?php include("../includes/j1/connection-date-default.php"); ?>


				<!-- **********
				Sim card
				********** -->

				<?php include("../includes/j1/sim-card-default.php"); ?>



		

			</div>
			
			
		</div>

	</div>

</div>

<?php include("../includes/footer.php"); ?>