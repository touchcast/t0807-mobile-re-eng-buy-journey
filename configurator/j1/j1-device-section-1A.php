<?php include("../includes/header.php"); ?>
<?php include("../includes/sidebar.php"); ?>

<div class="container-fluid global-style">

	<?php include("../includes/page-header.php"); ?>

	<div class="inner-container">

		<br />

		<div class="row content-container">
			<div class="col-xs-8">
				<?php include("../includes/j1/content-block-header.php"); ?>

				<!-- Device details -->
				<?php include("../includes/j1/device-details.php"); ?>
				<!-- End: Device details -->

				

		
				<!-- **********
				Your mobile number
				********** -->
				
				<div class="card clearfix secondary">
					<div class="inner-card clearfix">
						<div class="inner-wrapper"> 
							<!-- <a href="#" class="float-right">Close <i class="icon-x"></i></a> -->
							<h2 class="card-title underlined">Your mobile number</h2>
							<div class="">
								<!-- <h4>Choose the contract you'd like</h4> -->

								<div class="row">
								<!-- Radio button -->
									<div class="col-xs-12">

										<input type="radio" value="radio0001" id="radio0001" name="radio000" checked class="normal">
										<label for="radio0001"><span></span>Use my Spark number</label>

										<span class="info-box white no-margin margin-bottom">

										  	<span style="display:block; text-align:center; margin:60px auto;"><img id="spinner-image" style="width:25px;" src="http://spark.co.nz/content/dam/telecomcms/responsive/images/shop/internet/fibre-speed/loadingIcon_desktop.gif"><br><br> Checking... </span>

										</span>
									</div>
									<div class="col-xs-12">
										<input type="radio" value="radio0002" id="radio0002" name="radio000" class="normal">
										<label for="radio0002"><span></span>Give me a new number</label>
									</div>
									<div class="col-xs-12">
										<input type="radio" value="radio0003" id="radio0003" name="radio000" class="normal">
										<label for="radio0003"><span></span>Bring my number from another mobile provider</label>
									</div>

								</div>
							
							</div>
						</div>
					</div>
				</div>

				


				<!-- **********
				Connection date
				********** -->

				<?php include("../includes/j1/connection-date-default.php"); ?>


				<!-- **********
				Sim card
				********** -->

				<?php include("../includes/j1/sim-card-default.php"); ?>




			</div>
			<!-- End of left side of the page -->
			
			
		</div>

	</div>

</div>

<footer>
	<!-- ///// Desktop Footer | same structure as  CQ ///// -->
	<div class="desktop-nav">
		<div id="footer">
			<div class="footerchannel">
				<div id="footer_channel">
					<div id="mask"></div>
					<div class="help"><a href="javascript:window.print()"><i class="icon-doc-text-inv"></i>Print this page</a> </div>
					<a href="#top" class="top"><i class="icon-angle-circled-up"></i> Back to top</a> </div>
			</div>
			<div class="footerbreadcrumb">
				<div class="fullwidth-bg">
				</div>
			</div>

			<div class="bottom">
				<div class="wrap">
					<div class="row">
						<div class="col-xs-6">
							<ul>
								<li> <a href="http://www.sparknz.co.nz"> Spark New Zealand </a> </li>
								<li> &nbsp;|&nbsp; <a href="http://www.sparkdigital.co.nz"> Spark Digital </a> </li>
								<li> &nbsp;|&nbsp; <a href="http://www.sparkventures.co.nz"> Spark Ventures </a> </li>
								<li> &nbsp;|&nbsp; <a href="http://www.sparkfoundation.org.nz"> Spark Foundation </a> </li>
							</ul>
						</div>
						<div class="col-xs-6">
							<div class="copyright">© Copyright Spark 2014 All rights reserved</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<!-- Desktop footer END -->
</footer>
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script> 
<script type="text/javascript" src="js/modernizr.js"></script> 
<script type="text/javascript" src="js/bootstrap.js"></script> 
<script type="text/javascript" src="js/mobile.js"></script> 
<script type="text/javascript" src="js/wow.min.js"></script> 
<script type="text/javascript" src="js/fastclick.js"></script> 
<script type="text/javascript" src="js/bootstrap-tabcollapse.js"></script> 
<script type="text/javascript" src="js/slick.js"></script>
<!-- <script type="text/javascript" src="js/sidebar.js"></script> -->
<script type="text/javascript" src="js/carousel.js"></script>
<script type="text/javascript" src="js/detect.js"></script>
<script type="text/javascript" src="js/custom.js"></script>

<script>
	$(function() {

    var $sidebar   = $(".accord-wrapper "), 
        $window    = $(window),
        offset     = $sidebar.offset(),
        topPadding = 0;

    $window.scroll(function() {
        if ($window.scrollTop() > offset.top) {
            $sidebar.stop().animate({
                marginTop: $window.scrollTop() - offset.top + topPadding
            },0);
        } else {
            $sidebar.stop().animate({
                marginTop: 0
            },0);
        }
    });
    
});
</script>
</body>
</html>