<?php include("../includes/header.php"); ?>
<?php include("../includes/sidebar.php"); ?>

<div class="container-fluid global-style">

	<?php include("../includes/page-header.php"); ?>

	<div class="inner-container">

		<br />

		<div class="row content-container">
			<div class="col-xs-8">
				<?php include("../includes/j1/content-block-header.php"); ?>

				<!-- Device details -->
				<?php include("../includes/j1/device-details.php"); ?>
				<!-- End: Device details -->

				

		
				<!-- **********
				Your mobile number
				********** -->
				
				<div class="card clearfix secondary">
					<div class="inner-card clearfix">
						<div class="inner-wrapper"> 
							<!-- <a href="#" class="float-right">Close <i class="icon-x"></i></a> -->
							<h2 class="card-title underlined">Your mobile number</h2>
							<div class="">
								<!-- <h4>Choose the contract you'd like</h4> -->

								<div class="row">
								<!-- Radio button -->
									
									<div class="col-xs-12">
										<input type="radio" value="radio0001" id="radio0001" name="radio000" checked class="normal">
										<label for="radio0001"><span></span>Use my Spark number</label>
									</div>
									<div class="col-xs-12">
										<input type="radio" value="radio0002" id="radio0002" name="radio000" class="normal">
										<label for="radio0002"><span></span>Give me a new number</label>
									</div>
									<div class="col-xs-12">
										<input type="radio" value="radio0003" id="radio0003" name="radio000" class="normal">
										<label for="radio0003"><span></span>Bring my number from another mobile provider</label>
									</div>
									<div class="col-xs-12">
										<span class="info-box white no-margin margin-bottom">
											
											<div class="row">

												<div class="form-group">
													<div class="row">
											    		<div class="col-sm-4">
											    			 <label for="contact_number">Current mobile provider:</label>
											    		</div>
											    		<div class="col-sm-7">
											    			 <div class="">
													            <div class="prefix">
													                <div class="dropdown">
													                    <button data-toggle="dropdown" id="prefix-number-1" type="button" class="dropdown-toggle">
													                    Select
													                    <span class="caret"></span>
													                    </button>
													                    <ul aria-labelledby="prefix-number-1" role="menu" class="dropdown-menu">
													                        <li><a role="menuitem">Vodafone</a></li>
													                        <li><a role="menuitem">2 Degrees</a></li>
													                        <li><a role="menuitem">Telstra</a></li>
													                        <li><a role="menuitem">Skinny</a></li>
													                    </ul>
													                </div>
													            </div>
													        </div>  
											    		</div>
											    	</div>

											    	<div class="row margin-top">
											    		<div class="col-sm-4">
											    			 <label for="contact_number">My Spark number:</label>
											    		</div>
											    		<div class="col-sm-7">
											    			 <div class="row phone-number">
													            <div class="prefix col-xs-4">
													                <div class="dropdown short prefix">
													                	<input type="hidden" name="prefix-number" value="">
													                    <button class="dropdown-toggle" type="button" id="prefix-number" data-toggle="dropdown">
													                    Prefix
													                    <span class="caret"></span>
													                    </button>
													                    <ul class="dropdown-menu" role="menu" aria-labelledby="prefix-number">
													                        <li><a role="menuitem">020</a></li>
													                        <li><a role="menuitem">021</a></li>
													                        <li><a role="menuitem">022</a></li>
													                        <li><a role="menuitem">027</a></li>
													                        <li><a role="menuitem">028</a></li>
													                        <li><a role="menuitem">029</a></li>
													                        <li><a role="menuitem">0204</a></li>
													                    </ul>
													                </div>
													            </div>
													            <div class="number col-xs-8">
													                <input class="input-large" id="contact_number" placeholder="Number" type="text">
													            </div>
													        </div>  
											    		</div>
											    	</div>

													<div class="row margin-top">
														<div class="col-sm-4">
											    			 <label for="contact_number">How do you pay for it:</label>
											    		</div>
											    		<div class="col-sm-7">
											    			<div class="col-sm-4">
																<input type="radio" value="radio0004" id="radio0004" checked name="radio0001" class="normal">
																<label for="radio0004"><span></span>Prepay</label>
															</div>
															<div class="col-sm-8">
																<input type="radio" value="radio0005" id="radio0005" name="radio0001" class="normal">
																<label for="radio0005"><span></span>On account</label>
															</div>
														</div>
													</div>

											    	<div class="row">
											    		<div class="col-sm-4">
											    			 <label for="contact_number">Account number:</label>
											    		</div>
											    		<div class="col-sm-7">
											    			<input class="input-large" id="contact_number" placeholder="Number" type="text">

											    			<!-- <h5 class="block blue padding-top"><span><span class="block-link">Link #FFF</span></h5> -->
											    		</div>
											    	</div>
											    	
											    </div>
											</div>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- **********
				Connection date
				********** -->

				<?php include("../includes/j1/connection-date-default.php"); ?>


				<!-- **********
				Sim card
				********** -->

				<div class="card clearfix secondary">
					<div class="inner-card clearfix">
						<div class="inner-wrapper">
							<a href="#" class="float-right">
								Change <i class="icon-pencil"></i>
							</a>
							<h2 class="card-title underlined">SIM cards</h2>
							<div class="">
								<!-- <h4>Choose the contract you'd like</h4> -->

								<div class="row">
									<ul>
										<li>
											<strong>Add Trio SIM card</strong> - 4G ready and works with your mobile - $4.99
										</li>
									</ul>

								</div>
							
							</div>
						</div>
					</div>
				</div>




			</div>
			<!-- End of left side of the page -->
			
			
		</div>

	</div>

</div>

<?php include("../includes/footer.php"); ?>