<?php include("../includes/header.php"); ?>
<?php include("../includes/sidebar.php"); ?>

<div class="container-fluid global-style">

	<?php include("../includes/page-header.php"); ?>

	<div class="inner-container">

		<br />

		<div class="row content-container">
			<div class="col-xs-8">
				<?php include("../includes/j1/content-block-header.php"); ?>

				<!-- Device details -->
				<?php include("../includes/j1/device-details.php"); ?>
				<!-- End: Device details -->

				

		
				<!-- **********
				Your mobile number
				********** -->
				
				<div class="card clearfix secondary">
					<div class="inner-card clearfix">
						<div class="inner-wrapper"> 
							<!-- <a href="#" class="float-right">Close <i class="icon-x"></i></a> -->
							<h2 class="card-title underlined">Your mobile number</h2>
							<div class="">
								<!-- <h4>Choose the contract you'd like</h4> -->

								<div class="row">
								<!-- Radio button -->
									<div class="col-xs-12">

										<input type="radio" value="radio0001" id="radio0001" name="radio000" checked class="normal">
										<label for="radio0001"><span></span>Use my Spark number</label>

										<span class="info-box white no-margin margin-bottom">
											
											
										  	<div>
										  		<div class="row">

										  			<div class="col-xs-12">

												    	<!-- Radio button -->

												    	<div>
												    		<input type="radio" value="radio1" id="radio1" name="radio-plan">
													    	
													    	<label for="radio1" class="col-xs-12">
													    		<div class="row">
													    			<div class="col-xs-1">
													    				<span></span>
													    			</div>
													    			<div class="col-xs-11">
													    				<div class="radio-content">
															    			<!-- radio button 	start -->
																	  			<div class="radio-button">
																	  				<div class="row" style="margin:10px 0;">
																			  			<div class="col-xs-3">Mobile number:</div>
																			  			<div class="col-xs-8">027 123 4567</div>
																			  		</div>

																			  	
																			  		<div class="row" style="margin:10px 0;">
																			  			<div class="col-xs-3">Current plan:</div>

																			  			<div class="row">
																			  				<div class="col-xs-8">
																				  				<p>24 Month Ultra Mobile $59</p>
																								<div class="row">
																									<div class="col-xs-4">
																										<p><strong>TALK:</strong> Unlimited</p>
																								
																										<img class="thanks-offer" src="../images/config/Thanks.png" alt="Thanks">
																									</div>
																									<div class="col-xs-4">
																										<p><strong>TEXT:</strong> Unlimited</p>
																										
																										<div class="wifi-offer">
																											<img src="../images/config/Wifi.png" alt="Thanks"><strong style="color:#666"> 1GB WiFi/day</strong>
																										</div>
																										
																									</div>
																									<div class="col-xs-4">
																										<p><strong>DATA:</strong> 2.5GB</p>
																									
																										<img class="spotify-offer" src="../images/config/Spotify.png" alt="Spotify">
																									</div>
																								</div>
																				  			</div>
																			  			</div>
																			  			
																			  		</div>
																			  		
																			  		<div class="row" style="margin:10px 0;">
																			  			<div class="col-xs-3">Plan end date:</div>
																			  			<div class="col-xs-8">12/06/2016</div>
																			  		</div>
																	  			</div>
																	  		<!-- radio button 	end -->
															    		</div>
													    			</div>
													    		</div>
													    	</label>
												    	</div>
												    	<div class="clearfix">
												    		<hr class="col-xs-12">
												    	</div>
														
												    	<div>
												    		 <input type="radio" value="radio2" id="radio2" name="radio-plan">
													    	<label for="radio2" class="col-xs-12">

													    		<div class="row">
													    			<div class="col-xs-1">
													    				<span></span>
													    			</div>

													    			<div class="col-xs-11">
													    				<div class="radio-content">
															    			<!-- radio button 	start -->
																	  			<div class="radio-button">
																	  				<div class="row" style="margin:10px 0;">
																			  			<div class="col-xs-3">Mobile number:</div>
																			  			<div class="col-xs-8">027 123 4567</div>
																			  		</div>

																			  	
																			  		<div class="row" style="margin:10px 0;">
																			  			<div class="col-xs-3">Current plan:</div>

																			  			<div class="row">
																			  				<div class="col-xs-8">
																				  				<p>24 Month Ultra Mobile $59</p>
																								<div class="row">
																									<div class="col-xs-4">
																										<p><strong>TALK:</strong> Unlimited</p>
																								
																										<img class="thanks-offer" src="../images/config/Thanks.png" alt="Thanks">
																									</div>
																									<div class="col-xs-4">
																										<p><strong>TEXT:</strong> Unlimited</p>
																										
																										<div class="wifi-offer">
																											<img src="../images/config/Wifi.png" alt="Thanks"><strong style="color:#666"> 1GB WiFi/day</strong>
																										</div>
																										
																									</div>
																									<div class="col-xs-4">
																										<p><strong>DATA:</strong> 2.5GB</p>
																									
																										<img class="spotify-offer" src="../images/config/Spotify.png" alt="Spotify">
																									</div>
																								</div>
																				  			</div>
																			  			</div>
																			  			
																			  		</div>
																			  		
																			  		<div class="row" style="margin:10px 0;">
																			  			<div class="col-xs-3">Plan end date:</div>
																			  			<div class="col-xs-8">12/06/2016</div>
																			  		</div>
																	  			</div>
																	  			<!-- radio button 	end -->
															    		</div>
													    			</div>
													    			
													    		</div>

													    		
													    	</label>
												    	</div>

												    

													</div>
										  			
										  		</div>

										  		<div class="row" style="margin:10px 0">
										  			<span class="col-xs-12"><a href="#">Not the right number? Enter it here.</a></span>
										  		</div>


										  	</div>

										</span>
									</div>
									<div class="col-xs-12">
										<input type="radio" value="radio0002" id="radio0002" name="radio000" class="normal">
										<label for="radio0002"><span></span>Give me a new number</label>
									</div>
									<div class="col-xs-12">
										<input type="radio" value="radio0003" id="radio0003" name="radio000" class="normal">
										<label for="radio0003"><span></span>Bring my number from another mobile provider</label>
									</div>

								</div>
							
							</div>
						</div>
					</div>
				</div>

				


				<!-- **********
				Connection date
				********** -->

				<?php include("../includes/j1/connection-date-default.php"); ?>


				<!-- **********
				Sim card
				********** -->
				<?php include("../includes/j1/sim-card-default.php"); ?>




			</div>
			<!-- End of left side of the page -->
			
			
		</div>

	</div>

</div>

<?php include("../includes/footer.php"); ?>