<?php include("../includes/header.php"); ?>
<?php include("../includes/sidebar.php"); ?>

<div class="container-fluid global-style">

	<?php include("../includes/page-header.php"); ?>

	<div class="inner-container">

		<br />

		<div class="row content-container">
			<div class="col-xs-8">
				<?php include("../includes/j1/content-block-header.php"); ?>

				<!-- Device details -->
				<?php include("../includes/j1/device-details.php"); ?>
				<!-- End: Device details -->

				

		
				<!-- **********
				Your mobile number
				********** -->
				
				<?php include("../includes/j1/your-mobile-number-default.php"); ?>

				<!-- **********
				Connection date
				********** -->

				<div class="card clearfix secondary">
					<div class="inner-card clearfix">
						<div class="inner-wrapper"> 
							<a href="#" class="float-right">
								Close <i class="icon-x"></i>
							</a> 
							<h2 class="card-title underlined">Connection date</h2>
							<div class="">
								<!-- <h4>Choose the contract you'd like</h4> -->

								<div class="row">
									<div class="col-xs-12">
										<input type="radio" value="radio0006" id="radio0006" name="radio002" class="normal">
										<label for="radio0006"><span></span>As soon as possible</label>
									</div>
									<div class="col-xs-12">
										<input type="radio" value="radio0007" id="radio0007" name="radio002" checked class="normal">
										<label for="radio0007"><span></span>Choose a date</label>
									</div>

									<div class="col-xs-12">
										<span class="info-box white no-margin margin-bottom">
											
											<div class="row">

												<div class="form-group">
													<div class="row">
											    		<div class="col-sm-4 preffered-date">
											    			 <label for="contact_number">Preffered date:</label>
											    		</div>
											    		<div class="col-sm-7">
											    			<input type='text' class="form-control" placeholder="--select connection date--" id="datetimepicker4"/>
											    		</div>
											    	</div>									    	
											    </div>
											</div>
										</span>
									</div>
								</div>
							
							</div>
						</div>
					</div>
				</div>


				<!-- **********
				Sim card
				********** -->


				<?php include("../includes/j1/sim-card-default.php"); ?>




			</div>
			<!-- End of left side of the page -->
			
			
		</div>

	</div>

</div>

<?php include("../includes/footer.php"); ?>