<?php include("../includes/checkout-header.php"); ?>

    <div class="container-fluid global-style mobile-reeng">

        <div class="page-header">
            <div class="center-wrap">
                <h1 class="set-up-header">Confirmation</h1>
            </div>
        </div>

        <div class="inner-container">
            <br />

            <div class="row">
                <div class="col-xs-12">
                    <div class="content-block-header">
                        <h1 class="block"><span>Yes, you're all done</span></h1>
                    </div>
                </div>  
            </div>
            <div class="row">
                    <div class="col-xs-12 col-sm-12 col-xs-12">
                        <div class="card primary table clearfix ico-content-card">
                            <div class="inner-card clearfix">
                                <span class="info-box green-box green margin-bottom no-margin-top">
                                    <div>
                                        <h3>Your order number is ONL1043. We've emailed your order details to &lt;jonathan.bardsley@gmail.com&gt;.</h3>
                                    </div>
                                </span>
                                <h3>What happens next?</h3>
                                <p>We'll be in touch again within 48 hours to confirm the details. This will include your connection date and any other stuff you might need. Any questions? Email us at <a href="mailto:customer_service@spark.co.nz">customer_service@spark.co.nz</a> or give us a call on 123.</p>
                                <h3>Your next Spark bill</h3>
                                <p>We bill for any services one month in advance. So when you sign up for something new, we need to bring all your Spark stuff into line on the next bill. That can make things look complicated but it's not really. Here's a <a href="#">simple explanation</a>.</p>
                                <h3>One last thing</h3>
                                <p>A 30 day minimum payment applies to your new packages. So if you cancel it within 30 days, you'll still be charged for a full month.</p>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
                <div class="row">
                    <div class="col-xs-12">
                        <hr class="hr-top" />
                    </div>
                </div>
    
            <div class="inner-container">

                <div class="row">
                    <div class="col-xs-12">
                        <div class="content-block-header">
                            <h1 class="block"><span>Discover</span></h1>
                        </div>
                    </div>  
                </div>

                <div class="card primary clearfix hero-card can-animate ClickTransition thankyou-image">
                    <a href="#">    
                        <div class="fg-image">
                            <img src="../images/experience-rhythm.png" />
                        </div>
                        <div class="inner-card custom-inner-card">
                            <h1 class="block pink">
                                <span>Experience rhythm and vines with Spark and Spotify</span><br><span class="block-link play">Find out more</span>
                            </h1>

                        </div>
                    </a>        
                </div>

                <div class="card primary clearfix hero-card can-animate ClickTransition image-2">
                    <a href="#">    
                        <div class="fg-image">
                            <img src="../images/protect-your-smartphone.png" />
                        </div>
                        <div class="inner-card">
                            <h1 class="block pink">
                                <span>Protect your smart phone<br /> - IP Ratings</span><span class="block-link play">Watch</span>
                            </h1>

                        </div>
                    </a>        
                </div>              
            </div>
<?php include("../includes/checkout-footer.php");?>

