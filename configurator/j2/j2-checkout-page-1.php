<?php include("../includes/header.php"); ?>

        <div class="page-header">
            <div class="center-wrap">
                <h1>Checkout</h1>
            </div>
        </div>

        <div class="container-fluid global-style checkout-step-4">
            <div class="inner-container">

                <!-- Nav bar #1 -->
                <div class="top-progress-bar four-steps">
                    <a href="#">
                        <div class="previous">
                            <span class="progress-bar-label"><h4>Delivery</h4></span>
                            <div class="arrow-right"></div>
                            <div class="arrow-right border"></div>
                        </div>
                    </a>
                    <a href="#">
                        <div class="previous">
                            <span class="progress-bar-label"><h4>Account setup</h4></span>
                            <div class="arrow-right"></div>
                            <div class="arrow-right border"></div>
                        </div>
                    </a>
                    <div class="current">
                        <span class="progress-bar-label"><h4>Review & confirm</h4></span>
                        <div class="arrow-right"></div>
                    </div>
                    <div class="last default">
                        <span class="progress-bar-label"><h4>Payments</h4></span>
                    </div>
                    <div class="bottom-border"></div>
                </div>
                <!-- END: Nav bar -->

                <!-- Nav bar #2 -->
                <div class="top-progress-bar three-steps">
                    <a href="#">
                        <div class="previous">
                            <span class="progress-bar-label"><h4>Delivery</h4></span>
                            <div class="arrow-right"></div>
                            <div class="arrow-right border"></div>
                        </div>
                    </a>
                    <div class="current">
                        <span class="progress-bar-label"><h4>Review & confirm</h4></span>
                        <div class="arrow-right"></div>
                    </div>
                    <div class="last default">
                        <span class="progress-bar-label"><h4>Payments</h4></span>
                    </div>
                    <div class="bottom-border"></div>
                </div>
                <!-- END: Nav bar -->


                <div class="card clearfix primary">
                    <div class="inner-card clearfix">
                    <h2>Please review and confirm your order</h2>
                    <p>
                        One last thing - make sure you're happy with your order. Use the edit links if you need to change anything then hit 'Place Order'. Easy.
                    </p>
                    </div>
                </div>


                <div class="card clearfix secondary">
                    <div class="inner-card clearfix">
                        <h2>24 month Ultra Mobile $99</h2>

                        <a class="float-right" href="#">
                            Change
                            <i class="icon-pencil"></i>
                        </a>
                        <h4 class="uppercase wth-underline">Plan</h4>

                        <table class="table on-light-grey margin-bottom" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    24 month Ultra Mobile $99
                                </td>
                            </tr>
                        </table>

                        <a class="float-right" href="#">
                            Change
                            <i class="icon-pencil"></i>
                        </a>
                        <h4 class="uppercase wth-underline">Number</h4>

                        <table class="table on-light-grey margin-bottom" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    026 870 530
                                </td>
                            </tr>
                        </table>

                        <div class="col-xs-4 col-h3 pull-right">
                            <h3 class="no-margin">SUBTOTAL</h3>
                            <div class="row">
                                <div class="col-xs-8">
                                    <h5>Monthly plan payments</h5>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <h5>$99.00</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card clearfix secondary">
                    <div class="inner-card clearfix">
                        <h2>Your details</h2>
                        <a class="float-right" href="#">
                            Change
                            <i class="icon-pencil"></i>
                        </a>
                        <h4 class="uppercase wth-underline">Personal details</h4>

                        <table class="table on-light-grey margin-bottom" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    Name
                                </td>
                                <td>
                                    Jonathan Bardsley
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Contact number
                                </td>
                                <td>
                                    (021) 870 530
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Email
                                </td>
                                <td>
                                    jonathan.bardsley@gmail.com
                                </td>
                            </tr>
                        </table>

                        <!-- divider -->

                        <a class="float-right" href="#">
                            Change
                            <i class="icon-pencil"></i>
                        </a>
                        <h4 class="uppercase wth-underline">Delivery address</h4>

                        <table class="table on-light-grey margin-bottom" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    Touchcast, Level 2, 445 Karangahape Road, Auckland 1010
                                </td>
                            </tr>
                        </table>

                        <!-- divider -->

                        <a class="float-right" href="#">
                            Change
                            <i class="icon-pencil"></i>
                        </a>
                        <h4 class="uppercase wth-underline">Billing details</h4>

                        <table class="table on-light-grey margin-bottom" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    Account name
                                </td>
                                <td>
                                    Jonathan Bardsley
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Account number
                                </td>
                                <td>
                                    288 106 134
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Billing address
                                </td>
                                <td>
                                    6 Ferguson Avenue, Greenlane, Auckland 1051
                                </td>
                            </tr>
                        </table>

                    </div>
                </div>

                <div class="card clearfix secondary">
                    <div class="inner-card clearfix tight">
                        <h2>Summary</h2>

                        <div class="cart-summary-row">
                            <div class="row">
                                <div class="col-xs-6">
                                    <span class="cart-summary-text">Monthly plan payments</span>
                                </div>
                                <div class="col-xs-6">
                                    <div class="cart-summary-text text-right">
                                        $99.00
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="cart-summary-row">
                            <div class="row">
                                <div class="col-xs-6">
                                    <span class="cart-summary-text">Total</span>
                                </div>
                                <div class="col-xs-6">
                                    <div class="cart-summary-text text-right">
                                        $99.00
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix">
                            <input type="checkbox" name="terms" class="normal" id="terms">
                            <label for="terms" class="no-margin"><span></span>I agree to the <a href="#">Terms and Conditions</a>.</label>
                        </div>

                        <div class="clearfix">
                            <input type="checkbox" name="opt_in" class="normal" id="opt_in">
                            <label for="opt_in" class="no-margin"><span></span>Please email me updates about any new services, news, offers and exclusive promotions.</label>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="button-section quad-btns">
                        <div class="blue_bg">
                            <div class="row">
                                <button class="normal primary blue" type="button">Place Order</button>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card clearfix secondary">
                    <div id="accordion2" class="panel-group card animated" style="visibility: visible;">

                      <div id="panel12" class="panel panel-default">
                        <div class="panel-heading">
                          <div class="panel-title">
                            <a href="#collapseTwelve" data-parent="#accordion2" data-toggle="collapse" class="panel-toggle">
                              <h4 class="uppercase">Important things you need to know</h4>
                            </a>
                          </div>
                        </div>
                        <div class="panel-collapse collapse in" id="collapseTwelve">
                          <div class="panel-body">
                            <p>
                                All monthly costs will be charged to your Spark bill. Ongoing monthly costs may differ to the amount you see on your next Spark bill due to billing part way through the month. <a href="#">Find out more</a>.
                            </p>
                            <p>
                                There are several factors that can affect whether broadband is available at your address or not. And we’re not able to test for all of them in advance. So it’s important to note that we can’t guarantee the service works until broadband has been hooked up and we have tested your phone line.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>

                <div class="card clearfix secondary">
                    <div  id="accordion3" class="panel-group card animated">
                      <div id="panel13" class="panel panel-default">
                        <div class="panel-heading">
                          <div class="panel-title">
                            <a href="#collapseThirteen" data-parent="#accordion3" data-toggle="collapse" class="panel-toggle collapsed">
                              <h4 class="uppercase">Not so important stuff</h4>
                            </a>
                          </div>
                        </div>
                        <div class="panel-collapse collapse" id="collapseThirteen" style="">
                          <div class="panel-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mi mi, fringilla sed urna non, porttitor luctus est. Curabitur quis congue erat. Donec et elit ut justo pulvinar commodo. Sed ac ipsum id ex consequat posuere sed at risus. Donec vitae libero nibh. Praesent et mattis lectus, ut volutpat est. Pellentesque felis diam, sollicitudin et velit in, rutrum egestas est. Sed sed libero elementum, ultricies augue sed, lacinia nulla. Integer dictum scelerisque magna quis pulvinar. Maecenas sit amet libero dignissim, efficitur velit ut, ornare ligula. Ut tincidunt ac justo sit amet fringilla. Mauris malesuada est ex, at tincidunt elit scelerisque sed. Pellentesque porta lacus vitae ipsum fringilla tincidunt.</p>
                          </div>
                        </div>
                      </div>
                      
                    </div>
                </div>

            </div>
        </div>

<?php include("../includes/footer.php"); ?>