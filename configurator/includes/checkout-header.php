<!DOCTYPE HTML>
<html lang="en">
    <head>
        <title>Spark New Zealand - Configurator</title>
        <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=0" /> -->
        <meta name="viewport" content="user-scalable=yes"/>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" href="http://www.spark.co.nz/etc/designs/spark-responsive/clientlib-responsive.css"/>
        <link rel="stylesheet" href="http://www.spark.co.nz/etc/designs/spark-responsive/clientlib-responsive2.css"/>
        <link rel="stylesheet" href="http://www.spark.co.nz/content/dam/telecomcms/responsive/css/responsive-addon.css" type="text/css">
        
        <script type="text/javascript" src="http://www.spark.co.nz/etc/designs/spark-responsive/clientlib-responsive.js"></script>
        <script type="text/javascript" src="http://www.spark.co.nz/content/dam/telecomcms/responsive/js/responsive-addon.js"></script>


        <!-- Additional style for Mobile Re Eng configurator -->
        <link rel="stylesheet" href="../css/configurator/mobile-configurator.css" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../css/lib/datepicker.css"/>

        <!-- Additional font CSS for local renders -->
        <link rel="stylesheet" href="../css/configurator/font-addon.css" type="text/css"/>
    </head>
<body class="re-eng page configurator-new-customer">

    <div id="header">
    <div class="floating-header-wrap">
        <!-- ************************ -->
        <!-- Skinned desktop nav here -->
        <!-- ************************ -->
        <nav id="desktop-header">   

            <div id="header-nav-wrap" class="ui-helper-clearfix">
                <div class="logo"> <a class="-parent" id="top-logo" href="/home/"> <img src="../images/logo-desk-pink.png"> </a> 
                        
                </div>
                <div id="navigation">
                    <div class="divisions">
                        <div class="toprightlinks">
                            <nav style="">
                                <ul>
                                    <li> <a href="http://www.sparknz.co.nz/"> Spark New Zealand </a> </li>
                                    <li> <a href="http://www.sparkdigital.co.nz"> Spark Digital </a> </li>
                                    <li> <a href="http://www.sparkventures.co.nz/"> Spark Ventures </a> </li>
                                    <li> <a href="http://www.sparkfoundation.org.nz/"> Spark Foundation </a> </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="headertabs">
                        <nav class="type">
                            <ul>
                                <li class="-parent menu-top-item active" id="menu-top-item1" data-topmenu_id="1"> <a class="-parent" href="/home/">Personal</a> </li>
                                <li class="-parent menu-top-item " id="menu-top-item2" data-topmenu_id="2"> <a class="-parent" href="/business/">Business</a> </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="parbase globalnavigationbar">
                        <nav class="primary show">
                            <ul class="primary-wrap">
                                <li class=" parent"> <a class="" href="/shop/">Shop</a>
                                    <ul class="products">
                                        <div class="arrow-down"></div>
                                        <li> <a href="/shop/mobile.html" class="icon circle ie-rounded"> <img src="http://www.spark.co.nz/etc/designs/telecomcms/resources/assets/Uploads/mobile-icon.png" alt="mobile-icon.png">
                                            
                                            </a>
                                            <ul class="popular">
                                                <h4>Mobile</h4>
                                                <li><a href="/shop/mobile/phones.html">View phones</a></li>
                                                <li><a href="/shop/mobile/mobile/plansandpricing.html">Plans &amp; pricing</a></li>
                                            </ul>
                                        </li>
                                        <li> <a href="/shop/internet/" class="icon circle ie-rounded"> <img src="http://www.spark.co.nz/etc/designs/telecomcms/resources/assets/Uploads/cloud-icon.png" alt="cloud-icon.png">
                                            
                                            </a>
                                            <ul class="popular">
                                                <h4>Internet</h4>
                                                <li><a href="/shop/internet/datacalculator/">Which broadband?</a></li>
                                                <li><a href="/shop/internet/">Plans &amp; pricing</a></li>
                                            </ul>
                                        </li>
                                        <li> <a href="/shop/landline/" class="icon circle ie-rounded"> <img src="http://www.spark.co.nz/content/dam/telecomcms/icons/phoneicon.png">
                                            
                                            </a>
                                            <ul class="popular">
                                                <h4>Landline</h4>
                                                <li><a href="/shop/landline/homephones/">Our home phones</a></li>
                                                <li><a href="/shop/landline/pricing/">Plans &amp; pricing</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class=" "> <a class="" href="/discover/">Discover</a> </li>
                                <li class=" "> <a class="" href="/help/">Help</a> </li>
                                <li class=" "> <a class="" href="/myspark/">MySpark</a> </li>
                                <li class="search">
                                    <form action="http://search.spark.co.nz/search" method="get" name="site-search">
                                        <div class="input-append"> 
                                            <!-- Spark Search Vars -->
                                            <div id="telecom-search-vars" class="hide">
                                                <input name="site" value="spark_all_collection">
                                                <input name="client" value="spark_frontend">
                                                <input name="output" value="xml_no_dtd">
                                                <input name="proxystylesheet" value="spark_frontend">
                                                <input name="filter" value="1">
                                            </div>
                                            <!-- Search Field -->
                                            <input type="text" placeholder="Search" name="q" id="appendedInputButton" class="span2">
                                            <!-- Search Button -->
                                            <button type="submit" class="btn"><i class="icon-search"></i></button>
                                        </div>
                                    </form>
                                </li>
                                <li><a href="/contactus/">Contact</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
           
        </nav>          
    </div>