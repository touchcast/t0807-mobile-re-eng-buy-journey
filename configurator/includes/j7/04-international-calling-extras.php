<!--Bundled Extras goes here -->

<div class="card primary">	
	<div class="inner-card" style="padding-bottom:0; margin-bottom:0">
		<a href="#" class="float-right">
			Close <i class="icon-x"></i>
		</a>
		<h2 class="card-title underlined" style="padding-bottom:0; margin-bottom:0">International calling Extras</h2>
							
	</div>	

	<div class="carousel device price white-card " data-carousel-name="carousel-price-01" data-items-lg="2" data-items-md="2">
		<!-- carousel panel start -->
		<div>
		
				<div class="clearfix device-carousel-card">

					<div class="header">
						<h4>$9 Aussie Pack</h4>
						<!-- <p>16GB space grey</p> -->
					</div>
					<hr>

					<div class="service-info">
					    <div class="service-price">
							<span class="number" data-pre="$">9</span>
							<span class="txt">/MTH</span>
						</div>
					    <div class="arrow-down"></div>
					</div>

					<button type="button" class="slim blue">Select Extra</button>

					<table class="detail-table">
					    <tbody>
					        <tr>
					            <th class="left">
					            	<h4>WHERE</h4>
					            </th>

					            <td class="right">
					                <img class="flag" src="../images/config/flag/Australia.png" alt="">
					            </td>
					        </tr>


					        <tr>
					            <th class="left">
					            	<h4>TALK</h4>
					            </th>

					            <td class="right">
					                200 mins
					            </td>
					        </tr>


					        <tr>
					           <th class="left">
					            	<h4>TEXT</h4>
					            </th>

					            <td class="right">
					            	100 texts
					            </td>
					        </tr>


					    </tbody>
					</table>

				</div>
		</div>
		<!-- carousel panel end -->


<!-- carousel panel start -->
		<div>
		
				<div class="clearfix device-carousel-card">

					<div class="header">
						<h4>$9 China Pack</h4>
						<!-- <p>16GB space grey</p> -->
					</div>
					<hr>

					<div class="service-info">
					    <div class="service-price">
							<span class="number" data-pre="$">9</span>
							<span class="txt">/MTH</span>
						</div>
					    <div class="arrow-down"></div>
					</div>

					<button type="button" class="slim blue">Select Extra</button>

					<table class="detail-table">
					    <tbody>
					        <tr>
					            <th class="left">
					            	<h4>WHERE</h4>
					            </th>

					            <td class="right">
					                <img class="flag" src="../images/config/flag/China.png" alt="">
					                <img class="flag" src="../images/config/flag/Hong%20Kong.png" alt="">
					            </td>
					        </tr>


					        <tr>
					            <th class="left">
					            	<h4>TALK</h4>
					            </th>

					            <td class="right">
					                200 mins
					            </td>
					        </tr>


					        <tr>
					           <th class="left">
					            	<h4>TEXT</h4>
					            </th>

					            <td class="right">
					            	100 texts
					            </td>
					        </tr>


					    </tbody>
					</table>

				</div>
		</div>
		<!-- carousel panel end -->


<!-- carousel panel start -->
		<div>
		
				<div class="clearfix device-carousel-card">

					<div class="header">
						<h4>$9 UK Pack</h4>
						<!-- <p>16GB space grey</p> -->
					</div>
					<hr>

					<div class="service-info">
					    <div class="service-price">
							<span class="number" data-pre="$">9</span>
							<span class="txt">/MTH</span>
						</div>
					    <div class="arrow-down"></div>
					</div>

					<button type="button" class="slim blue">Select Extra</button>

					<table class="detail-table">
					    <tbody>
					        <tr>
					            <th class="left">
					            	<h4>WHERE</h4>
					            </th>

					            <td class="right">
					               <img class="flag" src="../images/config/flag/United%20Kingdom(Great%20Britain).png" alt="">
					            </td>
					        </tr>


					        <tr>
					            <th class="left">
					            	<h4>TALK</h4>
					            </th>

					            <td class="right">
					                200 mins
					            </td>
					        </tr>


					        <tr>
					           <th class="left">
					            	<h4>TEXT</h4>
					            </th>

					            <td class="right">
					            	100 texts
					            </td>
					        </tr>


					    </tbody>
					</table>

				</div>
		</div>
		<!-- carousel panel end -->



	





	</div>

	<div class="inner-card" style="padding-top:0;">
		<div class="table-row gray">
			<a href="#">Terms &amp; conditions</a>
		</div>
	</div>
	

</div>

