<!--Bundled Extras goes here -->

<div class="card primary">	
	<div class="inner-card" style="padding-bottom:0; margin-bottom:0">
		<a href="#" class="float-right">
			Close <i class="icon-x"></i>
		</a>
		<h2 class="card-title underlined" style="padding-bottom:0; margin-bottom:0">Bundled Extras</h2>
							
	</div>	

	<div class="carousel device price white-card " data-carousel-name="carousel-price-01" data-items-lg="2" data-items-md="2">
		<!-- carousel panel start -->
		<div>
		
				<div class="clearfix device-carousel-card">

					<div class="header">
						<h4>$19 Aussie Roaming</h4>
						<!-- <p>16GB space grey</p> -->
					</div>
					<hr>

					<div class="service-info">
					    <div class="service-price">
							<span class="number" data-pre="$">19</span>
							<span class="txt">/MTH</span>
						</div>
					    <div class="arrow-down"></div>
					</div>

					<button type="button" class="slim blue">Add Extra</button>

					<table class="detail-table">
					    <tbody>
					        <tr>
					            <th class="left">
					            	<h4>TALK</h4>
					            </th>

					            <td class="right">
					                50 outgoing mins <br>
					                50 incoming mins
					            </td>
					        </tr>


					        <tr>
					            <th class="left">
					            	<h4>TEXT</h4>
					            </th>

					            <td class="right">
					                50 roaming texts
					            </td>
					        </tr>


					        <tr>
					           <th class="left">
					            	<h4>DATA</h4>
					            </th>

					            <td class="right">
					            	250 MB roaming
					            </td>
					        </tr>


					    </tbody>
					</table>

				</div>
		</div>
		<!-- carousel panel end -->

		<!-- carousel panel start -->
		<div>
		
				<div class="clearfix device-carousel-card">

					<div class="header">
						<h4>$39 Aussie Roaming</h4>
						<!-- <p>16GB space grey</p> -->
					</div>
					<hr>

					<div class="service-info">
					    <div class="service-price">
							<span class="number" data-pre="$">39</span>
							<span class="txt">/MTH</span>
						</div>
					    <div class="arrow-down"></div>
					</div>

					<button type="button" class="slim blue">Add Extra</button>

					<table class="detail-table">
					    <tbody>
					        <tr>
					            <th class="left">
					            	<h4>TALK</h4>
					            </th>

					            <td class="right">
					                100 outgoing mins <br>
					                100 incoming mins
					            </td>
					        </tr>


					        <tr>
					            <th class="left">
					            	<h4>TEXT</h4>
					            </th>

					            <td class="right">
					                100 roaming texts
					            </td>
					        </tr>


					        <tr>
					           <th class="left">
					            	<h4>DATA</h4>
					            </th>

					            <td class="right">
					            	500 MB roaming
					            </td>
					        </tr>


					    </tbody>
					</table>

				</div>
		</div>
		<!-- carousel panel end -->


		<!-- carousel panel start -->
		<div>
		
				<div class="clearfix device-carousel-card">

					<div class="header">
						<h4>$49 Aussie Roaming</h4>
						<!-- <p>16GB space grey</p> -->
					</div>
					<hr>

					<div class="service-info">
					    <div class="service-price">
							<span class="number" data-pre="$">49</span>
							<span class="txt">/MTH</span>
						</div>
					    <div class="arrow-down"></div>
					</div>

					<button type="button" class="slim blue">Add Extra</button>

					<table class="detail-table">
					    <tbody>
					        <tr>
					            <th class="left">
					            	<h4>TALK</h4>
					            </th>

					            <td class="right">
					                150 outgoing mins <br>
					                150 incoming mins
					            </td>
					        </tr>


					        <tr>
					            <th class="left">
					            	<h4>TEXT</h4>
					            </th>

					            <td class="right">
					                150 roaming texts
					            </td>
					        </tr>


					        <tr>
					           <th class="left">
					            	<h4>DATA</h4>
					            </th>

					            <td class="right">
					            	750 MB roaming
					            </td>
					        </tr>


					    </tbody>
					</table>

				</div>
		</div>
		<!-- carousel panel end -->



	</div>

	<div class="inner-card" style="padding-top:0;">
		<div class="table-row gray">
			<a href="#">Terms &amp; conditions</a>
		</div>
	</div>
	

</div>

