<div class="card primary table clearfix ico-content-card">
					<div class="inner-card clearfix">
						<a href="#" class="float-right">
							Change <i class="icon-pencil"></i>
						</a>
						<h2>Here's what you're connecting</h2>
						<!-- Prepaid Value Pack table -->
						<div class="row">
							<div class="col-xs-12">
								<div class="table-row-divider">
									<div class="table-row gray">
										<div class="row">
											<div class="col-xs-5 margin-bottom">
                                                <div class="product-image-float">
                                                    <div class="device-price-white">
                                                        <span class="number" data-pre="$">99</span>
                                                        <span class="txt">/MTH</span>
                                                    </div>
                                                </div>
                                            </div>
											<div class="col-xs-7">
												<strong>24 month Ultra Mobile $99</strong>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-xs-12">
								<div class="table-row-divider">
									<div class="table-row gray">
										<div class="row">
											<div class="col-xs-5">
											&nbsp;
											</div>
											<div class="col-xs-7">
												<strong>Also included with this plan</strong>
												<div class="row margin-bottom">
													<div class="col-xs-4"><img src="../images/config/Thanks.png" alt="Thanks" class="thanks-offer"></div>
													<div class="col-xs-4"><div class="wifi-offer wifi-offer-new"><img src="../images/config/Wifi.png" alt="Thanks"><strong> 1GB WiFi/day</strong></div></div>
													<div class="col-xs-4"><img src="../images/config/Spotify.png" alt="Spotify" class="spotify-offer"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- <div><br /><br /><br /></div> -->
						<!-- // -->
					</div>
				</div>