<!--Bundled Extras goes here -->

<div class="card primary">	
	<div class="inner-card" style="padding-bottom:0; margin-bottom:0">
		<a href="#" class="float-right">
			Close <i class="icon-x"></i>
		</a>
		<h2 class="card-title underlined" style="padding-bottom:0; margin-bottom:0">Data Extras</h2>
							
	</div>	

	<div class="carousel device price white-card " data-carousel-name="carousel-price-01" data-items-lg="2" data-items-md="2">


		<!-- carousel panel start -->
		<div>
		
				<div class="clearfix device-carousel-card" >

					<div class="header">
						<h4>$15 Monthly Data</h4>
						<!-- <p>16GB space grey</p> -->
					</div>
					<hr>

					<div class="service-info">
					    <div class="service-price">
							<span class="number" data-pre="">500</span>
							<span class="txt">MB</span>
						</div>
					    <div class="arrow-down"></div>
					</div>

					<button type="button" class="slim blue" >Your Free Gift</button>

					

				</div>
		</div>
		<!-- carousel panel end -->

		<!-- carousel panel start -->
		<div>
		
				<div class="clearfix device-carousel-card">

					<div class="header">
						<h4>$29 Monthly Data</h4>
						<!-- <p>16GB space grey</p> -->
					</div>
					<hr>

					<div class="service-info">
					    <div class="service-price">
							<span class="number" data-pre="">1.5</span>
							<span class="txt">GB</span>
						</div>
					    <div class="arrow-down"></div>
					</div>

					<button type="button" class="slim blue">Add Extra</button>

					

				</div>
		</div>
		<!-- carousel panel end -->

		<!-- carousel panel start -->
		<div>
		
				<div class="clearfix device-carousel-card">

					<div class="header">
						<h4>$39 Monthly Data</h4>
						<!-- <p>16GB space grey</p> -->
					</div>
					<hr>

					<div class="service-info">
					    <div class="service-price">
							<span class="number" data-pre="">2.5</span>
							<span class="txt">GB</span>
						</div>
					    <div class="arrow-down"></div>
					</div>

					<button type="button" class="slim blue">Add Extra</button>

					

				</div>
		</div>
		<!-- carousel panel end -->





	</div>

	<div class="inner-card" style="padding-top:0;">
		<div class="table-row gray">
			<a href="#">Terms &amp; conditions</a>
		</div>
	</div>
	

</div>

