<div class="card primary table clearfix ico-content-card">
	<div class="inner-card clearfix">
		<a href="#" class="float-right">
			Change <i class="icon-pencil"></i>
		</a>
		<h2>Here's what you're connecting</h2>
		<!-- Prepaid Value Pack table -->
		<div class="row">
			<div class="col-xs-12">
				<div class="table-row-divider">
					<div class="gray">
						<div class="row">
							<div class="col-xs-4">
								<div class="product-image-float">
                                        <div class="device-price-white small-price">
                                            <span data-pre="$" class="number">99</span>
                                            <span class="txt">/MTH</span>
                                        </div>
                                    </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="table-row-divider">
					<div class="table-row gray">
						<div class="row">
							<div class="col-xs-4">
							&nbsp;
							</div>
							<div class="col-xs-8">
								<strong>$24 month Ultra Mobile $99</strong>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="table-row-divider">
					<div class="table-row gray">
						<div class="row">
							<div class="col-xs-4">
							&nbsp;
							</div>
							<div class="col-xs-8">
								<strong>Also included with this plan</strong>
								<div class="row">
									<div class="col-xs-4"><img src="../images/config/Thanks.png" alt="Thanks" class="thanks-offer"></div>
									<div class="col-xs-4"><div class="wifi-offer"><img src="../images/config/Wifi.png" alt="Thanks"><strong> 1GB WiFi/day</strong></div></div>
									<div class="col-xs-4"><img src="../images/config/Spotify.png" alt="Spotify" class="spotify-offer"></div>
								</div>
								<!-- For double up test: This is to be removed after testing-->
								<!-- <div class="row">
									<div class="col-xs-4"><img src="../images/config/Thanks.png" alt="Thanks" class="thanks-offer"></div>
									<div class="col-xs-4"><div class="wifi-offer"><img src="../images/config/Wifi.png" alt="Thanks"><strong> 1GB WiFi/day</strong></div></div>
									<div class="col-xs-4"><img src="../images/config/Spotify.png" alt="Spotify" class="spotify-offer"></div>
								</div> -->
								<!-- ****************************************************-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- // -->
	</div>
</div>