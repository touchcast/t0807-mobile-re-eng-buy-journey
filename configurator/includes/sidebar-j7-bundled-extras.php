<!-- Sidebar specific style is now part of mobile-reeng css-->


<!-- ***************-->
<!-- Side bar starts-->
<!-- ***************-->
<div class="summary-header-block-1 sidebar-float">
	<div class="row sidebar-float-body">
		<div class="sidebar-float-hide">
			<!-- original spark title -->
			<div class="content-block-header with-line">
            	<h2 class="block"><span>Summary</span></h2>
            </div>
           
		</div>
	</div>

</div>

<div class="sidebar-wrapper sidebar-float sidebar-float-context">
    <div class="row sidebar-float-body">
        <div class="cartsummary">
            <div class="sidebar" id="sidebar-affix">
                <div class="summary-header-block-2 content-block-header with-line">
                    <h2 class="block"><span>Summary</span></h2>
                </div>


                <div>
                    <!-- Sidebar floater -->
                    <!-- Todo: write JS for the floater to float along the page -->

                    <div class="sidebar" id="sidebar-affix">
                        <!-- <div class="content-block-header with-line">
                        <h2 class="block"><span>Summary</span></h2>
                    </div> -->

                        <div class="sidebar-floater">
                            <div class="accord-wrapper">
                                <div class="accord-container">
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default extend" id="panel1">
                                            <div class="panel-heading extend">
                                                <div class="panel-title">
                                                    <a class="panel-toggle collapsed" data-parent="#accordion" data-toggle="collapse" href="#collapseOne">
                                                    <div class="accord-menu">
                                                        <div class="accord-col-1">
                                                            <h5>Pay monthly plan</h5>
                                                        </div>

                                                        <div class="accord-col-2">
                                                            $99.00 /mth
                                                        </div>
                                                    </div></a>
                                                </div>
                                            </div>

                                            <div class="panel-collapse collapse" id="collapseOne">
                                                <div class="panel-body table">
                                                    <div class="table-card white-table-card non-res">
                                                 

                                                        <table cellpadding="0" cellspacing="0" class="table">
                                                            <tbody>
                                                                

                                                                <tr>
                                                                    <td>24 month <br/>Ultra Mobile $99</td>

                                                                    <td><strong>$99.00</strong> /mth</td>
                                                                </tr>

                                                               
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default extend" id="panel2">
                                            <div class="panel-heading extend">
                                                <div class="panel-title">
                                                    <a class="panel-toggle collapsed" data-parent="#accordion" data-toggle="collapse" href="#collapseTwo">
                                                    <div class="accord-menu">
                                                        <div class="accord-col-1">
                                                            <h5>Mobile Extras</h5>
                                                        </div>

                                                        <div class="accord-col-2">
                                                            $24.00 /mth
                                                        </div>
                                                    </div></a>
                                                </div>
                                            </div>

                                            <div class="panel-collapse collapse" id="collapseTwo">
                                                <div class="panel-body table">
                                                    <div class="table-card white-table-card non-res">
                                                 

                                                        <table cellpadding="0" cellspacing="0" class="table">
                                                            <tbody>
                                                                

                                                                <tr>
                                                                    <td>500MB data pack</td>

                                                                    <td><strong>$15.00</strong> /mth</td>
                                                                </tr>

                                                                <tr>
                                                                    <td>Aussie pack</td>

                                                                    <td><strong>$9.00</strong> /mth</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- Total -->


                                    <div class="total-container stop-here">
                                        <div class="row">
                                            <div class="col-xs-12 col-h3 pull-right">
                                                <h3>Total<br></h3>
                                                <!-- <span class="grey-text_c">Including promotional credits</span></h3> -->

                                                <div class="table-row">
                                                    <div class="row">
                                                        <div class="col-xs-8">
                                                            <strong>Monthly plan payments</strong>
                                                        </div>

                                                        <div class="col-xs-4">
                                                            <div class="text-right">
                                                                <strong>$123.00</strong>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- <div class="table-row">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <strong>Credit on your account</strong>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <div class="text-right ">
                                                            <strong><span class="green-txt">$100.00 CR</span></strong>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- End of container -->

                                <div class="blue-bg blue-button-under-card side">
                                    <div class="col-xs-12 text-center">
                                        <button class="slim blue add-to-cart" type="button">Add to cart</button>
                                    </div>

                                    <div class="col-xs-12 text-center">
                                        <button class="slim secondary blue" type="button">Cancel</button>
                                    </div>

                                    <div class="clearfix"></div>
                                      
                                </div>
                           
                            </div>
                        </div>
                    </div>
                  
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ***************-->
<!-- Side bar ends -->
<!-- ***************-->
