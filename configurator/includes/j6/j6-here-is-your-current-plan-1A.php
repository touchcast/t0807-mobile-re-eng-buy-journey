<div class="card primary table clearfix ico-content-card">
					<div class="inner-card clearfix">
		
						<h2>Here's your current mobile plan</h2>
						<!-- Prepaid Value Pack table -->
						<div class="row">
							<div class="col-xs-12">
								<div class="table-row-divider">
									<div class="table-row gray">
										<div class="row">
											<div class="col-xs-3">
                                                Mobile number:
                                            </div>
											<div class="col-xs-9">
												027 123 4561
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-xs-12">
								<div class="table-row-divider">
									<div class="table-row gray">
										<div class="row">
											<div class="col-xs-3">
											Current plan:
											</div>
											<div class="col-xs-9">
												24 month Ultra Mobile $39<br/>
												<!-- <strong>Also included with this plan</strong> -->
												<div class="row margin-top margin-bottom">
													<div class="col-xs-4"><b>TALK</b>: Unlimited<br/><img src="../images/config/Thanks.png" alt="Thanks" class="thanks-offer"></div>
													<div class="col-xs-4"><b>TEXT</b>: Unlimited<br/><div class="wifi-offer wifi-offer-new"><img src="../images/config/Wifi.png" alt="Thanks"><strong> 1GB WiFi /day</strong></div></div>
													<div class="col-xs-4"><b>DATA</b>: 2.5GB<br/><img src="../images/config/Spotify.png" alt="Spotify" class="spotify-offer"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12">
								<div class="table-row-divider">
									<div class="table-row gray">
										<div class="row">
											<div class="col-xs-3">
											Plan end date:
											</div>
											<div class="col-xs-9">
												17/01/2015
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<p class="margin-top"><a>Not the right number? Enter it here</a></p>
					</div>
				</div>