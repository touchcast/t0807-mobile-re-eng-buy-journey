<div class="row">
	<div class="col-sm-12 card" style="visibility: visible;">
		
		<ul id="myResponsiveTab" class="nav nav-tabs nav-justified responsive-tab">


	        <li>
	        	<a href="#profile1" data-toggle="tab">
	        		<h4>
			            <span class="visible-xs">Open Term</span>
			            <span class="visible-sm">Open Term<br><sub style="text-transform: none; margin: 8px 0;font-size:13px; font-family: AvenirNextLTPro-Demi, Calibri;">Perfect if you already have a phone</sub></span>
			            <span class="visible-md visible-lg">Open Term<br><sub style="text-transform: none; margin: 8px 0;font-size:13px; font-family: AvenirNextLTPro-Demi, Calibri;">Perfect if you already have a phone</sub></span>
			        </h4>
		        </a>
		    </li>
	        
	     	 <li class="active">
	     	 	<a href="#coffee1" data-toggle="tab">
	     	 		<h4>
			            <span class="visible-xs">24 Month term</span>
			            <span class="visible-sm">24 Month term<br><sub style="text-transform: none; margin: 8px 0;font-size:13px; font-family: AvenirNextLTPro-Demi, Calibri;">Get savings upfront on a new phone</sub></span>
			            <span class="visible-md visible-lg">24 Month term<br><sub style="text-transform: none; margin: 8px 0;font-size:13px; font-family: AvenirNextLTPro-Demi, Calibri;">Get savings upfront on a new phone</sub></span>
			        </h4>
	     	 	</a>
	     	</li>
	    </ul>
	    <div id="myResponsiveTabContent" class="tab-content" style="padding:0; margin:0; background:white">
	      
	        <div class="tab-pane fade " id="profile1" >
	           

	          <!-- Start carousel -->

					<div class="carousel device price white-card " data-carousel-name="carousel-price-01" data-items-lg="2" data-items-md="2">


						<!-- carousel panel start -->
						<div>
						
								<div class="clearfix device-carousel-card">

									<div class="service-info">
									    <div class="service-price">
											<span class="number" data-pre="$">19</span>
											<span class="txt">/MTH</span>
										</div>
									    <div class="arrow-down"></div>
									</div>

									

									<button type="button" class="slim blue">Select plan</button>

									<table class="detail-table">
									    <tbody>
									        <tr>
									            <th class="left">
									            	<h4>TALK</h4>
									            </th>

									            <td class="right">
									                300 mins
									            </td>
									        </tr>


									        <tr>
									            <th class="left">
									            	<h4>TEXT</h4>
									            </th>

									            <td class="right">
									                <a href="#">Unlimited</a> to any NZ network.
									            </td>
									        </tr>


									        <tr>
									           <th class="left">
									            	<h4>DATA</h4>
									            </th>

									            <td class="right">
									            	250 MB roaming
									            </td>
									        </tr>

									        <tr>
									            <th class="left"><img src="//spark.co.nz/content/dam/telecomcms/responsive/images/shop/mobile/plans-pricing/wifi-icon.svg">
									            </th>

									            <td class="right">1 GB WiFi /day<br></td>
									        </tr>


									        <tr>
									            <th class="left">
									            	<img src="//spark.co.nz/content/dam/telecomcms/responsive/images/shop/mobile/plans-pricing/thanks-icon.svg">
									            </th>

									            <td class="right">Thanks Perks<br></td>
									        </tr>
								             <tr>         
								             	<th class="left">
								             		<img src="//spark.co.nz/content/dam/telecomcms/responsive/images/shop/mobile/plans-pricing/music-icon.svg">
								             	</th>          
								             	<td class="right">Spotify Premium</td>     
								             </tr> 


									    </tbody>
									</table>

								</div>
						</div>
						<!-- carousel panel end -->
						
						<!-- carousel panel start -->
						<div>
						
								<div class="clearfix device-carousel-card">

									<div class="service-info">
									    <div class="service-price">
											<span class="number" data-pre="$">39</span>
											<span class="txt">/MTH</span>
										</div>
									    <div class="arrow-down"></div>
									</div>

									<div class="special-offer-banner">     
										<h5>PERFECT<br>FOR YOU</h5>
									</div>

									<button type="button" class="slim blue">Select plan</button>

									<table class="detail-table">
									    <tbody>
									        <tr>
									            <th class="left">
									            	<h4>TALK</h4>
									            </th>

									            <td class="right">
									                50 outgoing mins <br>
									                50 incoming mins
									            </td>
									        </tr>


									        <tr>
									            <th class="left">
									            	<h4>TEXT</h4>
									            </th>

									            <td class="right">
									                50 roaming texts
									            </td>
									        </tr>


									        <tr>
									           <th class="left">
									            	<h4>DATA</h4>
									            </th>

									            <td class="right">
									            	250 MB roaming
									            </td>
									        </tr>


									    </tbody>
									</table>

								</div>
						</div>
						<!-- carousel panel end -->

						<!-- carousel panel start -->
						<div>


						
								<div class="clearfix device-carousel-card">

									<div class="service-info">
										<div class="service-price">
											<span data-pre="$" class="number">59</span>
											<span class="txt">/MTH</span>
										</div>
										<div class="arrow-down"></div>
										<div class="yellow-note clearfix">
											<div><h5>MOST POPULAR</h5></div>
											<div class="arrow-down"></div>
										</div>
									</div>

									<button type="button" class="slim blue">Select plan</button>

									<table class="detail-table">
									    <tbody>
									        <tr>
									            <th class="left">
									            	<h4>TALK</h4>
									            </th>

									            <td class="right">
									                50 outgoing mins <br>
									                50 incoming mins
									            </td>
									        </tr>


									        <tr>
									            <th class="left">
									            	<h4>TEXT</h4>
									            </th>

									            <td class="right">
									                50 roaming texts
									            </td>
									        </tr>


									        <tr>
									           <th class="left">
									            	<h4>DATA</h4>
									            </th>

									            <td class="right">
									            	250 MB roaming
									            </td>
									        </tr>


									    </tbody>
									</table>

								</div>
						</div>
						<!-- carousel panel end -->
				
					</div>


	           <!-- End carousel -->	

					<div class="text-center inner-card">
					
							<p><a href="#">Additional charges and details</a></p>
							<p><a href="#">Unlimited usage policy</a></p>
							
					</div>

	        </div>
	        <div class="tab-pane fade in active" id="coffee1">
	            
	            <!-- Start carousel -->

					<div class="carousel device price white-card " data-carousel-name="carousel-price-01" data-items-lg="2" data-items-md="2">


						<!-- carousel panel start -->
						<div>
						
								<div class="clearfix device-carousel-card disabled">

									<div class="service-info">
									    <div class="service-price">
											<span class="number" data-pre="$">39</span>
											<span class="txt">/MTH</span>
										</div>
									    <div class="arrow-down"></div>
									    <div class="yellow-note clearfix">
											<div><h5>MOST POPULAR</h5></div>
											<div class="arrow-down"></div>
										</div>
									</div>

									<!-- <button type="button" class="slim blue">Added to cart</button> -->
									<button type="button" class="slim blue">Current plan</button>

									<table class="detail-table">
									    <tbody>
									        <tr>
									            <th class="left">
									            	<h4>TALK</h4>
									            </th>

									            <td class="right">
									               300 Mins
									            </td>
									        </tr>


									        <tr>
									            <th class="left">
									            	<h4>TEXT</h4>
									            </th>

									            <td class="right">
									                <a href="#">Unlimited</a> to any <br>NZ network.
									            </td>
									        </tr>


									        <tr>
									           <th class="left">
									            	<h4>DATA</h4>
									            </th>

									            <td class="right">
									            	1 GB
									            </td>
									        </tr>

									        <tr>
									            <th class="left"><img src="//spark.co.nz/content/dam/telecomcms/responsive/images/shop/mobile/plans-pricing/wifi-icon.svg">
									            </th>

									            <td class="right">1 GB WiFi /day<br></td>
									        </tr>


									        <tr>
									            <th class="left">
									            	<img src="//spark.co.nz/content/dam/telecomcms/responsive/images/shop/mobile/plans-pricing/thanks-icon.svg">
									            </th>

									            <td class="right">Thanks Perks<br></td>
									        </tr>
								             

									    </tbody>
									</table>

								</div>
						</div>
						<!-- carousel panel end -->

						<!-- carousel panel start -->
						<div>
						
								<div class="clearfix device-carousel-card selected">

									<div class="service-info">
									    <div class="service-price">
											<span class="number" data-pre="$">59</span>
											<span class="txt">/MTH</span>
										</div>
									    <div class="arrow-down"></div>
									</div>

								
									<button type="button" class="slim blue">Added to cart</button>

									<table class="detail-table">
									    <tbody>
									        <tr>
									            <th class="left">
									            	<h4>TALK</h4>
									            </th>

									            <td class="right">
									               <a href="#">Unlimited</a> to any <br>NZ/Aus Phone.
									            </td>
									        </tr>


									        <tr>
									            <th class="left">
									            	<h4>TEXT</h4>
									            </th>

									            <td class="right">
									                <a href="#">Unlimited</a> to any <br>NZ network.
									            </td>
									        </tr>


									        <tr>
									           <th class="left">
									            	<h4>DATA</h4>
									            </th>

									            <td class="right">
									            	1 GB
									            </td>
									        </tr>

									        <tr>
									            <th class="left"><img src="//spark.co.nz/content/dam/telecomcms/responsive/images/shop/mobile/plans-pricing/wifi-icon.svg">
									            </th>

									            <td class="right">1 GB WiFi /day<br></td>
									        </tr>


									        <tr>
									            <th class="left">
									            	<img src="//spark.co.nz/content/dam/telecomcms/responsive/images/shop/mobile/plans-pricing/thanks-icon.svg">
									            </th>

									            <td class="right">Thanks Perks<br></td>
									        </tr>
								             <tr>         
								             	<th class="left">
								             		<img src="//spark.co.nz/content/dam/telecomcms/responsive/images/shop/mobile/plans-pricing/music-icon.svg">
								             	</th>          
								             	<td class="right">Spotify Premium</td>     
								             </tr> 


									    </tbody>
									</table>


								</div>
						</div>
						<!-- carousel panel end -->

						<!-- carousel panel start -->
						<div>
						
								<div class="clearfix device-carousel-card">

									<div class="service-info">
									    <div class="service-price">
											<span class="number" data-pre="$">79</span>
											<span class="txt">/MTH</span>
										</div>
									    <div class="arrow-down"></div>
									</div>

									<button type="button" class="slim blue">Select plan</button>

									<table class="detail-table">
									    <tbody>
									        <tr>
									            <th class="left">
									            	<h4>TALK</h4>
									            </th>

									            <td class="right">
									                50 outgoing mins <br>
									                50 incoming mins
									            </td>
									        </tr>


									        <tr>
									            <th class="left">
									            	<h4>TEXT</h4>
									            </th>

									            <td class="right">
									                50 roaming texts
									            </td>
									        </tr>


									        <tr>
									           <th class="left">
									            	<h4>DATA</h4>
									            </th>

									            <td class="right">
									            	250 MB roaming
									            </td>
									        </tr>


									    </tbody>
									</table>

								</div>
						</div>
						<!-- carousel panel end -->

				
					</div>


	           <!-- End carousel -->	
	          		<div class="text-center inner-card">
					
							<p><a href="#">Additional charges and details</a></p>
							<p><a href="#">Unlimited usage policy</a></p>
							
					</div>			           
	        </div>

	        <div class="clearfix"></div>
	        
	    </div>


	</div>
</div>