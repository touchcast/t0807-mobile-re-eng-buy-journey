<div class="card primary table clearfix ico-content-card">
					<div class="inner-card clearfix">
		
						<h2>Here's your current mobile plan</h2>
						<!-- Prepaid Value Pack table -->
						<div class="row">
							<div class="col-xs-12">
								<div class="table-row-divider">
									<div class="row">
										<div class="col-xs-12">
											<p class="info-box yellow margin-bottom note-box">
				                                <strong>Looks like you are unable to change the plan online. </strong>Call us now on <a class="link-bg">0800 Business</a>
				                            </p>
			                            </div>
									</div>
									<div class="table-row gray">
										
										<div class="row">
											<div class="col-xs-3">
                                                Mobile number:
                                            </div>
											<div class="col-xs-9">
												027 123 4561
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-xs-12">
								<div class="table-row-divider">
									<div class="table-row gray">
										<div class="row">
											<div class="col-xs-3">
											Current plan:
											</div>
											<div class="col-xs-9">
												<span class="margin-bottom">12 month Ultra Business $39</span>
												<!-- <strong>Also included with this plan</strong> -->
												<div class="row margin-top margin-bottom">
													<div class="col-xs-4"><b>TALK</b>: 300</div>
													<div class="col-xs-4"><b>TEXT</b>: 1000</div>
													<div class="col-xs-4"><b>DATA</b>: 1GB</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						
						<p class="margin-top"><a>Not the right number? Enter it here</a></p>
					</div>
				</div>