<div class="card primary table clearfix ico-content-card">
					<div class="inner-card clearfix">
		
						<h2>Here's your current mobile plan</h2>
						<!-- Prepaid Value Pack table -->
						<div class="row">
							<div class="col-xs-12">
								<div class="table-row-divider">
									<div class="row">
											<div class="col-xs-12">
												<p class="info-box yellow margin-bottom note-box">
					                                <strong>Looking for more Prepaid data, talk or text?</strong> Prepaid extras is the way to go, <a class="link-bg">learn more.</a>
					                            </p>
				                            </div>
										</div>
									<div class="table-row gray">
										
										<div class="row">
											<div class="col-xs-3">
                                                Mobile number:
                                            </div>
											<div class="col-xs-9">
												027 123 4561
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-xs-12">
								<div class="table-row-divider">
									<div class="table-row gray">
										<div class="row">
											<div class="col-xs-3">
											Current plan:
											</div>
											<div class="col-xs-9">
												<span class="margin-bottom">Prepaid-casual rate</span>
												<!-- <strong>Also included with this plan</strong> -->
												<div class="row margin-top margin-bottom">
													<div class="col-xs-4"><b>TALK</b>: 49c/min</div>
													<div class="col-xs-4"><b>TEXT</b>: 20c/msg</div>
													<div class="col-xs-4"><b>DATA</b>: 10MB $1/day</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						
						<p class="margin-top"><a>Not the right number? Enter it here</a></p>
					</div>
				</div>