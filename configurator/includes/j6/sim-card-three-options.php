<div class="card clearfix secondary">
	<div class="inner-card clearfix">
		<div class="inner-wrapper">
			<a href="#" class="float-right">
				Close <i class="icon-x"></i>
			</a> 
			<h2 class="card-title underlined">SIM cards</h2>
			<div class="">
				<!-- <h4>Choose the contract you'd like</h4> -->

				<div class="row">
					<div class="col-xs-12">
						<input type="radio" value="radio0006" id="radio0006" name="radio002" class="normal">
						<label for="radio0006"><span></span><strong>Add Trio SIM card</strong> - 4G ready and works with any mobile phone</label>
					</div>
					<div class="col-xs-12">
						<input type="radio" value="radio0007" id="radio0007" name="radio002" class="normal">
						<label for="radio0007"><span></span><strong>Add Semble SIM card</strong> - NFC and 4G ready and works with any Android phone</label>
					</div>
					<div class="col-xs-12">
						<input type="radio" value="radio0008" id="radio0008" name="radio002" class="normal" checked>
						<label for="radio0008"><span></span><strong>Use my existing SIM card</strong></label>
					</div>
				</div>
			
			</div>
		</div>
	</div>
</div>
