<style>
	.sidebar-float {
	  height:1px;
	  position: fixed;
	  z-index: 9999;
	  top: 0;
	  left: 50%;
	  width: 940px;
	  margin-left: -480px;
	}
	.sidebar-float .sidebar-float-body {
	  float: right;
	  width: 32%;
	  background-color: #E3E3E3;
	}
	.sidebar-float .sidebar-float-hide { display: none; }
	.sidebar-float .sidebar-float-hide.float-toggle { display: block; }
	.sidebar-float.sidebar-float-context {
	  position: absolute;
	  z-index: 9998;
	  top: 228px;
	  height:1px;
	}
	.sidebar-float.sidebar-float-context.float-fixed {
	  position: fixed;
	}

	.stop-here-2 {
		margin-top: 0;
	}

	.summary-header-block-2,
	.sidebar-floater {
		-webkit-transform: translateZ(0);
		-webkit-backface-visibility: hidden;
		-webkit-perspective: 1000;
		z-index: 5;
	}
    .pull-right .table-row.gray { margin-top: 10px; }
</style>
<!-- ***************-->
<!-- Side bar starts-->
<!-- ***************-->
<div class="summary-header-block-1 sidebar-float">
	<div class="row sidebar-float-body">
		<div class="sidebar-float-hide">
			<!-- original spark title -->
			<div class="content-block-header with-line">
            	<h2 class="block"><span>Summary</span></h2>
            </div>
           
		</div>
	</div>

</div>

<div class="sidebar-wrapper sidebar-float sidebar-float-context">
    <div class="row sidebar-float-body">
        <div class="cartsummary">
            <div class="sidebar" id="sidebar-affix">
                <div class="summary-header-block-2 content-block-header with-line">
                    <h2 class="block"><span>Summary</span></h2>
                </div>


                <div>
                    <!-- Sidebar floater -->
                    <!-- Todo: write JS for the floater to float along the page -->

                    <div class="sidebar" id="sidebar-affix">
                        <!-- <div class="content-block-header with-line">
                        <h2 class="block"><span>Summary</span></h2>
                    </div> -->

                        <div class="sidebar-floater">
                            <div class="accord-wrapper">
                                <div class="accord-container">
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default extend" id="panel1">
                                            <div class="panel-heading extend">
                                                <div class="panel-title">
                                                    <a class="panel-toggle collapsed" data-parent="#accordion" data-toggle="collapse" href="#collapseOne">
                                                    <div class="accord-menu">
                                                        <div class="accord-col-1">
                                                            <h5>Pay monthly plan</h5>
                                                        </div>

                                                        <div class="accord-col-2">
                                                            $99.00/mth
                                                        </div>
                                                    </div></a>
                                                </div>
                                            </div>

                                            <div class="panel-collapse collapse" id="collapseOne">
                                                <div class="panel-body table">
                                                    <div class="table-card white-table-card non-res">
                                                 

                                                        <table cellpadding="0" cellspacing="0" class="table">
                                                            <tbody>
                                                                <tr>
                                                                    <td>24 month<br>Ultra Mobile $99</td>

                                                                    <td><strong>$99.00</strong>/mth</td>
                                                                </tr>

                                                                <tr>
                                                                    <td>Courier delivery - 3 to 5<br>business days from today</td>

                                                                    <td><strong>Free</strong></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- Total -->


                                    <div class="total-container stop-here">
                                        <div class="row">
                                            <div class="col-xs-12 col-h3 pull-right">
                                                <h3>Total</h3>
                                                <div class="table-row">
                                                    <div class="row">
                                                        <div class="col-xs-8">
                                                            <strong>Monthly plan payments</strong>
                                                        </div>

                                                        <div class="col-xs-4">
                                                            <div class="text-right">
                                                                <strong>$99.00</strong>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- End of container -->

                                <div class="blue-bg blue-button-under-card side">
                                    <div class="col-xs-12 text-center">
                                        <button class="slim blue add-to-cart" type="button">Add to cart</button>
                                    </div>

                                    <div class="col-xs-12 text-center">
                                        <button class="slim secondary blue" type="button">Cancel</button>
                                    </div>

                                    <div class="clearfix"></div>
                                      
                                </div>
                           
                            </div>
                        </div>
                    </div>
                  
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ***************-->
<!-- Side bar ends -->
<!-- ***************-->
