<div class="card primary table clearfix ico-content-card">
					<div class="inner-card clearfix">
		
						<h2>Here's your current mobile plan</h2>
						<!-- Prepaid Value Pack table -->
						<div class="row">
							<div class="col-xs-12">
								<div class="table-row-divider">
									<div class="table-row gray">
										<div class="row">
											<div class="col-xs-3">
                                                Mobile number:
                                            </div>
											<div class="col-xs-9">
												027 123 4561
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-xs-12">
								<div class="table-row-divider">
									<div class="table-row gray">
										<div class="row">
											<div class="col-xs-3">
											Current plan:
											</div>
											<div class="col-xs-9">
												24 month Talk & Text $39<br/>
												<!-- <strong>Also included with this plan</strong> -->
												<div class="row margin-top margin-bottom">
													<div class="col-xs-4"><b>TALK</b>: 300<br/></div>
													<div class="col-xs-4"><b>TALK</b>: 1000<br/></div>
													<div class="col-xs-4"><b>DATA</b>: 1GB<br/></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12">
								<div class="table-row-divider">
									<div class="table-row gray">
										<div class="row">
											<div class="col-xs-3">
											Plan end date:
											</div>
											<div class="col-xs-9">
												1/06/2016
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<p class="margin-top"><a>Not the right number? Enter it here</a></p>
					</div>
				</div>