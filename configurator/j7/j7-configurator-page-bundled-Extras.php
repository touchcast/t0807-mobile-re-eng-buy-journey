<?php include("../includes/header.php"); ?>
<?php include("../includes/sidebar-j7-bundled-extras.php"); ?>

<div class="container-fluid global-style">

	<?php include("../includes/page-header.php"); ?>

	<div class="inner-container">

		<br />

		<div class="row content-container">
			<div class="col-xs-8">
				<?php include("../includes/j1/content-block-header.php"); ?> 

				<!-- Device details -->
				<?php include("../includes/j7/j7-here-is-what-you-top-block.php"); ?>
				<!-- End: Device details -->

				

		
				<!-- **********
				Your mobile number
				********** -->
				
				<div class="card clearfix secondary">
					<div class="inner-card clearfix">
						<div class="inner-wrapper"> 
							<!-- <a href="#" class="float-right">Close <i class="icon-x"></i></a> -->
							<h2 class="card-title underlined">Your mobile number</h2>
							<div class="">
								<!-- <h4>Choose the contract you'd like</h4> -->

								<div class="row">
								<!-- Radio button -->
									
									<div class="col-xs-12">
										<input type="radio" value="radio0001" id="radio0001" name="radio000" checked class="normal">
										<label for="radio0001"><span></span>Use my Spark number</label>
									</div>
									<div class="col-xs-12">
										<input type="radio" value="radio0002" id="radio0002" name="radio000" class="normal">
										<label for="radio0002"><span></span>Give me a new number</label>
									</div>
									<div class="col-xs-12">
										<input type="radio" value="radio0003" id="radio0003" name="radio000" class="normal">
										<label for="radio0003"><span></span>Bring my number from another mobile provider</label>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>

				
				
				<!-- **********
				Data Extras
				********** -->

				
				<div class="card clearfix secondary">
					<div class="inner-card clearfix">
						<div class="inner-wrapper"> 
							<a href="#" class="float-right">
								Change <i class="icon-pencil"></i>
							</a>
							<h2 class="card-title underlined">Data Extras</h2>
							<div class="">
								<!-- <h4>Choose the contract you'd like</h4> -->

								<div class="clearfix">
									<ul>
										<li>
											<strong>500MB data pack</strong> - $15 /mth
										</li>
									</ul>
								
								</div>
							
							</div>
						</div>
					</div>
				</div>

				<!-- **********
				Intl calling extras
				********** -->

				<div class="card clearfix secondary">
					<div class="inner-card clearfix">
						<div class="inner-wrapper">
							<a href="#" class="float-right">
								Change <i class="icon-pencil"></i>
							</a>
							<h2 class="card-title underlined">International calling Extras</h2>
							<div class="">
								<!-- <h4>Choose the contract you'd like</h4> -->

								<div class="clearfix">
									<ul>
										<li>
											<strong>Aussie pack</strong> - $9 /mth
										</li>
									</ul>

								</div>
							
							</div>
						</div>
					</div>
				</div>

				<!-- **********
				Bundled Extras
				********** -->

				<?php include("../includes/j7/01-bundled-extras.php"); ?>

				<!-- **********
				Voice Extras
				********** -->

				<div class="card clearfix secondary">
					<div class="inner-card clearfix">
						<div class="inner-wrapper">
							<a href="#" class="float-right">
								Change <i class="icon-pencil"></i>
							</a>
							<h2 class="card-title underlined">Voice Extras</h2>
							<div class="">
								<!-- <h4>Choose the contract you'd like</h4> -->

								<div class="clearfix">
									<ul>
										<li>
											<strong>Not Selected</strong> - Why not bump up your plan with voice Extras?
										</li>
									</ul>

								</div>
							
							</div>
						</div>
					</div>
				</div>

				<!-- **********
				Voice Extras
				********** -->

				<div class="card clearfix secondary">
					<div class="inner-card clearfix">
						<div class="inner-wrapper">
							<a href="#" class="float-right">
								Change <i class="icon-pencil"></i>
							</a>
							<h2 class="card-title underlined">My Favourites</h2>
							<div class="">
								<!-- <h4>Choose the contract you'd like</h4> -->

								<div class="clearfix">
									<ul>
										<li>
											<strong>Not Selected</strong> - Pick your fave people you want to call any time and all the time.
										</li>
									</ul>

								</div>
							
							</div>
						</div>
					</div>
				</div>

			</div>
			<!-- End of left side of the page -->
			
			
		</div>

	</div>

</div>

<?php include("../includes/footer.php"); ?>