<?php include("../includes/your-cart-header.php"); ?>

    <div class="container-fluid global-style">

        <div class="page-header">
            <div class="center-wrap">
                <h1 class="set-up-header">Your cart</h1>
            </div>
        </div>

        <div class="inner-container">
            <br />

            <div class="row">
                <div class="col-xs-12">
                    <div class="content-block-header">
                        <h1 class="block"><span>Here's what's in your cart</span></h1>
                    </div>
                </div>  
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-xs-12">
                    <div class="card primary clearfix ico-content-card">
                        <div class="inner-card clearfix">
                            <a href="#" class="float-right">
                                Change <i class="icon-pencil"></i>
                                &nbsp;&nbsp;&nbsp;
                                Delete <i class="icon-trash-empty"></i>
                            </a>
                            <h2>24 month Ultra Mobile $99</h2>
                    
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="table-row-divider">
                                        <div class="table-row pad-table-row gray">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <div class="product-image-float">
                                                        <div class="device-price-white">
                                                            <span class="number" data-pre="$">99</span>
                                                            <span class="txt">/MTH</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4">
                                                    <strong>24 month Ultra Mobile $99</strong>
                                                    <!-- <ul>
                                                        <li>You are keeping your number <br>026 870 530</li>
                                                        <li>You are changing from (PLAN NAME)</li>
                                                    </ul> -->
                                                </div>
                                                <div class="col-xs-2">
                                                    Monthly
                                                </div>
                                                <div class="col-xs-2">
                                                    <div class="text-right">
                                                        <strong>$99.00</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="table-row-divider">
                                        <div class="table-row pad-table-row gray">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <div class="product-image-float">
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-xs-4">
                                                    <strong>Also included with this plan</strong><br/>
                                                    <img src="../images/config/config-v1-thanks.png" style="margin:10px 0 0 0;" />
                                                </div>
                                                <div class="col-xs-2">
                                                </div>
                                                <div class="col-xs-2">
                                                    <div class="text-right">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                              <div class="row">
                                <div class="col-xs-12">
                                    <div class="table-row-divider">
                                        <div class="table-row pad-table-row gray">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <div class="product-image-float">
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-xs-4">
                                                    <strong>Delivery</strong><br/>
                                                    <ul>
                                                        <li>
                                                            Courier delivery - 3 to 5 business days from today
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-xs-2">
                                                </div>
                                                <div class="col-xs-2">
                                                    <div class="text-right">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h2 class="mobile-extras">Mobile Extras</h2>
                    
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="table-row-divider">
                                        <div class="table-row pad-table-row gray">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    
                                                </div>
                                                <div class="col-xs-4">
                                                    <strong>500MB Data Pack</strong>
                                                    <!-- <ul>
                                                        <li>You are keeping your number <br>026 870 530</li>
                                                        <li>You are changing from (PLAN NAME)</li>
                                                    </ul> -->
                                                </div>
                                                <div class="col-xs-2">
                                                    Monthly
                                                </div>
                                                <div class="col-xs-2">
                                                    <div class="text-right">
                                                        <strong>$15.00</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>




                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="table-row-divider">
                                        <div class="table-row pad-table-row gray">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    
                                                </div>
                                                <div class="col-xs-4">
                                                    <strong>Aussie Pack</strong>
                                                    <!-- <ul>
                                                        <li>You are keeping your number <br>026 870 530</li>
                                                        <li>You are changing from (PLAN NAME)</li>
                                                    </ul> -->
                                                </div>
                                                <div class="col-xs-2">
                                                    Monthly
                                                </div>
                                                <div class="col-xs-2">
                                                    <div class="text-right">
                                                        <strong>$9.00</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="table-row-divider">
                                        <div class="table-row pad-table-row gray">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    
                                                </div>
                                                <div class="col-xs-4">
                                                    <strong>$19 Aussie Roaming Pack</strong>
                                                    <!-- <ul>
                                                        <li>You are keeping your number <br>026 870 530</li>
                                                        <li>You are changing from (PLAN NAME)</li>
                                                    </ul> -->
                                                </div>
                                                <div class="col-xs-2">
                                                    Monthly
                                                </div>
                                                <div class="col-xs-2">
                                                    <div class="text-right">
                                                        <strong>$19.00</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="table-row-divider">
                                        <div class="table-row pad-table-row gray">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    
                                                </div>
                                                <div class="col-xs-4">
                                                    <strong>$9 Talk</strong>
                                                    <!-- <ul>
                                                        <li>You are keeping your number <br>026 870 530</li>
                                                        <li>You are changing from (PLAN NAME)</li>
                                                    </ul> -->
                                                </div>
                                                <div class="col-xs-2">
                                                    Monthly
                                                </div>
                                                <div class="col-xs-2">
                                                    <div class="text-right">
                                                        <strong>$9.00</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-4 col-h3 pull-right">
                                    <h3 style="margin-top:12px;">Subtotal</h3>
                                    <div class="table-row">
                                        <div class="row">
                                            <div class="col-xs-8">
                                                <strong>Monthly plan payments</strong>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="text-right">
                                                <strong>$151.00</strong>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div> 
                                </div>
                            </div>
                            <!-- // -->


                        </div>
                    </div>
                </div>
            </div>


             <hr class="no-margin margin-bottom"/>

            <div class="row">
                <div class="col-xs-12">
                    <div class="content-block-header">
                        <h1 class="block"><span>Your cart summary</span></h1>
                    </div>
                </div>  
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-xs-12">
                    <div class="card primary clearfix ico-content-card">
                        <div class="inner-card clearfix">

                            <div class="cart-summary-row">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <span class="cart-summary-text">Monthly plan payments</span>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="cart-summary-text text-right">
                                            $151.00
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-8">
                                    &nbsp;
                                </div>
                                <div class="col-xs-4">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <a href="#" class="blue-button-float-right">
                                                <div class="inner-blue-button">Apply</div>
                                            </a>
                                            <input type="text" class="form-control form-control-with-blue-button" name="account_number" placeholder="Enter voucher code here">
                                        </div>
                                    </div>
                                    <div class="text-right margin-top">
                                        <a href="#">What is this?</a>
                                    </div>
                                    <div class="cart-summary-row top">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="cart-summary-text">
                                                    Total
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="cart-summary-text text-right">
                                                    $151.00
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                          
                        </div>

                        <div class="blue-bg blue-button-under-card">
                            <div class="col-xs-4 text-center">
                                <button type="button" class="normal secondary blue">Keep shopping</button>
                            </div>
                            <div class="col-xs-4 text-center">
                                <button type="button" class="normal secondary blue">Save</button>
                            </div>
                            <div class="col-xs-4 text-center">
                                <button type="button" class="normal blue">Checkout</button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>

            

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-xs-12">
                    <div class="card transparent clearfix">
                        <div class="inner-card">
                            <h2>Reasons to go with Spark</h2>
                            <div class="row">
                                <div class="col-xs-3 text-center">
                                    <div class="ico-card-img">
                                        <div>
                                            <img src="../images/cart/cart_purchases_ico_1.svg" />
                                        </div>
                                    </div>
                                    <h3>Safe and Secure</h3>
                                    We use SSL 128-bit encryption. So your personal stuff stays safe.
                                </div>
                                <div class="col-xs-3 text-center">
                                    <div class="ico-card-img">
                                        <div>
                                            <img src="../images/cart/cart_purchases_ico_2.svg" />
                                        </div>
                                    </div>
                                    <h3>Online Only Deals</h3>
                                    When you buy online it's a special deal. You can't get it in stores or over the phone.
                                </div>
                                <div class="col-xs-3 text-center">
                                    <div class="ico-card-img">
                                        <div>
                                            <img src="../images/cart/cart_purchases_ico_3.svg" />
                                        </div>
                                    </div>
                                    <h3>24 Hours Confirmation</h3>
                                    We'll turn your order around in under 24 hours. And email to let you know.
                                </div>
                                <div class="col-xs-3 text-center">
                                    <div class="ico-card-img">
                                        <div>
                                            <img src="../images/cart/cart_purchases_ico_4.svg" />
                                        </div>
                                    </div>
                                    <h3>Credit Cards</h3>
                                    We accept Visa, Mastercard, American Express and Diners.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <?php include("../includes/checkout-footer.php");?>