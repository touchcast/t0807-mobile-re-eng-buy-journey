<?php include("../includes/header.php"); ?>
<?php include("../includes/sidebar-j7-configurator-page-data-extras-free-gift.php"); ?>

<div class="container-fluid global-style">

	<?php include("../includes/page-header.php"); ?>

	<div class="inner-container">

		<br />

		<div class="row content-container">
			<div class="col-xs-8">
				<?php include("../includes/j1/content-block-header.php"); ?>

				<!-- Device details -->
				<?php include("../includes/j7/j7-here-is-what-you-top-block.php"); ?>
				<!-- End: Device details -->

				
				<!-- Popup Modal -->
				<div class="row">
					<div class="col-md-12">
						<div class="card primary clearfix">
							<div class="inner-card text-center">
								<!-- Button trigger modal -->
								<button type="button" class="blue fit" data-toggle="modal" data-target="#myModal">
								  Launch Modal
								</button>
								<button type="button" class="blue fit hidden-md hidden-lg" data-toggle="modal" data-target="#handoffmodal" data-backdrop="false">
								  Launch Modal
								</button>
							</div>
						</div>
					</div>
				</div>
		
				<!-- **********
				Your mobile number
				********** -->
				
				<div class="card clearfix secondary">
					<div class="inner-card clearfix">
						<div class="inner-wrapper"> 
							<!-- <a href="#" class="float-right">Close <i class="icon-x"></i></a> -->
							<h2 class="card-title underlined">Your mobile number</h2>
							<div class="">
								<!-- <h4>Choose the contract you'd like</h4> -->

								<div class="row">
								<!-- Radio button -->
									
									<div class="col-xs-12">
										<input type="radio" value="radio0001" id="radio0001" name="radio000" checked class="normal">
										<label for="radio0001"><span></span>Use my Spark number</label>
									</div>
									<div class="col-xs-12">
										<input type="radio" value="radio0002" id="radio0002" name="radio000" class="normal">
										<label for="radio0002"><span></span>Give me a new number</label>
									</div>
									<div class="col-xs-12">
										<input type="radio" value="radio0003" id="radio0003" name="radio000" class="normal">
										<label for="radio0003"><span></span>Bring my number from another mobile provider</label>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>

				
				
				<!-- **********
				Data Extras
				********** -->

			
				<div class="card clearfix secondary">
					<div class="inner-card clearfix">
						<div class="inner-wrapper"> 
							<a href="#" class="float-right">
								Change <i class="icon-pencil"></i>
							</a>
							<h2 class="card-title underlined">Data Extras</h2>
							<div class="">
								<!-- <h4>Choose the contract you'd like</h4> -->

								<div class="clearfix">
									<ul>
										<li>
											<strong>500MB data pack</strong> - Normally $15 /mth - FREE
										</li>
									</ul>
								
								</div>
							
							</div>
						</div>
					</div>
				</div>

				<!-- **********
				Intl calling extras
				********** -->

				<div class="card clearfix secondary">
					<div class="inner-card clearfix">
						<div class="inner-wrapper">
							<a href="#" class="float-right">
								Change <i class="icon-pencil"></i>
							</a>
							<h2 class="card-title underlined">International calling Extras</h2>
							<div class="">
								<!-- <h4>Choose the contract you'd like</h4> -->

								<div class="clearfix">
									<ul>
										<li>
											<strong>Aussie pack</strong> - $9 /mth
										</li>
									</ul>

								</div>
							
							</div>
						</div>
					</div>
				</div>

				<!-- **********
				Bundled Extras
				********** -->

				<div class="card clearfix secondary">
					<div class="inner-card clearfix">
						<div class="inner-wrapper">
							<a href="#" class="float-right">
								Change <i class="icon-pencil"></i>
							</a>
							<h2 class="card-title underlined">Bundled Extras</h2>
							<div class="">
								<!-- <h4>Choose the contract you'd like</h4> -->

								<div class="clearfix">
									<ul>
										<li>
											<strong>$19 Aussie roaming pack</strong> - $19 /mth
										</li>
									</ul>

								</div>
							
							</div>
						</div>
					</div>
				</div>

				<!-- **********
				Voice Extras
				********** -->

				<div class="card clearfix secondary">
					<div class="inner-card clearfix">
						<div class="inner-wrapper">
							<a href="#" class="float-right">
								Change <i class="icon-pencil"></i>
							</a>
							<h2 class="card-title underlined">Voice Extras</h2>
							<div class="">
								<!-- <h4>Choose the contract you'd like</h4> -->

								<div class="clearfix">
									<ul>
										<li>
											<strong>$9 Talk</strong> - $9 /mth
										</li>
									</ul>

								</div>
							
							</div>
						</div>
					</div>
				</div>

				<!-- **********
				Voice Extras
				********** -->

				<div class="card clearfix secondary">
					<div class="inner-card clearfix">
						<div class="inner-wrapper">
							<a href="#" class="float-right">
								Change <i class="icon-pencil"></i>
							</a>
							<h2 class="card-title underlined">My Favourites</h2>
							<div class="">
								<!-- <h4>Choose the contract you'd like</h4> -->

								<div class="clearfix">
									<ul>
										<li>
											<strong>3 numbers added</strong> - $6 per number per month
										</li>
									</ul>

								</div>
							
							</div>
						</div>
					</div>
				</div>

			</div>
			<!-- End of left side of the page -->
			
			
		</div>


	</div>

</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	<a type="button" class="close btn-mobile" data-dismiss="modal"><i class="icon-cancel-circle blue-text"></i></a>

	<div class="modal-dialog">
	    <div class="modal-content">
	    	 <!-- <a type="button" class="close btn-desk" data-dismiss="modal"><i class="icon-cancel-circle">Close</i></a> -->



	      <!-- <div class="modal-header">
	        
	      </div> -->

	      <div class="modal-body">
	      	 <a href="#" class="float-right clearfix" data-dismiss="modal">
				Close <i class="icon-x"></i>
			</a>
	        <h2 class="title underlined">Terms & conditions</h2>
	        <p>
	        	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim 
	        	veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate 
	        	velit esse cillum dolore eu fugiat nulla pariatur.
	        </p>
	        <p>
	        	Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis 
	        	iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi 
	        	architecto.
	       </p>
	       <p>
			Beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur 
			magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, 
			adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
			<p><br/></p>
			<h2 class="title underlined margin-top">My Favourites for only $6</h2>
	        <h4 class="margin-bottom">How do I get My Favourites</h4>
	        <p>
	        	Simply  <a>sign up online</a>  or text ADD, then the number you want to call, to FAVE(3283). If you’re on prepaid, you can also call *333.</p>
	        	<h4 class="margin-bottom">Change your numbers</h4>
<p>Use our  <a>online form</a>  to manage your My Favourites selections: you can add, change and remove numbers. You can also use  text messaging  or, if you’re on Prepaid, you can call *333.</p>
<h4 class="margin-bottom">Landlines</h4>
<p>We’ve extended this popular product to landlines so you can get MyFavourites for your home phone.
	        </p>
			<!-- <a href="#" class="button blue"> call to action</a> -->
			<!-- <div class="modal-header">
	        	
	      	</div> -->
	      </div>
	      
	    </div>
	</div>
</div>
<!-- /End Popup Modal -->
<?php include("../includes/footer.php"); ?>