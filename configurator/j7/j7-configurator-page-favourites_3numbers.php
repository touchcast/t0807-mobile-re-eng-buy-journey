<?php include("../includes/header.php"); ?>
<?php include("../includes/sidebar-j7-configurator-page-favourites_1number.php"); ?>

<div class="container-fluid global-style">

	<?php include("../includes/page-header.php"); ?>

	<div class="inner-container">

		<br />

		<div class="row content-container">
			<div class="col-xs-8">
				<?php include("../includes/j1/content-block-header.php"); ?>

				<!-- Device details -->
				<?php include("../includes/j7/j7-here-is-what-you-top-block.php"); ?>
				<!-- End: Device details -->

				

		
				<!-- **********
				Your mobile number
				********** -->
				
				<div class="card clearfix secondary">
					<div class="inner-card clearfix">
						<div class="inner-wrapper"> 
							<!-- <a href="#" class="float-right">Close <i class="icon-x"></i></a> -->
							<h2 class="card-title underlined">Your mobile number</h2>
							<div class="">
								<!-- <h4>Choose the contract you'd like</h4> -->

								<div class="row">
								<!-- Radio button -->
									
									<div class="col-xs-12">
										<input type="radio" value="radio0001" id="radio0001" name="radio000" checked class="normal">
										<label for="radio0001"><span></span>Use my Spark number</label>
									</div>
									<div class="col-xs-12">
										<input type="radio" value="radio0002" id="radio0002" name="radio000" class="normal">
										<label for="radio0002"><span></span>Give me a new number</label>
									</div>
									<div class="col-xs-12">
										<input type="radio" value="radio0003" id="radio0003" name="radio000" class="normal">
										<label for="radio0003"><span></span>Bring my number from another mobile provider</label>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>

				
				
				<!-- **********
				Data Extras
				********** -->

				<div class="card clearfix secondary">
					<div class="inner-card clearfix">
						<div class="inner-wrapper">
							<a href="#" class="float-right">
								Change <i class="icon-pencil"></i>
							</a>
							<h2 class="card-title underlined">Data Extras</h2>
							<div class="">
								<!-- <h4>Choose the contract you'd like</h4> -->

								<div class="clearfix">
									<ul>
										<li>
											<strong>500MB data pack</strong> - $15 /mth
										</li>
									</ul>

								</div>
							
							</div>
						</div>
					</div>
				</div>

				<!-- **********
				Intl calling extras
				********** -->

				<div class="card clearfix secondary">
					<div class="inner-card clearfix">
						<div class="inner-wrapper">
							<a href="#" class="float-right">
								Change <i class="icon-pencil"></i>
							</a>
							<h2 class="card-title underlined">International calling Extras</h2>
							<div class="">
								<!-- <h4>Choose the contract you'd like</h4> -->

								<div class="clearfix">
									<ul>
										<li>
											<strong>Aussie pack</strong> - $9 /mth
										</li>
									</ul>

								</div>
							
							</div>
						</div>
					</div>
				</div>

				<!-- **********
				Bundled Extras
				********** -->

				<div class="card clearfix secondary">
					<div class="inner-card clearfix">
						<div class="inner-wrapper">
							<a href="#" class="float-right">
								Change <i class="icon-pencil"></i>
							</a>
							<h2 class="card-title underlined">Bundled Extras</h2>
							<div class="">
								<!-- <h4>Choose the contract you'd like</h4> -->

								<div class="clearfix">
									<ul>
										<li>
											<strong>$19 Aussie roaming pack</strong> - $19 /mth
										</li>
									</ul>

								</div>
							
							</div>
						</div>
					</div>
				</div>



				<!-- **********
				Voice Extras
				********** -->

				<div class="card clearfix secondary">
					<div class="inner-card clearfix">
						<div class="inner-wrapper">
							<a href="#" class="float-right">
								Change <i class="icon-pencil"></i>
							</a>
							<h2 class="card-title underlined">Voice Extras</h2>
							<div class="">
								<!-- <h4>Choose the contract you'd like</h4> -->

								<div class="clearfix">
									<ul>
										<li>
											<strong>$9 Talk</strong> - $9 /mth
										</li>
									</ul>

								</div>
							
							</div>
						</div>
					</div>
				</div>

				<!-- **********
				Voice Extras
				********** -->

				<div class="card clearfix primary">
					<div class="inner-card clearfix">
						<div class="inner-wrapper">
							<a href="#" class="float-right">
								Close <i class="icon-x"></i>
							</a>
							<h2 class="card-title underlined">My Favourites</h2>
							<div class="table-row gray">
							<!--Number 1-->
								<div class="form-group">
							    	<div class="row">
							    		<div class="col-sm-12">
							    			<p class="margin-top">Nominate up to 3 Spark mobile numbers you call all the time and talk as often as you want (up to 2 hours per call)</p>
							    			<br/>
							    			<p><strong>My Favourites</strong> - $6 per month</p>
							    		</div>
							    	</div>	
							    	<div class="row">
							    		<div class="col-sm-2">
							    			 <label for="contact_number">Number 1:</label>
							    		</div>
							    		<div class="col-sm-9">
							    			 <div class="phone-number row">
									            <div class="prefix col-xs-5">
									                <div class="dropdown short prefix">
									                	<input type="hidden" name="prefix-number" value="">
									                    <button class="dropdown-toggle selected" type="button" id="prefix-number" data-toggle="dropdown">
									                    027
									                    <span class="caret"></span>
									                    </button>
									                    <ul class="dropdown-menu" role="menu" aria-labelledby="prefix-number">
									                        <li><a role="menuitem">020</a></li>
									                        <li><a role="menuitem">021</a></li>
									                        <li><a role="menuitem" >022</a></li>
									                        <li><a role="menuitem">027</a></li>
									                        <li><a role="menuitem">028</a></li>
									                        <li><a role="menuitem">029</a></li>
									                        <li><a role="menuitem">0204</a></li>
									                    </ul>
									           
									                </div>
									            </div>
									            <div class="number col-xs-7">
									                <!-- <input class="input-large" id="contact_number" placeholder="1234567" type="text"> -->
									 				<input class="form-control active" name="account-number" value="1234567" type="text">
									            </div>
									        </div>  
									        <p class="margin-top"><a href="#">Add another number</a></p>
									        
							    		</div>
							    	</div>
							    </div>

							    <!--Number 2-->
							    <div class="form-group">
							    	<div class="row">
							    		<div class="col-sm-12">
							    			<p><strong>My Favourites</strong> - $6 per month</p>
							    		</div>
							    	</div>	
							    	<div class="row">
							    		<div class="col-sm-2">
							    			 <label for="contact_number">Number 2:</label>
							    		</div>
							    		<div class="col-sm-9">
							    			 <div class="phone-number row">
									            <div class="prefix col-xs-5">
									                <div class="dropdown short prefix">
									                	<input type="hidden" name="prefix-number" value="">
									                    <button class="dropdown-toggle selected" type="button" id="prefix-number" data-toggle="dropdown">
									                    027
									                    <span class="caret"></span>
									                    </button>
									                    <ul class="dropdown-menu" role="menu" aria-labelledby="prefix-number">
									                        <li><a role="menuitem">020</a></li>
									                        <li><a role="menuitem">021</a></li>
									                        <li><a role="menuitem">022</a></li>
									                        <li><a role="menuitem">027</a></li>
									                        <li><a role="menuitem">028</a></li>
									                        <li><a role="menuitem">029</a></li>
									                        <li><a role="menuitem">0204</a></li>
									                    </ul>
									           
									                </div>
									            </div>
									            <div class="number col-xs-7">
									                <!-- <input class="input-large" id="contact_number" placeholder="1234568" type="text"> -->
									 				<input class="form-control active" name="account-number" value="1234568" type="text">
									            </div>
									        </div>  
									        <div class="phone-number row">
									            <div class="prefix col-xs-7">
       										        <p class="margin-top"><a href="#">Add another number</a>	
									            </div>
									            <div class="prefix col-xs-5">
									            	<p class="margin-top text-right"><a href="#">Remove</a></p>	
									            </div>
								            </div>

									        
							    		</div>
							    	</div>
							    </div>
							    <!--Number 3-->
							    <div class="form-group">
							    	<div class="row">
							    		<div class="col-sm-12">
							    			<p><strong>My Favourites</strong> - $6 per month</p>
							    		</div>
							    	</div>	
							    	<div class="row">
							    		<div class="col-sm-2">
							    			 <label for="contact_number">Number 3:</label>
							    		</div>
							    		<div class="col-sm-9">
							    			 <div class="phone-number row">
									            <div class="prefix col-xs-5">
									                <div class="dropdown short prefix">
									                	<input type="hidden" name="prefix-number" value="">
									                    <button class="dropdown-toggle selected" type="button" id="prefix-number" data-toggle="dropdown">
									                    027
									                    <span class="caret"></span>
									                    </button>
									                    <ul class="dropdown-menu" role="menu" aria-labelledby="prefix-number">
									                        <li><a role="menuitem">020</a></li>
									                        <li><a role="menuitem">021</a></li>
									                        <li><a role="menuitem">022</a></li>
									                        <li><a role="menuitem">027</a></li>
									                        <li><a role="menuitem">028</a></li>
									                        <li><a role="menuitem">029</a></li>
									                        <li><a role="menuitem">0204</a></li>
									                    </ul>
									           
									                </div>
									            </div>
									            <div class="number col-xs-7">
									                <!-- <input class="input-large font-control active" id="contact_number" placeholder="1234569" type="text"> -->
									 				<input class="form-control active" name="account-number" value="1234569" type="text">
									            </div>
									        </div>  
									        <p class="margin-top text-right"><a href="#">Remove</a></p>
									        <a href="#" class="button blue slim margin-top">Add Favourite</a>
							    		</div>
							    	</div>
							    </div>
							</div>
						</div>
					</div>
					<div class="inner-card row" style="padding-top:0;">
						<div class="table-row gray">
							<a href="#">Show terms &amp; conditions</a>
						</div>
					</div>
				</div>



			</div>
			<!-- End of left side of the page -->
			
			
		</div>

	</div>

</div>

<?php include("../includes/footer.php"); ?>