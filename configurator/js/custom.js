/* custom */

$(document).ready(function(){

    //
    // Search for all Modal triggers (if there are any)
    // and append to body so that Modals and
    // Modal-backdrop is siblings (z-index issue workaround)
    //
    if ($('[data-toggle="modal"]')[0]) {
        var detchedModals = $('.modal').detach();
        detchedModals.appendTo('body');
    }

	//add animation class to div
	function addAnimationClass(matchClass, newclass) {
	    var elems = document.getElementsByClassName(matchClass), i;
        // var elems2 = $('*');
        // only apply animation on tablet size and lower, fourth card onward

        if ($(window).width() < 940){
            for (i in elems) {
                if (i > 2){
                    elems[i].className += " " + newclass;
                }
            }
        } 	    
	}
	    
	addAnimationClass('card', 'fadeInUp');

    
	$('.can-animate').hover(function() {
    $(this).addClass('HoverTransition');
 
    }, function() {
        $(this).removeClass('HoverTransition');
    });
    
    $('.can-animate').on('mousedown touchstart', function(){
        $(this).addClass('ClickTransition');
    });
    
    $('.can-animate').on('mouseup touchend', function(){
        $(this).removeClass('ClickTransition');
    });
    

    //FastClick
    //FastClick.attach(document.body);


    // -----------------------------------------
    // user agent checking, to serve font tweaks
    // -----------------------------------------

    var b = document.documentElement;
    b.className = b.className.replace('no-js', 'js');
    b.setAttribute("data-useragent",  navigator.userAgent);
    b.setAttribute("data-platform", navigator.platform );

    // -----------------------------------------
    // touch device checking, to serve hover
    // -----------------------------------------

    if (Modernizr.touch) {   
        b.setAttribute("touch-device", 'true' );
    } 

    // --------------------------------------------------------------------------
    // JS function to create a physical space on the ticker tape after each word
    // --------------------------------------------------------------------------

    $.fn.mathSpace = function() {
      return $(this).each(function(){
        $(this).children('span').each(function() {
          var el = $(this);
          var text = el.text();
          el.text(
            text.split(' ').join(' \u205f')
          ); 
        });
        $(this).children('.block-link').each(function() {
          var el = $(this);
          var text = el.text();
          el.text(
            text.split(' ').join(' \u205f')
          ); 
        });
      });
    }

    //$('.block').mathSpace();


    // ---------------------------------------------------------------------------
    // Initiate stacked tabs
    // ---------------------------------------------------------------------------
    $('.stacked-tab').tabCollapse();



    // ---------------------------------------------------------------------------
    // JS function to add mobile class to element inside a container < 320 width on 
    // ---------------------------------------------------------------------------

    $.fn.smallForm = function() {
      var mobileWidth = 480; // max width for 'small form' module
      return $(this).each(function(){
        $(this).removeClass("small-form"); // clear up class
        //add small-form classs when component meets criteria
        if($(this).width() < mobileWidth && $(window).width() > 639){
           $(this).addClass("small-form");
        }
      });
    }

    $('.card').smallForm();

    // Reinstate the function on window resize
    $( window ).resize(function() {
      $('.card').smallForm();
    });



    // ---------------------------------------------------------------------------
    // Script for form's Dropdown list 
    // ---------------------------------------------------------------------------

    $(document).on("click", ".dropdown-menu li a", function(event) {
      var selText = $(this).text();
      $(this).parents('.dropdown').find('.dropdown-toggle').html(selText);
      $(this).parents('.dropdown').find('.dropdown-toggle').addClass('selected');
    });

});



    // ---------------------------------------------------------------------------
    // Unsupported browser detection
    // ---------------------------------------------------------------------------

    // Create 'user' object that will contain Detect.js stuff
    // Call detect.parse() with navigator.userAgent as the argument
    var user = detect.parse(navigator.userAgent);
  
    console.log(
      user.browser.family,
      user.browser.version,
      user.os.name,
      user.device.type
    );

    if(user.browser.family === 'Chrome' && user.browser.version < 37){
        $('#unsupported-browser').show();
    } else if (user.browser.family ==='IE' && user.browser.version < 9){
        $('#unsupported-browser').show();
    } else if (user.browser.family === 'Firefox' && user.browser.version < 31){
        $('#unsupported-browser').show();
    } else if (user.browser.family === 'Safari' && user.browser.version < 7){
        $('#unsupported-browser').show();
    } else if (user.browser.family !=='Chrome' && 
        user.browser.family !=='IE' && 
        user.browser.family !=='Firefox' && 
        user.browser.family !=='Safari' && 
        user.device.type ==='Desktop'){
        
        $('#unsupported-browser').show();
    } 



    // ---------------------------------------------------------------------------
    // wow fade in animation
    // ---------------------------------------------------------------------------

    var wow = new WOW({
        boxClass:     'card',      // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset:       0,          // distance to the element when triggering the animation (default is 0)
        mobile:       true       // trigger animations on mobile devices (true is default)
    });

    //wow.init();


    // ---------------------------------------------------------------------------
    // Getting available query strings
    // ---------------------------------------------------------------------------

    function GetQueryStringParams(sParam)
    {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) 
        {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) 
            {
                return sParameterName[1];
            }
        }
    };

    // ------------------------------------------------------------------------------
    // Deep linking to tabs 'tab' and accordion according 'acc' querystring
    // ------------------------------------------------------------------------------
    // Usage : www.spark.co.nz?tab=tab1 OR www.spark.co.nz?acc=accordion1 (IDs of the element)
    // ------------------------------------------------------------------------------

    $(function() {

        // get acc/tab query string
        var acc = GetQueryStringParams('acc');
        var tab = GetQueryStringParams('tab');
        var firstLaod;

        if(acc || tab){
            firstLaod = true;
        }
        
        
        // if ref exist, open the corresponding accordion/tab item
        if(acc && firstLaod){
            // open queried accordion item
            $("a[href*="+acc+"]").click();
        }

        if(tab && firstLaod){
            // open queried tab item
            $("a[href*="+tab+"]").click();
        }
        

        if(tab && firstLaod){
            var tabHead = "#"+tab;
            navigateToElement(tabHead);
            firstLaod = false;
        }    

        $('.panel-group').on('shown.bs.collapse', function (e) {
            if(firstLaod || $(window).width() <= 768){
                var titleHead = $(e.target).prev().find('.panel-title .panel-toggle').attr('href');
                navigateToElement(titleHead);
                firstLaod = false;
            }
        });  


    });

    // ------------------------------------------------------------------------------
    // Accordion when opening in mobile view should pin current accordion to the top.
    // ------------------------------------------------------------------------------

    function navigateToElement(titleHead) {
        //console.log(titleHead);
        $('html, body').animate({
            scrollTop: $('a[href="' + titleHead + '"]').offset().top
        }, 800);
    }