<?php include("../includes/your-cart-header.php"); ?>

    <div class="container-fluid global-style">

        <div class="page-header">
            <div class="center-wrap">
                <h1 class="set-up-header">Your cart</h1>
            </div>
        </div>

        <div class="inner-container">
            <br />

            <div class="row">
                <div class="col-xs-12">
                    <div class="content-block-header">
                        <h1 class="block"><span>Here's what's in your cart</span></h1>
                    </div>
                </div>  
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-xs-12">
                    <div class="card primary clearfix ico-content-card">
                        <div class="inner-card clearfix">
                            <a href="#" class="float-right">
                                Change <i class="icon-pencil"></i>
                                &nbsp;&nbsp;&nbsp;
                                Delete <i class="icon-trash-empty"></i>
                            </a>
                            <h2>24 month Ultra Mobile $99</h2>
                            <div class="info-box yellow margin-bottom white-text">
                                <strong>Because (NUMBER) is on Open term (PLAN), a 1 month plan charge will apply.</strong> See below for pricing details.
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="table-row-divider">
                                        <div class="table-row pad-table-row gray">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <div class="product-image-float">
                                                        <div class="device-price-white">
                                                            <span class="number" data-pre="$">99</span>
                                                            <span class="txt">/MTH</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4">
                                                    <strong>24 month Ultra Mobile $99</strong>
                                                    <ul>
                                                        <li>You are keeping your number <br>026 870 530</li>
                                                        <li>You are changing from (PLAN NAME)</li>
                                                    </ul>
                                                </div>
                                                <div class="col-xs-2">
                                                    Monthly
                                                </div>
                                                <div class="col-xs-2">
                                                    <div class="text-right">
                                                        <strong>$99.00</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="table-row-divider">
                                        <div class="table-row pad-table-row gray">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <div class="product-image-float">
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-xs-4">
                                                    <strong>1 Month Plan Charge</strong><br/>
                                                </div>
                                                <div class="col-xs-2">
                                                    Upfront
                                                </div>
                                                <div class="col-xs-2">
                                                    <div class="text-right">
                                                        <strong>$99.00</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="table-row-divider">
                                        <div class="table-row pad-table-row gray">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <div class="product-image-float">
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-xs-4">
                                                    <strong>Also included with this plan</strong><br/>
                                                    <img src="../images/config/config-v1-thanks.png" style="margin:10px 0 0 0;" />
                                                </div>
                                                <div class="col-xs-2">
                                                </div>
                                                <div class="col-xs-2">
                                                    <div class="text-right">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-4 col-h3 pull-right">
                                    <h3 style="margin-top:12px;">Subtotal</h3>
                                    <div class="table-row">
                                        <div class="row">
                                            <div class="col-xs-8">
                                                <strong>Monthly plan payments</strong>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="text-right">
                                                <strong>$99.00</strong>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top">
                                            <div class="col-xs-8">
                                                <strong>Total upfront payment</strong>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="text-right">
                                                <strong>$99.00</strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                            <!-- // -->
                        </div>
                    </div>
                </div>
            </div>

             <hr class="no-margin margin-bottom"/>

            <div class="row">
                <div class="col-xs-12">
                    <div class="content-block-header">
                        <h1 class="block"><span>Your cart summary</span></h1>
                    </div>
                </div>  
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-xs-12">
                    <div class="card primary clearfix ico-content-card">
                        <div class="inner-card clearfix">

                            <div class="cart-summary-row">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <span class="cart-summary-text">Monthly plan payments</span>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="cart-summary-text text-right">
                                            $99.00
                                        </div>
                                    </div>
                                </div>
                            </div>

                             <div class="cart-summary-row">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <span class="cart-summary-text">Total upfront payment</span>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="cart-summary-text text-right">
                                            $99.00
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-8">
                                    &nbsp;
                                </div>
                                <div class="col-xs-4">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <a href="#" class="blue-button-float-right">
                                                <div class="inner-blue-button">Apply</div>
                                            </a>
                                            <input type="text" class="form-control form-control-with-blue-button" name="account_number" placeholder="Enter voucher code here">
                                        </div>
                                    </div>
                                    <div class="text-right margin-top">
                                        <a href="#">What is this?</a>
                                    </div>
                                    <div class="cart-summary-row top">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="cart-summary-text">
                                                    Total
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="cart-summary-text text-right">
                                                    $198.00
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="info-box green margin-bottom">
                                <strong>Heads-up.</strong> The upfront payments will be added to your next Spark bill with account no. Look out for your bill arriving to find out how long you have to pay.
                            </div>

                            <div class="clearfix">
                                <input type="checkbox" name="terms1" class="normal" id="terms1">
                                <label for="terms1" class="no-margin"><span class="margin-right"></span>I agree to the <a href="#">Spark Mobile change plan Terms and Conditions</a></label>
                            </div>

                            <div class="clearfix">
                                <input type="checkbox" name="terms2" class="normal" id="terms2">
                                <label for="terms2" class="no-margin"><span class="margin-right"></span>I agree to the <a href="#">Spark Mobile Terms and Conditions</a></label>
                            </div>

                            <div class="clearfix">
                                <input type="checkbox" name="opt_in" class="normal" id="opt_in">
                                <label for="opt_in" class="no-margin"><span class="margin-right"></span>Please email me updates about any new services, news, offers and exclusive promotions</label>
                            </div>

                        </div>

                        <div class="blue-bg blue-button-under-card">
                            <div class="col-xs-4 text-center">
                                <button type="button" class="normal secondary blue">Keep shopping</button>
                            </div>
                            <div class="col-xs-4 text-center">
                                <button type="button" class="normal secondary blue">Save</button>
                            </div>
                            <div class="col-xs-4 text-center">
                                <button type="button" class="normal blue">Checkout</button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card clearfix secondary">
                    <div id="accordion2" class="panel-group card animated" style="visibility: visible;">

                      <div id="panel12" class="panel panel-default">
                        <div class="panel-heading">
                          <div class="panel-title">
                            <a href="#collapseTwelve" data-parent="#accordion2" data-toggle="collapse" class="panel-toggle">
                              <h4 class="uppercase">Important things you need to know</h4>
                            </a>
                          </div>
                        </div>
                        <div class="panel-collapse collapse in" id="collapseTwelve">
                          <div class="panel-body">
                            <p>
                                All monthly costs will be charged to your Spark bill. Ongoing monthly costs may differ to the amount you see on your next Spark bill due to billing part way through the month. <a href="#">Find out more</a>.
                            </p>
                            <p>
                                There are several factors that can affect whether broadband is available at your address or not. And we’re not able to test for all of them in advance. So it’s important to note that we can’t guarantee the service works until broadband has been hooked up and we have tested your phone line.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>

                <div class="card clearfix secondary">
                    <div  id="accordion3" class="panel-group card animated">
                      <div id="panel13" class="panel panel-default">
                        <div class="panel-heading">
                          <div class="panel-title">
                            <a href="#collapseThirteen" data-parent="#accordion3" data-toggle="collapse" class="panel-toggle collapsed">
                              <h4 class="uppercase">Not so important stuff</h4>
                            </a>
                          </div>
                        </div>
                        <div class="panel-collapse collapse" id="collapseThirteen" style="">
                          <div class="panel-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mi mi, fringilla sed urna non, porttitor luctus est. Curabitur quis congue erat. Donec et elit ut justo pulvinar commodo. Sed ac ipsum id ex consequat posuere sed at risus. Donec vitae libero nibh. Praesent et mattis lectus, ut volutpat est. Pellentesque felis diam, sollicitudin et velit in, rutrum egestas est. Sed sed libero elementum, ultricies augue sed, lacinia nulla. Integer dictum scelerisque magna quis pulvinar. Maecenas sit amet libero dignissim, efficitur velit ut, ornare ligula. Ut tincidunt ac justo sit amet fringilla. Mauris malesuada est ex, at tincidunt elit scelerisque sed. Pellentesque porta lacus vitae ipsum fringilla tincidunt.</p>
                          </div>
                        </div>
                      </div>
                      
                    </div>
                </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-xs-12">
                    <div class="card transparent clearfix">
                        <div class="inner-card">
                            <h2>Reasons to go with Spark</h2>
                            <div class="row">
                                <div class="col-xs-3 text-center">
                                    <div class="ico-card-img">
                                        <div>
                                            <img src="../images/cart/cart_purchases_ico_1.svg" />
                                        </div>
                                    </div>
                                    <h3>Safe and Secure</h3>
                                    We use SSL 128-bit encryption. So your personal stuff stays safe.
                                </div>
                                <div class="col-xs-3 text-center">
                                    <div class="ico-card-img">
                                        <div>
                                            <img src="../images/cart/cart_purchases_ico_2.svg" />
                                        </div>
                                    </div>
                                    <h3>Online Only Deals</h3>
                                    When you buy online it's a special deal. You can't get it in stores or over the phone.
                                </div>
                                <div class="col-xs-3 text-center">
                                    <div class="ico-card-img">
                                        <div>
                                            <img src="../images/cart/cart_purchases_ico_3.svg" />
                                        </div>
                                    </div>
                                    <h3>24 Hours Confirmation</h3>
                                    We'll turn your order around in under 24 hours. And email to let you know.
                                </div>
                                <div class="col-xs-3 text-center">
                                    <div class="ico-card-img">
                                        <div>
                                            <img src="../images/cart/cart_purchases_ico_4.svg" />
                                        </div>
                                    </div>
                                    <h3>Credit Cards</h3>
                                    We accept Visa, Mastercard, American Express and Diners.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <?php include("../includes/checkout-footer.php");?>