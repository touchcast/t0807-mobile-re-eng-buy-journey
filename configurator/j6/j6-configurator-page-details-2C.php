<?php include("../includes/header.php"); ?>
<?php include("../includes/j6/sidebar-j6-configurator-page-details-2B.php"); ?>

<div class="container-fluid global-style">

	<?php include("../includes/page-header.php"); ?>

	<div class="inner-container">

		<br />

		<div class="row content-container">
			<div class="col-xs-8">
				<?php include("../includes/j1/j6-content-block-header.php"); ?> 

				<!-- Device details -->
				<?php include("../includes/j6/j6-here-is-your-current-plan-2A.php"); ?>
				<!-- End: Device details -->

					<!-- Popup Modal -->
				<div class="row">
					<div class="col-md-12">
						<div class="card primary clearfix">
							<div class="inner-card text-center">
								<!-- Button trigger modal -->
								<button type="button" class="blue fit" data-toggle="modal" data-target="#myModal">
								  Launch Modal
								</button>
								<button type="button" class="blue fit hidden-md hidden-lg" data-toggle="modal" data-target="#handoffmodal" data-backdrop="false">
								  Launch Modal
								</button>
							</div>
						</div>
					</div>
				</div>	

		
				<!-- **********
				better plan
				********** -->
				
				<div class="card clearfix secondary">
					<div class="inner-card clearfix">
						<div class="inner-wrapper"> 
							<!-- <a href="#" class="float-right">Close <i class="icon-x"></i></a> -->
							<h2 class="card-title underlined">Maybe there's a better plan for you</h2>
							
							<p>
								We want to make sure you get the best value for your money. <br>
								So, looking at how you use your current plan, we've found one that might suit you a little better.
							</p>
						</div>
					</div>
				</div>

				<!-- **********
				Term options
				********** -->

				<?php include("../includes/j6/terms-option-tabs-1A.php"); ?> 


			

			</div>
			<!-- End of left side of the page -->
			
			
		</div>

	</div>

</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	<a type="button" class="close btn-mobile" data-dismiss="modal"><i class="icon-cancel-circle blue-text"></i></a>

	<div class="modal-dialog modal-dialog-new">
	    <div class="modal-content">
	    	 <!-- <a type="button" class="close btn-desk" data-dismiss="modal"><i class="icon-cancel-circle">Close</i></a> -->



	      <!-- <div class="modal-header">
	        
	      </div> -->

	      <div class="modal-body">
	      	 <a href="#" class="float-right clearfix"  data-dismiss="modal">
				Close <i class="icon-x"></i>
			</a>
	        <h2 class="title underlined">Do you want a new mobile with that?</h2>
	        <p>
	        	Your new plan comes with a big discount on mobiles.<br/>
	        	So you could get a sweet deal on a new phone at the same time as you switch plans.</p>
	        Want to go shopping before you check out?

			
	      </div>
	      <div class="blue-bg blue-button-under-card side">
                <div class="col-xs-6 text-center margin-top margin-bottom">
                    <button class="slim secondary blue add-to-cart" type="button">No Thanks</button>
                </div>

                <div class="col-xs-6 text-center margin-top margin-bottom">
                    <button class="slim blue custom-button" type="button">Show me the phones</button>
                </div>

                <div class="clearfix"></div>
                  
            </div>
	      
	    </div>
	</div>
</div>
<!-- /End Popup Modal -->
<?php include("../includes/footer.php"); ?>