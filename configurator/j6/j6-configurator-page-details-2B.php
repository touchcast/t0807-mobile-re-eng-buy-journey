<?php include("../includes/header.php"); ?>
<?php include("../includes/j6/sidebar-j6-configurator-page-details-2B.php"); ?>

<div class="container-fluid global-style">

	<?php include("../includes/page-header.php"); ?>

	<div class="inner-container">

		<br />

		<div class="row content-container">
			<div class="col-xs-8">
				<?php include("../includes/j1/j6-content-block-header.php"); ?> 

				<!-- Device details -->
				<?php include("../includes/j6/j6-here-is-your-current-plan-2A.php"); ?>
				<!-- End: Device details -->

				

		
				<!-- **********
				better plan
				********** -->
				
				<div class="card clearfix secondary">
					<div class="inner-card clearfix">
						<div class="inner-wrapper"> 
							<!-- <a href="#" class="float-right">Close <i class="icon-x"></i></a> -->
							<h2 class="card-title underlined">Maybe there's a better plan for you</h2>
							
							<p>
								We want to make sure you get the best value for your money. <br>
								So, looking at how you use your current plan, we've found one that might suit you a little better.
							</p>
						</div>
					</div>
				</div>

				<!-- **********
				Term options
				********** -->

				<?php include("../includes/j6/terms-option-tabs-2B.php"); ?> 


			

			</div>
			<!-- End of left side of the page -->
			
			
		</div>

	</div>

</div>

<?php include("../includes/footer.php"); ?>