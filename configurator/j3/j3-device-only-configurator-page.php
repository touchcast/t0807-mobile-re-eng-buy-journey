<?php include("../includes/header.php"); ?>
<?php include("../includes/j3/sidebar-j3.php"); ?>

<div class="container-fluid global-style">

	<?php include("../includes/page-header.php"); ?>

	<div class="inner-container">

		<br />

		<div class="row content-container">
			<div class="col-xs-8">
				<div class="content-block-header">
					<h1 class="block block-var-1"><span>Before you checkout...</span></h1>
					
				</div>

				<!-- Device details -->
					<div class="card primary table clearfix ico-content-card">
					<div class="inner-card clearfix">
						<a href="#" class="float-right">
							Change <i class="icon-pencil"></i>
						</a>
						<h2>Here's what you're buying</h2>
						<!-- Prepaid Value Pack table -->
						<div class="row">
							<div class="col-xs-12">
								<div class="table-row-divider">
									<div class="table-row gray">
										<div class="row">
											<div class="col-xs-4">
												<div class="product-image-float-2">
													<img src="../images/iPhone.png">
												</div>
											</div>
											<div class="col-xs-8">
												<strong>iPhone 6 Plus 16GB</strong>

												<ul>
													<li>Space Grey</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div><br /><br /><br /><br/>
						
						<div><br /><br /><br /></div>
						<!-- // -->
					</div>
				</div>
				<!-- End: Device details -->

				

		


				<div><br /><br /><br /></div>


				




			</div>
			<!-- End of left side of the page -->
			
			
		</div>

	</div>

</div>

<?php include("../includes/footer.php"); ?>