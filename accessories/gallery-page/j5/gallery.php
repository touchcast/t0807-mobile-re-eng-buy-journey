


<?php include("../../includes/header.php"); ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.2.1/jquery-migrate.js"></script>
<script src="../../../js/filtrify.js"></script>
<script src="../../../js/jquery.isotope.min.js"></script>
<link rel="stylesheet" href="../../../css/accessories/filtrify.css">
<link rel="stylesheet" href="../../../css/accessories/animate.css">

<style>
	#list-wrapper{
		padding: 0;
		margin: 0;
	}

	.item{
		padding: 0;
		margin: 0;
		display: block;
	}
	
</style>
	
	<div class="page-header">
		<div class="center-wrap">
			<h4><a href="#" class="show-xs back-btn"><i class="icon-left-open-big"></i> Back</a></h4>
			<h1>Accessories</h1>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">


				<div id="placeHolder"></div>
				

				<!-- Main filter tab -->

						<!-- filter tab  -->

							<div class="row">
								<div class="col-sm-12 card animated" style="visibility: visible;">
									
									<ul id="myResponsiveTab" class="nav nav-tabs nav-justified responsive-tab">
								        <li>
								        	<a href="#home1" data-toggle="tab">
									        	<h3>
										        	<span class="visible-xs"><i class="icon-mobile circled"></i>Mobile</span>
										        	<span class="visible-sm"><i class="icon-mobile circled"></i>Tablet title</span>
										        	<span class="visible-md visible-lg"><i class="icon-mobile circled"></i>Desktop title</span>
									        	</h3>
								        	</a>
								        </li>

								        <li class="active"><a href="#profile1" data-toggle="tab"><h3><span class="visible-xs"><i class="icon-laptop circled"></i>Mobile</span><span class="visible-sm"><i class="icon-laptop circled"></i>Tablet title</span><span class="visible-md visible-lg"><i class="icon-laptop circled"></i>Desktop title</span></h3></a></li>
								        
								     	 <li><a href="#coffee1" data-toggle="tab"><h3><span class="visible-xs"><i class="icon-mobile circled"></i>Mobile</span><span class="visible-sm"><i class="icon-mobile circled"></i>Tablet title</span><span class="visible-md visible-lg"><i class="icon-mobile circled"></i>Desktop title</span></h3></a></li>
								    </ul>
								    <div id="myResponsiveTabContent" class="tab-content">
								        <div class="tab-pane fade" id="home1">
								            <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat salvia cillum iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.</p>
								        </div>
								        <div class="tab-pane fade in active" id="profile1">
								            <p>Light Blue - is a next generation admin template based on the latest Metro design. There are few reasons we want to tell you, why we have created it:
								                We didn't like the darkness of most of admin templates, so we created this light one. We didn't like the high contrast of most of admin templates, so we created this unobtrusive one.
								                We searched for a solution of how to make widgets look like real widgets, so we decided that deep background - is what makes widgets look real.</p>
								           
								        </div>
								        <div class="tab-pane fade" id="coffee1">
								            <p>Coffee - is a next generation admin template based on the latest Metro design. There are few reasons we want to tell you, why we have created it:
								                We didn't like the darkness of most of admin templates, so we created this light one. We didn't like the high contrast of most of admin templates, so we created this unobtrusive one.
								                We searched for a solution of how to make widgets look like real widgets, so we decided that deep background - is what makes widgets look real.</p>
								           
								        </div>
								        
								    </div>


								</div>
							</div>
						<!-- End filter tab -->

				<!-- Main filter tab end -->



				<!-- Listing panel -->
					
					<div class="panel-section">
						
						<ul id="list-wrapper">


							<!-- item start -->
							<li class="item" data-genre="pop, british, classic rock">
								<div class="col-md-4 col-sm-6">
									<!-- Device card 02 -->
									<div class="card primary clearfix device-card purple-white-card can-animate small-form animated ClickTransition" style="visibility: visible;">
										<a href="#">
											<div class="sash">
												<img src="images/sash-device.png" alt="">
											</div>
											<div class="clearfix inner-card">
												<div class="device-img">
													<img src="images/iphone.png">
												</div>
												
												<div class="device-info">
													<h4>iphone <br>5s 16gb</h4>
													<div class="device-price">
														<span class="number" data-pre="$">149</span>
														<span class="txt">RRP</span>
													</div>
													<p>Open Term Ultra	<br>mobile $39</p>
												</div>
												<div class="device-info-bg"></div>
												<button type="button" class="slim blue">Buy now</button>
											</div>

											<div class="bottom-bar clearfix">
												Mobile
											</div>
										</a>
									</div>
								</div>
							</li>
							<!-- item end -->

							<!-- item start -->
							<li class="item" data-genre="pop, rock, british">
								<div class="col-md-4 col-sm-6">
									<!-- Device card 02 -->
									<div class="card primary clearfix device-card purple-white-card can-animate small-form animated ClickTransition" style="visibility: visible;">
										<a href="#">
											<div class="sash">
												<img src="images/sash-device.png" alt="">
											</div>
											<div class="clearfix inner-card">
												<div class="device-img">
													<img src="images/iphone.png">
												</div>
												
												<div class="device-info">
													<h4>iphone <br>5s 16gb</h4>
													<div class="device-price">
														<span class="number" data-pre="$">149</span>
														<span class="txt">RRP</span>
													</div>
													<p>Open Term Ultra	<br>mobile $39</p>
												</div>
												<div class="device-info-bg"></div>
												<button type="button" class="slim blue">Buy now</button>
											</div>

											<div class="bottom-bar clearfix">
												Mobile
											</div>
										</a>
									</div>
								</div>
							</li>
							<!-- item end -->

							<!-- item start -->
							<li class="item" data-genre="rock, british, classic rock">
								<div class="col-md-4 col-sm-6">
									<!-- Device card 02 -->
									<div class="card primary clearfix device-card purple-white-card can-animate small-form animated ClickTransition" style="visibility: visible;">
										<a href="#">
											<div class="sash">
												<img src="images/sash-device.png" alt="">
											</div>
											<div class="clearfix inner-card">
												<div class="device-img">
													<img src="images/iphone.png">
												</div>
												
												<div class="device-info">
													<h4>iphone <br>5s 16gb</h4>
													<div class="device-price">
														<span class="number" data-pre="$">149</span>
														<span class="txt">RRP</span>
													</div>
													<p>Open Term Ultra	<br>mobile $39</p>
												</div>
												<div class="device-info-bg"></div>
												<button type="button" class="slim blue">Buy now</button>
											</div>

											<div class="bottom-bar clearfix">
												Mobile
											</div>
										</a>
									</div>
								</div>
							</li>
							<!-- item end -->

							<!-- item start -->
							<li class="item" data-genre="pop, rock, classic rock">
								<div class="col-md-4 col-sm-6">
									<!-- Device card 02 -->
									<div class="card primary clearfix device-card purple-white-card can-animate small-form animated ClickTransition" style="visibility: visible;">
										<a href="#">
											<div class="sash">
												<img src="images/sash-device.png" alt="">
											</div>
											<div class="clearfix inner-card">
												<div class="device-img">
													<img src="images/iphone.png">
												</div>
												
												<div class="device-info">
													<h4>iphone <br>5s 16gb</h4>
													<div class="device-price">
														<span class="number" data-pre="$">149</span>
														<span class="txt">RRP</span>
													</div>
													<p>Open Term Ultra	<br>mobile $39</p>
												</div>
												<div class="device-info-bg"></div>
												<button type="button" class="slim blue">Buy now</button>
											</div>

											<div class="bottom-bar clearfix">
												Mobile
											</div>
										</a>
									</div>
								</div>
							</li>
							<!-- item end -->

							<!-- item start -->
							<li class="item" data-genre="pop, rock, british">
								<div class="col-md-4 col-sm-6">
									<!-- Device card 02 -->
									<div class="card primary clearfix device-card purple-white-card can-animate small-form animated ClickTransition" style="visibility: visible;">
										<a href="#">
											<div class="sash">
												<img src="images/sash-device.png" alt="">
											</div>
											<div class="clearfix inner-card">
												<div class="device-img">
													<img src="images/iphone.png">
												</div>
												
												<div class="device-info">
													<h4>iphone <br>5s 16gb</h4>
													<div class="device-price">
														<span class="number" data-pre="$">149</span>
														<span class="txt">RRP</span>
													</div>
													<p>Open Term Ultra	<br>mobile $39</p>
												</div>
												<div class="device-info-bg"></div>
												<button type="button" class="slim blue">Buy now</button>
											</div>

											<div class="bottom-bar clearfix">
												Mobile
											</div>
										</a>
									</div>
								</div>
							</li>
							<!-- item end -->

							<!-- item start -->
							<li class="item" data-genre="pop">
								<div class="col-md-4 col-sm-6">
									<!-- Device card 02 -->
									<div class="card primary clearfix device-card purple-white-card can-animate small-form animated ClickTransition" style="visibility: visible;">
										<a href="#">
											<div class="sash">
												<img src="images/sash-device.png" alt="">
											</div>
											<div class="clearfix inner-card">
												<div class="device-img">
													<img src="images/iphone.png">
												</div>
												
												<div class="device-info">
													<h4>iphone <br>5s 16gb</h4>
													<div class="device-price">
														<span class="number" data-pre="$">149</span>
														<span class="txt">RRP</span>
													</div>
													<p>Open Term Ultra	<br>mobile $39</p>
												</div>
												<div class="device-info-bg"></div>
												<button type="button" class="slim blue">Buy now</button>
											</div>

											<div class="bottom-bar clearfix">
												Mobile
											</div>
										</a>
									</div>
								</div>
							</li>
							<!-- item end -->

							<!-- item start -->
							<li class="item" data-genre="pop, british, classic rock">
								<div class="col-md-4 col-sm-6">
									<!-- Device card 02 -->
									<div class="card primary clearfix device-card purple-white-card can-animate small-form animated ClickTransition" style="visibility: visible;">
										<a href="#">
											<div class="sash">
												<img src="images/sash-device.png" alt="">
											</div>
											<div class="clearfix inner-card">
												<div class="device-img">
													<img src="images/iphone.png">
												</div>
												
												<div class="device-info">
													<h4>iphone <br>5s 16gb</h4>
													<div class="device-price">
														<span class="number" data-pre="$">149</span>
														<span class="txt">RRP</span>
													</div>
													<p>Open Term Ultra	<br>mobile $39</p>
												</div>
												<div class="device-info-bg"></div>
												<button type="button" class="slim blue">Buy now</button>
											</div>

											<div class="bottom-bar clearfix">
												Mobile
											</div>
										</a>
									</div>
								</div>
							</li>
							<!-- item end -->

							<!-- item start -->
							<li class="item" data-genre="rock, british, classic rock">
								<div class="col-md-4 col-sm-6">
									<!-- Device card 02 -->
									<div class="card primary clearfix device-card purple-white-card can-animate small-form animated ClickTransition" style="visibility: visible;">
										<a href="#">
											<div class="sash">
												<img src="images/sash-device.png" alt="">
											</div>
											<div class="clearfix inner-card">
												<div class="device-img">
													<img src="images/iphone.png">
												</div>
												
												<div class="device-info">
													<h4>iphone <br>5s 16gb</h4>
													<div class="device-price">
														<span class="number" data-pre="$">149</span>
														<span class="txt">RRP</span>
													</div>
													<p>Open Term Ultra	<br>mobile $39</p>
												</div>
												<div class="device-info-bg"></div>
												<button type="button" class="slim blue">Buy now</button>
											</div>

											<div class="bottom-bar clearfix">
												Mobile
											</div>
										</a>
									</div>
								</div>
							</li>
							<!-- item end -->



							<!-- item start -->
							<li class="item" data-genre="pop, rock, classic rock">
								<div class="col-md-4 col-sm-6">
									<!-- Device card 02 -->
									<div class="card primary clearfix device-card purple-white-card can-animate small-form animated ClickTransition" style="visibility: visible;">
										<a href="#">
											<div class="sash">
												<img src="images/sash-device.png" alt="">
											</div>
											<div class="clearfix inner-card">
												<div class="device-img">
													<img src="images/iphone.png">
												</div>
												
												<div class="device-info">
													<h4>iphone <br>5s 16gb</h4>
													<div class="device-price">
														<span class="number" data-pre="$">149</span>
														<span class="txt">RRP</span>
													</div>
													<p>Open Term Ultra	<br>mobile $39</p>
												</div>
												<div class="device-info-bg"></div>
												<button type="button" class="slim blue">Buy now</button>
											</div>

											<div class="bottom-bar clearfix">
												Mobile
											</div>
										</a>
									</div>
								</div>
							</li>
							<!-- item end -->



							
						</ul>

					</div>

				<!-- End listing panel -->

			</div>
		</div>
	</div>


	<script>

		$(function() {

		    $.filtrify("list-wrapper", "placeHolder");

		});
	</script>

<?php include("../../includes/footer.php"); ?>



