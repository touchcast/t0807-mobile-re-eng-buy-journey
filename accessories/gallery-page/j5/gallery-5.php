<?php include("../../includes/header.php"); ?>

	<div class="page-header">
		<div class="center-wrap">
			<h4><a href="#" class="show-xs back-btn"><i class="icon-left-open-big"></i> Back button</a></h4>
			<h1>Accessories</h1>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<ul id="myResponsiveTab" class="nav nav-tabs nav-justified responsive-tab">
			        <li>
			        	<a href="#categories" data-toggle="tab">
				        	<h4>
					        	<span class="visible-xs">Categories</span>
					        	<span class="visible-sm">Categories</span>
					        	<span class="visible-md visible-lg">Categories</span>
				        	</h4>
			        	</a>
			        </li>

			        <li>
			        	<a href="#brands" data-toggle="tab">
			        		<h4>
			        			<span class="visible-xs">Brands</span>
			        			<span class="visible-sm">Brands</span>
			        			<span class="visible-md visible-lg">Brands</span>
			        		</h4>
			        	</a>
			        </li>
			        
			     	<li>
			     	 	<a href="#colours" data-toggle="tab">
			     	 		<h4>
			     	 			<span class="visible-xs">Colours</span>
			     	 			<span class="visible-sm">Colours</span>
			     	 			<span class="visible-md visible-lg">Colours</span>
			     	 		</h4>
			     	 	</a>
			     	</li>

			     	<li class="active">
			     	 	<a href="#deals" data-toggle="tab">
			     	 		<h4>
			     	 			<span class="visible-xs">Deals</span>
			     	 			<span class="visible-sm">Deals</span>
			     	 			<span class="visible-md visible-lg">Deals</span>
			     	 		</h4>
			     	 	</a>
			     	 </li>
			    </ul>

			    <div id="myResponsiveTabContent" class="tab-content">
			    	<div class="tab-pane fade in" id="categories">
			    		<div class="card secondary">
			    			<div class="carousel carousel-list carousel-gallery">
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter selected"></div>
					    					<h4>Cases &amp; <br>protections</h4>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    					<h4>Batteries &amp; <br>chargers</h4>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    					<h4>Audio</h4>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    					<h4>Cars &amp; <br>travel</h4>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    					<h4>Wearables</h4>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    					<h4>Another <br>category</h4>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    					<h4>Another <br>category</h4>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    					<h4>Another <br>category</h4>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    					<h4>Another <br>category</h4>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    					<h4>Another <br>category</h4>
					    				</div>
					    			</a>
			    				</div>
			    			</div>
			    			<div class="inner-card">
						    	<hr>
						    	<div class="link-group ticker">
									<ul>
										<a href="#">
											Selected filters:
										</a>
										<a href="#">
											<li><i class="icon-x"></i> Buy one get one free</li>
										</a>
										<a href="#">
											<li>Clear all</li>
										</a>
									</ul>
								</div>
						    </div>
			    		</div>
			    	</div>
			    	<div class="tab-pane fade in" id="brands">
			    		<div class="card secondary">
			    			<div class="carousel carousel-list carousel-gallery">
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter selected"></div>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    				</div>
					    			</a>
			    				</div>
			    			</div>
			    			<div class="inner-card">
						    	<hr>
						    	<div class="link-group ticker">
									<ul>
										<a href="#">
											Selected filters:
										</a>
										<a href="#">
											<li><i class="icon-x"></i> Buy one get one free</li>
										</a>
										<a href="#">
											<li>Clear all</li>
										</a>
									</ul>
								</div>
						    </div>
			    		</div>
			    	</div>
			    	<div class="tab-pane fade in" id="colours">
			    		<div class="card secondary">
			    			<div class="carousel carousel-list carousel-gallery">
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter selected"></div>
					    					<h4>Black</h4>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    					<h4>Blue</h4>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    					<h4>Gold</h4>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    					<h4>Green</h4>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    					<h4>Grey</h4>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    					<h4>Another <br>colour</h4>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    					<h4>Another <br>colour</h4>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    					<h4>Another <br>colour</h4>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    					<h4>Another <br>colour</h4>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    					<h4>Another <br>colour</h4>
					    				</div>
					    			</a>
			    				</div>
			    			</div>
			    			<div class="inner-card">
						    	<hr>
						    	<div class="link-group ticker">
									<ul>
										<a href="#">
											Selected filters:
										</a>
										<a href="#">
											<li><i class="icon-x"></i> Buy one get one free</li>
										</a>
										<a href="#">
											<li>Clear all</li>
										</a>
									</ul>
								</div>
						    </div>
			    		</div>
			    	</div>
			    	<div class="tab-pane fade in active" id="deals">
			    		<div class="card secondary">
			    			<div class="carousel carousel-list carousel-gallery">
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter selected"></div>
					    					<h4>Buy one <br>get one free</h4>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    					<h4>Reduced <br>to clear</h4>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    					<h4>Latest <br>offers</h4>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    					<h4>Another <br>offer</h4>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    					<h4>Another <br>offer</h4>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    					<h4>Another <br>offer</h4>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    					<h4>Another <br>offer</h4>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    					<h4>Another <br>offer</h4>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    					<h4>Another <br>offer</h4>
					    				</div>
					    			</a>
			    				</div>
			    				<div>
			    					<a href="#">
					  					<div class="carousel-items">
					    					<div class="icon-filter"></div>
					    					<h4>Another <br>offer</h4>
					    				</div>
					    			</a>
			    				</div>
			    			</div>
			    			<div class="inner-card">
						    	<hr>
						    	<div class="link-group ticker">
									<ul>
										<a href="#">
											Selected filters:
										</a>
										<a href="#">
											<li><i class="icon-x"></i> Buy one get one free</li>
										</a>
										<a href="#">
											<li>Clear all</li>
										</a>
									</ul>
								</div>
						    </div>
			    		</div>
			    	</div>
			    </div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="card clearfix transparent">
					<div class="row filter-box">
						<div class="col-xs-8 left-filter">
							<div class="inline-block float-left margin-right">Works with my</div>
							<div class="inline-block float-left">
								<div class="dropdown short">
				                	<input type="hidden" name="accessory-brand" value="">
				                    <button class="dropdown-toggle" type="button" id="accessory-brand" data-toggle="dropdown" aria-expanded="false">
					                    Brand
					                    <span class="caret"></span>
				                    </button>
				                    <ul class="dropdown-menu" role="menu" aria-labelledby="accessory-brand">
				                        <li><a role="menuitem">Samsung</a></li>
				                        <li><a role="menuitem">Other</a></li>
				                    </ul>
				                </div>
							</div>
						</div>
						
						<div class="col-xs-4 right-filter">
							<div class="inline-block float-left">Sort by</div>
							<div class="inline-block float-right">
								<div class="dropdown short">
					                <input type="hidden" name="accessory-price" value="">
					                <button class="dropdown-toggle" type="button" id="accessory-price" data-toggle="dropdown" aria-expanded="false">
					                    -
					                    <span class="caret"></span>
				                    </button>
				                    <ul class="dropdown-menu" role="menu" aria-labelledby="accessory-price">
				                        <li><a role="menuitem">$ - $$$</a></li>
				                        <li><a role="menuitem">$$ - $$$</a></li>
				                    </ul>
				                </div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4 col-sm-6">
				<div class="card primary clearfix device-card purple-white-card can-animate animated small-form">
					<a href="#">
						<div class="clearfix inner-card">
							<div class="header">
								<h4>iPhone 6 PLUS</h4>
								<p>Leather Case</p>
							</div>
							<hr>
							<div class="device-img">
								<img src="images/iphone6-leather-case.png">
							</div>
							<div class="device-info">
								<div class="device-price">
									<div class="price-wrap">
										<span class="number" data-pre="$">49<sup class="decimal">.95</sup>
										</span>
										<div class="prev-price">Was <strike>$59.95</strike></div>
									</div>
								</div>
								<div class="free">
									FREE with <br>any iPhone <br>purchased
								</div>
								<ul class="available-colors">
									<li class="black"></li>
									<li class="red"></li>
									<li class="navy"></li>
									<li class="brown"></li>
									<li class="ivory"></li>
								</ul>
							</div>
						</div>
					</a>
				</div>
			</div>

			<div class="col-md-4 col-sm-6">
				<div class="card primary clearfix device-card purple-white-card can-animate animated small-form">
					<a href="#">
						<div class="clearfix inner-card">
							<div class="header">
								<h4>iPhone 5</h4>
								<p>Car Charger</p>
							</div>
							<hr>
							<div style="top: 136px;" class="device-img">
								<img src="images/iphone5-car-charger.png">
							</div>
							<div class="device-info">
								<div class="device-price">
									<div class="price-wrap">
										<span class="number" data-pre="$">49<sup class="decimal">.95</sup>
										</span>
										<div class="prev-price">Was <strike>$59.95</strike></div>
									</div>
								</div>
								<div class="free">
									FREE with <br>any iPhone <br>purchased
								</div>
								<ul class="available-colors">
									<li class="black"></li>
									<li class="red"></li>
								</ul>
							</div>
						</div>
					</a>
				</div>
			</div>

			<div class="col-md-4 col-sm-6">
				<div class="card primary clearfix device-card purple-white-card can-animate animated small-form">
					<a href="#">
						<div class="clearfix inner-card">
							<div class="header">
								<h4>BEATS Solo 2</h4>
								<p>On Ear Headphones</p>
							</div>
							<hr>
							<div class="device-img">
								<img src="images/beats-headphone.png">
							</div>
							<div class="device-info">
								<div class="device-price">
									<div class="price-wrap">
										<span class="number" data-pre="$">249<sup class="decimal">.95</sup>
										</span>
									</div>
								</div>
								<ul class="available-colors">
									<li class="black"></li>
									<li class="pink"></li>
									<li class="blue"></li>
									<li class="purple"></li>
									<li class="redwine"></li>
								</ul>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4 col-sm-6">
				<div class="card primary clearfix device-card purple-white-card can-animate animated small-form">
					<a href="#">
						<div class="clearfix inner-card">
							<div class="header">
								<h4>LEXAR 8GB</h4>
								<p>Micro SD Card</p>
							</div>
							<hr>
							<div style="top: 140px;" class="device-img">
								<img src="images/lexar-8gb.png">
							</div>
							<div class="device-info">
								<div class="device-price">
									<div class="price-wrap">
										<span class="number" data-pre="$">29<sup class="decimal">.95</sup>
										</span>
									</div>
								</div>
								<ul class="available-colors">
									<li class="black"></li>
								</ul>
							</div>
						</div>
					</a>
				</div>
			</div>

			<div class="col-md-4 col-sm-6">
				<div class="card primary clearfix device-card purple-white-card can-animate animated small-form">
					<a href="#">
						<div class="clearfix inner-card">
							<div class="header">
								<h4>iPhone 5</h4>
								<p>Silicone Case</p>
							</div>
							<hr>
							<div class="device-img">
								<img src="images/iphone5-silicone-case.png">
							</div>
							<div class="device-info">
								<div class="device-price">
									<div class="price-wrap">
										<span class="number" data-pre="$">39<sup class="decimal">.95</sup>
										</span>
									</div>
								</div>
								<ul class="available-colors">
									<li class="black"></li>
									<li class="red"></li>
									<li class="blue"></li>
									<li class="pink"></li>
									<li class="white"></li>
								</ul>
							</div>
						</div>
					</a>
				</div>
			</div>

			<div class="col-md-4 col-sm-6">
				<div class="card primary clearfix device-card purple-white-card can-animate animated small-form">
					<a href="#">
						<div class="sash">
							<img src="images/sash-device.png" alt="">
						</div>
						<div class="clearfix inner-card">
							<div class="header">
								<h4>iPhone 5</h4>
								<p>Leather Case</p>
							</div>
							<hr>
							<div class="device-img">
								<img src="images/iphone5-leather-case.png">
							</div>
							<div class="device-info">
								<div class="device-price">
									<div class="price-wrap">
										<span class="number" data-pre="$">39<sup class="decimal">.95</sup>
										</span>
									</div>
								</div>
								<ul class="available-colors">
									<li class="black"></li>
									<li class="red"></li>
									<li class="blue"></li>
									<li class="pink"></li>
									<li class="white"></li>
								</ul>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
			<div class="card">
				<div class="blue_bg">
					<div class="col-xs-12 text-center">
						<button type="button" class="normal blue">Load more</button>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$('.carousel-gallery').slick({
			slidesToShow: 5,
		  	infinite: false,
		  	slidesToScroll: 5,
		  	responsive: [
			    {
			      breakpoint: 1024,
			      settings: {
			        slidesToShow: 4,
			        slidesToScroll: 4,
			        infinite: true,
			        dots: true
			      }
			    },
			    {
			      breakpoint: 600,
			      settings: {
			        slidesToShow: 3,
			        slidesToScroll: 3
			      }
			    },
			    {
			      breakpoint: 480,
			      settings: {
			        slidesToShow: 2,
			        slidesToScroll: 2
			      }
			    }
			]
		});
	</script>

<?php include("../../includes/footer.php"); ?>