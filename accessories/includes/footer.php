<footer>
    <!-- ///// Mobile Footer ///// -->

    <div class="mobile-footer visible-xs">

        
        <div class="social-links">
            
            <a href="http://www.facebook.com/spark4nz"><i class="icon-facebook"></i></a>
            
            <a href="http://twitter.com/sparknz"><i class="icon-twitter"></i></a>
            
            <a href="http://www.youtube.com/user/sparknewzealand"><i class="icon-youtube"></i></a>
            
        </div>
        
        
        <div class="links">
            
        </div>
        
        <div class="copyright">&copy; Copyright Spark 2015 All rights reserved</div>
    </div>
    <!-- Mobile footer END -->


    <!-- ///// Desktop Footer | same structure as  CQ ///// -->

    <div class="desktop-nav hidden-xs">
        <div id="footer">
            <div class="footerchannel">
                <div id="footer_channel">
                    <!-- Popup Modal -->
                    <div id="itpg" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                
                        <a type="button" class="close btn-mobile" data-dismiss="modal"><i class="icon-cancel-circle blue-text"></i></a>
                
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <a type="button" class="close btn-desk" data-dismiss="modal"><i class="icon-cancel-circle"></i></a>
                
                                <div class="modal-body">
                
                                    <h2 class="block pink title"><span>Help us improve this page</span></h2>
                
                                    <form name="itpForm" action="">
                                        <div id="itpgcont">
                                            <h4>How could we make this page better?</h4>
                                            <p id="itpMax">1000 characters</p>
                                            <textarea onkeyup="itpCounter()" name="itpMsg" cols="65" rows="7" id="itpMsg" placeholder="Please share your thoughts here..."></textarea>
                
                                            Cancel
                                            <a href="#" class="button blue slim float-right btn-send">send message</a>
                                        </div>
                                    </form>
                
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /End Popup Modal -->
                
                    <div id="mask"></div>
                    <div class="help">
                        <a id="itpLink" href="http://www.telecom.co.nz/rating/na/" data-toggle="modal" data-target="#itpg"><i class="icon-chat"></i>Help improve this page</a>
                        <a href="javascript:window.print()"><i class="icon-doc-text-inv"></i>Print this page</a>
                        <a onclick="emailPage()" href="#" rel="popup"><i class="icon-mail-alt"></i>Email this page</a>
                    </div>
                    <a href="#top" class="top"><i class="icon-angle-circled-up"></i> Back to top</a> 
                </div>
            </div>
            <div class="footerbreadcrumb">
                <div class="fullwidth-bg">
                    <ul class="breadcrumb">
                        
                        <li><a href="/home/">  Home</a>
                            <div class="arrow"></div>
                        </li>
                        
                    </ul>
                </div>
            </div>
            <div class="fullwidth-bg off-white">
                <div class="nav">
                    <div class="footernav">
                
                    
                        
                        <div class="col">
                            
                            <h5> <a href="/shop/">Shop </a> </h5>
                            
                            
                            <ul>
                                
                                <li> <a href="/shop/mobile.html"> Mobile  </a> </li>
                                
                                <li> <a href="/shop/internet/"> Internet </a> </li>
                                
                                <li> <a href="/shop/landline/"> Landline </a> </li>
                                
                            </ul>
                            
                        </div>
                        
                    
                    
                
                
                    
                        
                        <div class="col">
                            
                            <h5> <a href="/discover/">Discover</a> </h5>
                            
                            
                            <ul>
                                
                                <li> <a href="/discover/samsungs6/"> Samsung Galaxy S6 </a> </li>
                                
                                <li> <a href="/discover/ultrabroadband/"> Ultra Broadband </a> </li>
                                
                                <li> <a href="/discover/ultramobile/"> Ultra Mobile  </a> </li>
                                
                                <li> <a href="/discover/spotify/"> Spotify Premium  </a> </li>
                                
                                <li> <a href="/discover/lightbox/"> Lightbox </a> </li>
                                
                            </ul>
                            
                        </div>
                        
                    
                    
                
                
                    
                        
                        <div class="col">
                            
                            <h5> <a href="/help/">Help</a> </h5>
                            
                            
                            <ul>
                                
                                <li> <a href="/help/techinasec/"> Tech in a Sec  </a> </li>
                                
                                <li> <a href="/help/internet-email/"> Internet  </a> </li>
                                
                                <li> <a href="/help/mobile-data/"> Mobile  </a> </li>
                                
                                <li> <a href="/help/landline/"> Landline  </a> </li>
                                
                                <li> <a href="/help/billing/"> Billing  </a> </li>
                                
                            </ul>
                            
                        </div>
                        
                    
                    
                
                
                    
                        
                        <div class="col">
                            
                            <h5> <a href="/myspark/">MySpark</a> </h5>
                            
                            
                            <ul>
                                
                                <li> <a href="/myspark/access/login"> Sign in  </a> </li>
                                
                                <li> <a href="/myspark/access/login"> Register  </a> </li>
                                
                                <li> <a href="/myspark/mymobile/"> My Mobile  </a> </li>
                                
                                <li> <a href="/myspark/mymobilebroadband/"> My Mobile Broadband  </a> </li>
                                
                                <li> <a href="/myspark/myinternet/"> My Internet  </a> </li>
                                
                                <li> <a href="/myspark/mylandline/"> My Landline  </a> </li>
                                
                            </ul>
                            
                        </div>
                        
                    
                    
                
                    </div>
                    <div class="socialmedialinks">
                        <div class="col contact">
                    
                        
                            
                            <h5> <a href="/contactus/">Contact</a> </h5>
                            
                            
                            <ul>
                                
                                <li> <a href="/contactus/storefinder/"> Store Finder </a> </li>
                                
                            </ul>
                            
                        
                        
                    
                            
                            <div class="footer-social">
                                <h5>Follow us</h5>
                                <ul>
                                    
                                    <li class="social"><a href="http://www.facebook.com/spark4nz"><i class="icon-facebook"></i></a> </li>
                                    
                                    <li class="social"><a href="http://twitter.com/sparknz"><i class="icon-twitter"></i></a> </li>
                                    
                                    <li class="social"><a href="http://www.youtube.com/user/sparknewzealand"><i class="icon-youtube"></i></a> </li>
                                    
                                </ul>
                            </div>
                            
                        </div>
                    </div>
                    <div class="bottomlinks">
                        <div class="bottom">
                            <div class="wrap">
                                <ul>
                                    
                                    <li> 
                                        
                                        <a href="http://www.sparknz.co.nz/"> Spark New Zealand   </a> 
                                    </li>
                                    
                                    <li> 
                                         &nbsp;|&nbsp; 
                                        <a href="http://www.sparkdigital.co.nz/"> Spark Digital </a> 
                                    </li>
                                    
                                    <li> 
                                         &nbsp;|&nbsp; 
                                        <a href="http://www.sparkventures.co.nz/"> Spark Ventures </a> 
                                    </li>
                                    
                                    <li> 
                                         &nbsp;|&nbsp; 
                                        <a href="http://www.sparkfoundation.org.nz/"> Spark Foundation </a> 
                                    </li>
                                    
                                </ul>
                                <div class="copyright">&copy; Copyright Spark 2015 All rights reserved</div>
                                <ul class="legal">
                                    
                                    
                                    <li> 
                                        
                                        <a href="/help/other/legaldisclaimer/"> Legal disclaimer </a> 
                                    </li>
                                    
                                    <li> 
                                         &nbsp;|&nbsp; 
                                        <a href="/help/other/privacypolicy/"> Privacy Policy </a> 
                                    </li>
                                    
                                    <li> 
                                         &nbsp;|&nbsp; 
                                        <a href="/help/other/accessibilities/"> Accessibility </a> 
                                    </li>
                                    
                                    <li> 
                                         &nbsp;|&nbsp; 
                                        <a href="/help/other/terms/"> Terms and Conditions </a> 
                                    </li>
                                    
                                </ul>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- Desktop footer END -->

</footer>
    
   
    </body>
</html>