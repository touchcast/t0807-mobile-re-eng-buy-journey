<?php include("../../includes/header.php"); ?>
	
	<div class="page-header">
		<div class="center-wrap">
			<h4><a href="#" class="show-xs back-btn"><i class="icon-left-open-big"></i> Back button</a></h4>
			<h1>Accessories</h1>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-md-8">
				<div class="card primary">
					<div class="carousel carousel-prod-img">
					  <div><img src="http://lorempixel.com/350/350/abstract"></div>
					  <div><img src="http://lorempixel.com/350/350/animals"></div>
					  <div><img src="http://lorempixel.com/350/350/business"></div>
					  <div><img src="http://lorempixel.com/350/350/cats"></div>
					</div>
					<div class="carousel carousel-prod-nav">
					  <div><img src="http://lorempixel.com/350/350/abstract"></div>
					  <div><img src="http://lorempixel.com/350/350/animals"></div>
					  <div><img src="http://lorempixel.com/350/350/business"></div>
					  <div><img src="http://lorempixel.com/350/350/cats"></div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-4">
				<div class="card primary">
					<div class="content-block-header">
						<h1 class="block">
							<span>Beats solo 2<br>on-ear headphone<br>by beats</span>
						</h1>
					</div>
					<div class="inner-card">
						<div class="device-price device-price-shade">
                            <span class="number" data-pre="$">199</span>
                            <span class="decimal">.99</span>
                        </div>
                        <div class="device-colour-options">
                        	<input type="radio" value="radio1" id="radio1" name="radio">
                        	<label for="radio1"><span></span></label>

                        	<input type="radio" value="radio2" id="radio2" name="radio">
                        	<label for="radio2"><span class="pink"></span></label>

                        	<input type="radio" value="radio3" id="radio3" name="radio" checked>
                        	<label for="radio3"><span class="blue"></span></label>
                        	
                        	<input type="radio" value="radio4" id="radio4" name="radio">
                        	<label for="radio4"><span class="purple"></span></label>
                        	
                        	<input type="radio" value="radio5" id="radio5" name="radio">
                        	<label for="radio5"><span class="red"></span></label>
                        </div>
                        <div class="row">
                        	<div class="col-xs-5">Works with my</div>
                        	<div class="col-xs-7">
                        		<div class="dropdown short prefix">
				                	<input type="hidden" name="accessory-brand" value="">
				                    <button class="dropdown-toggle selected" type="button" id="accessory-brand" data-toggle="dropdown" aria-expanded="false">
					                    Samsung
					                    <span class="caret"></span>
				                    </button>
				                    <ul class="dropdown-menu" role="menu" aria-labelledby="accessory-brand">
				                        <li><a role="menuitem">Samsung</a></li>
				                        <li><a role="menuitem">Other</a></li>
				                    </ul>
				                </div>
				                <div class="dropdown short prefix">
					                <input type="hidden" name="accessory-model" value="">
					                <button class="dropdown-toggle" type="button" id="accessory-model" data-toggle="dropdown" aria-expanded="false">
					                    Model
					                    <span class="caret"></span>
				                    </button>
				                    <ul class="dropdown-menu" role="menu" aria-labelledby="accessory-model">
				                        <li><a role="menuitem">A</a></li>
				                        <li><a role="menuitem">B</a></li>
				                    </ul>
				                </div>
                        	</div>
                        </div>
					</div>
					<div class="blue_bg">
						<div class="col-xs-12 text-center">
							<button type="button" class="blue secondary normal">Add to cart</button>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<div class="card secondary">
					<div class="inner-card">
						<h2>Overview</h2>
						<hr class="visible-xs">
						<p>
							The Solo 2 has arrived. Beats' most popular headphone has been redesigned from the inside out. With updated and improved acoustics, the Solo 2 lets you feel your music with a wider range of sound and enhanced clarity. Streamlined, lightweight, and durable, this compact headphone is more comfortable than ever. Take your music with you wherever you go, with the Solo 2.
						</p>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<div class="card secondary">
					
					<ul id="myResponsiveTab" class="nav nav-tabs nav-justified responsive-tab">
				        <li class="active">
				        	<a href="#home1" data-toggle="tab">
					        	<h4>
						        	<span class="visible-xs">Key features</span>
						        	<span class="visible-sm">Key features</span>
						        	<span class="visible-md visible-lg">Key features</span>
					        	</h4>
				        	</a>
				        </li>

				        <li>
				        	<a href="#profile1" data-toggle="tab">
				        		<h4>
				        			<span class="visible-xs">Compatibility</span>
				        			<span class="visible-sm">Compatibility</span>
				        			<span class="visible-md visible-lg">Compatibility</span>
				        		</h4>
				        	</a>
				        </li>
				        
				     	 <li>
				     	 	<a href="#coffee1" data-toggle="tab">
				     	 		<h4>
				     	 			<span class="visible-xs">Tech details</span>
				     	 			<span class="visible-sm">Tech details</span>
				     	 			<span class="visible-md visible-lg">Tech details</span>
				     	 		</h4>
				     	 	</a>
				     	 </li>
				    </ul>

				    <div id="detailsTabContent" class="tab-content">
				        <div class="tab-pane fade in active" id="home1">
				            <div class="inner-card content-card right clearfix">
								<div class="cb-image">
									<img src="http://lorempixel.com/300/300/sports/" alt="content image">
								</div>
								<div class="inner-wrapper">	
									<h3>Feel the music</h3>
									<p>Immerse yourself in an emotional sound experience. The Solo 2 has a more dynamic and wider range of sound, with a clarity that will bring you closer to what the artist intended you to hear. Regardless of what kind of music you're into, you will feel the higher fidelity sound in your Solo 2.</p>
								</div>
							</div>
							<hr>
							<div class="inner-card content-card left clearfix">
								<div class="cb-image">
									<img src="http://lorempixel.com/300/300/sports/" alt="content image">
								</div>
								<div class="inner-wrapper">	
									<h3>Engineered for comfort</h3>
									<p>Starting at the center of the flexible headband, the frame of the headphone has been curved like never before, giving the Solo 2 a custom-fit feeling. The earcups have been ergonomically angled to complete this natural fit, with pivots for optimal comfort and sound delivery. Finally, the earcups' premium material helps dissipate heat and minimize sound leakage.</p>
								</div>
							</div>
							<hr>
							<div class="inner-card content-card right clearfix">
								<div class="cb-image">
									<img src="http://lorempixel.com/300/300/sports/" alt="content image">
								</div>
								<div class="inner-wrapper">	
									<h3>Beauty and brawn</h3>
									<p>The Solo 2 boasts a streamlined aesthetic with fast flowing curves and no visible screws. Disciplined decisions in engineering and material selection have created a more durable headphone that is equipped for extended use. Easily foldable, this premium headphone is ready for your life on the go.</p>
								</div>
							</div>
							<hr>
							<div class="inner-card content-card left clearfix">
								<div class="cb-image">
									<img src="http://lorempixel.com/300/300/sports/" alt="content image">
								</div>
								<div class="inner-wrapper">	
									<h3>Take control</h3>
									<p>With one color-matched RemoteTalk&#8482; cable, you can change songs, adjust the volume and even take calls, without having to reach for your device. (Compatible with iOS devices. Functionality may vary by device)</p>
								</div>
							</div>
				        </div>
				        <div class="tab-pane fade" id="profile1">
				            <div class="inner-card content-card right clearfix">
								<div class="cb-image">
									<img src="http://lorempixel.com/300/300/business/" alt="content image">
								</div>
								<div class="inner-wrapper">	
									<h3>Sub header</h3>
									<p>Paragraph text - Omnimagnat dolorep erciandae andigendiae sus, sunt utemodi ssequas consecusam. Omnimagnat dolorep erciandae andigendiae sus, sunt utemodi ssequas consecusam.</p>
									<p>Paragraph text - Omnimagnat dolorep erciandae andigendiae sus, sunt utemodi ssequas consecusam. Omnimagnat dolorep erciandae andigendiae sus, sunt utemodi ssequas consecusam.</p>
								</div>
							</div>
							<hr>
							<div class="inner-card content-card left clearfix">
								<div class="cb-image">
									<img src="http://lorempixel.com/300/300/business/" alt="content image">
								</div>
								<div class="inner-wrapper">	
									<h3>Beauty and brawn</h3>
									<p>The Solo 2 boasts a streamlined aesthetic with fast flowing curves and no visible screws. Disciplined decisions in engineering and material selection have created a more durable headphone that is equipped for extended use. Easily foldable, this premium headphone is ready for your life on the go. </p>
								</div>
							</div>
				        </div>
				        <div class="tab-pane fade" id="coffee1">
				            <div class="inner-card content-card right clearfix">
								<div class="cb-image">
									<img src="http://lorempixel.com/300/300/business/" alt="content image">
								</div>
								<div class="inner-wrapper">	
									<h3>Sub header</h3>
									<p>Paragraph text - Omnimagnat dolorep erciandae andigendiae sus, sunt utemodi ssequas consecusam. Omnimagnat dolorep erciandae andigendiae sus, sunt utemodi ssequas consecusam.</p>
									<p>Paragraph text - Omnimagnat dolorep erciandae andigendiae sus, sunt utemodi ssequas consecusam. Omnimagnat dolorep erciandae andigendiae sus, sunt utemodi ssequas consecusam.</p>
								</div>
							</div>
							<hr>
							<div class="inner-card content-card left clearfix">
								<div class="cb-image">
									<img src="http://lorempixel.com/300/300/business/" alt="content image">
								</div>
								<div class="inner-wrapper">	
									<h3>Beauty and brawn</h3>
									<p>The Solo 2 boasts a streamlined aesthetic with fast flowing curves and no visible screws. Disciplined decisions in engineering and material selection have created a more durable headphone that is equipped for extended use. Easily foldable, this premium headphone is ready for your life on the go. </p>
								</div>
							</div>
				        </div>
				    </div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<div class="card secondary">
					<div class="inner-card carousel-header">
						<h2>Other stuff you might like</h2>
						<hr class="visible-xs">
					</div>
					<div class="carousel carousel-list">
					  <div>
					  	<a href="#">
					  		<div class="carousel-items">
					  			<div class="header">
					  				<h4 class="title">iPhone 6 PLUS</h4>
					  				<p>LEATHER CASE</p>
					  			</div>
					  			<hr>
					  			<img class="device-img" src="http://lorempixel.com/130/130/technics">
					  			<p>$49.95</p>
					  			<button type="button" class="link">View details</button>
					  		</div>
					  	</a>
					  </div>
					  <div>
					  	<a href="#">
					  		<div class="carousel-items">
					  			<div class="header">
					  				<h4 class="title">iPhone 6</h4>
					  				<p>Silicon case</p>
					  			</div>
					  			<hr>
					  			<img class="device-img" src="http://lorempixel.com/130/130/nightlife">
					  			<p>$24.99</p>
					  			<button type="button" class="link">View details</button>
					  		</div>
					  	</a>
					  </div>
					  <div>
					  	<a href="#">
					  		<div class="carousel-items">
					  			<div class="header">
					  				<h4 class="title">iPad Air 2</h4>
					  				<p>Smart Case</p>
					  			</div>
					  			<hr>
					  			<img class="device-img" src="http://lorempixel.com/130/130/fashion">
					  			<p>$74.99</p>
					  			<button type="button" class="link">View details</button>
					  		</div>
					  	</a>
					  </div>
					  <div>
					  	<a href="#">
					  		<div class="carousel-items">
					  			<div class="header">
					  				<h4 class="title">iPhone 6</h4>
					  				<p>SILICON CASE</p>
					  			</div>
					  			<hr>
					  			<img class="device-img" src="http://lorempixel.com/130/130/people">
					  			<p>$24.99</p>
					  			<button type="button" class="link">View details</button>
					  		</div>
					  	</a>
					  </div>
					  <div>
					  	<a href="#">
					  		<div class="carousel-items">
					  			<div class="header">
					  				<h4 class="title">Title</h4>
					  				<p>Name</p>
					  			</div>
					  			<hr>
					  			<img class="device-img" src="http://lorempixel.com/130/130/nature">
					  			<p>$00.00</p>
					  			<button type="button" class="link">View details</button>
					  		</div>
					  	</a>
					  </div>
					  <div>
					  	<a href="#">
					  		<div class="carousel-items">
					  			<div class="header">
					  				<h4 class="title">Title</h4>
					  				<p>Name</p>
					  			</div>
					  			<hr>
					  			<img class="device-img" src="http://lorempixel.com/130/130/sports">
					  			<p>$00.00</p>
					  			<button type="button" class="link">View details</button>
					  		</div>
					  	</a>
					  </div>
					  <div>
					  	<a href="#">
					  		<div class="carousel-items">
					  			<div class="header">
					  				<h4 class="title">Title</h4>
					  				<p>Name</p>
					  			</div>
					  			<hr>
					  			<img class="device-img" src="http://lorempixel.com/130/130/technics">
					  			<p>$00.00</p>
					  			<button type="button" class="link">View details</button>
					  		</div>
					  	</a>
					  </div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$('.carousel-prod-img').slick({
		  arrows: true,
		  asNavFor: '.carousel-prod-nav',
		  fade: true,
		  infinite: false,
		  slidesToScroll: 1,
		  slidesToShow: 1
		});

		/*
		 * slidesToScroll, and slidesToShow should have equal value and
		 * reflect the total number of images (4 in this example).
		 */
		$('.carousel-prod-nav').slick({
		  arrows: false,
		  asNavFor: '.carousel-prod-img',
		  focusOnSelect: true,
		  slidesToScroll: 4,
		  slidesToShow: 4
		});
		$('.carousel-list').slick({
			slidesToShow: 4,
		  	infinite: false,
		  	slidesToScroll: 4
		});
	</script>

<?php include("../../includes/footer.php"); ?>