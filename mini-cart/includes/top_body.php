 <div data-tracking=
    "{event:'PageView','values':{'pageName':'telecom:personal:shop:mobile:phones:samsung-galaxy-s6-edge-32gb-black-sapphire','page.url':'/shop/mobile/phones/samsung-galaxy-s6-edge-32gb-black-sapphire.html'}, componentPath:'tnz/pages/devicedetailpage'}">
    <div class="parbase clientcontext"></div><!-- Defect # 6490 Start-->
        <!--googleoff: index-->

        <div id="header">
            <!-- EOL Test -->
            <!-- EOL Test -->

            <div class="ui-helper-clearfix" id="header-nav-wrap">
                <div class="logo">
                    <a class="current-parent" href="/home/" id=
                    "top-logo"><img src=
                    "http://www.spark.co.nz/etc/designs/telecomcms/resources/images/Spark-Header-Logo1.png"></a>
                </div>

                <div id="navigation">
                    <div class="divisions">
                        <div class="toprightlinks">
                            <nav style="">
                                <ul>
                                    <li>
                                        <a href=
                                        "http://www.sparknz.co.nz/">Spark New
                                        Zealand</a>
                                    </li>

                                    <li>
                                        <a href=
                                        "http://www.sparkdigital.co.nz">Spark
                                        Digital</a>
                                    </li>

                                    <li>
                                        <a href=
                                        "http://www.sparkventures.co.nz/">Spark
                                        Ventures</a>
                                    </li>

                                    <li>
                                        <a href=
                                        "http://www.sparkfoundation.org.nz/">Spark
                                        Foundation</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>

                    <div class="headertabs">
                        <nav class="type">
                            <ul>
                                <li class="current-parent menu-top-item active"
                                data-topmenu_id="1" id="menu-top-item1">
                                    <a class="current-parent" href=
                                    "/home/">Personal</a>
                                </li>

                                <li class="current-parent menu-top-item"
                                data-topmenu_id="2" id="menu-top-item2">
                                    <a class="current-parent" href=
                                    "/business/">Business</a>
                                </li>
                            </ul>
                        </nav>
                    </div>

                    <div class="parbase globalnavigationbar">
                        <nav class="primary show">
                            <ul class="primary-wrap">
                                <li class="current active">
                                    <a class="current" href="/shop/">Shop</a>

                                    <ul class="products">
                                        <li style=
                                        "list-style: none; display: inline">
                                            <div class="arrow-down"></div>
                                        </li>

                                        <li>
                                            <a class="icon circle ie-rounded"
                                            href="/shop/mobile.html"><img alt=
                                            "mobile-icon.png" src=
                                            "http://www.spark.co.nz/etc/designs/telecomcms/resources/assets/Uploads/mobile-icon.png">

                                            <p>Mobile</p></a>

                                            <ul class="popular">
                                                <li>
                                                    <a href=
                                                    "/shop/mobile/phones.html">View
                                                    phones</a>
                                                </li>

                                                <li>
                                                    <a href=
                                                    "/shop/mobile/mobile/plansandpricing.html">
                                                    Plans &amp; pricing</a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li>
                                            <a class="icon circle ie-rounded"
                                            href="/shop/internet/"><img alt=
                                            "cloud-icon.png" src=
                                            "http://www.spark.co.nz/etc/designs/telecomcms/resources/assets/Uploads/cloud-icon.png">

                                            <p>Internet</p></a>

                                            <ul class="popular">
                                                <li>
                                                    <a href=
                                                    "/shop/internet/datacalculator/">
                                                    Which broadband?</a>
                                                </li>

                                                <li>
                                                    <a href=
                                                    "/shop/internet/">Plans
                                                    &amp; pricing</a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li>
                                            <a class="icon circle ie-rounded"
                                            href="/shop/landline/"><img alt=
                                            "phoneicon.png" src=
                                            "http://www.spark.co.nz/content/dam/telecomcms/icons/phoneicon.png">

                                            <p>Landline</p></a>

                                            <ul class="popular">
                                                <li>
                                                    <a href=
                                                    "/shop/landline/homephones/">
                                                    Our home phones</a>
                                                </li>

                                                <li>
                                                    <a href=
                                                    "/shop/landline/pricing/">Plans
                                                    &amp; pricing</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>

                                <li class="current">
                                    <a class="current" href=
                                    "/discover/">Discover</a>
                                </li>

                                <li class="current">
                                    <a class="current" href=
                                    "/myspark/">MySpark</a>
                                </li>

                                <li class="current">
                                    <a class="current" href="/help/">Help</a>
                                </li>

                                <li class="search">
                                    <form action=
                                    "http://search.spark.co.nz/search" id=
                                    "site-search" method="get" name=
                                    "site-search">
                                        <div class="input-append">
                                            <!-- Spark Search Vars -->

                                            <div class="hide" id=
                                            "telecom-search-vars">
                                                <input name="site" value=
                                                "spark_all_collection">
                                                <input name="client" value=
                                                "spark_frontend"> <input name=
                                                "output" value="xml_no_dtd">
                                                <input name="proxystylesheet"
                                                value="spark_frontend">
                                                <input name="filter" value="1">
                                            </div><!-- Search Field -->
                                            <input class="span2" id=
                                            "appendedInputButton" name="q"
                                            placeholder="Search topic" type=
                                            "text"> <!-- Search Button -->
                                             <button class="btn icon-search"
                                            style="font-style: italic" type=
                                            "submit"></button>
                                        </div>
                                    </form>
                                </li>

                                <li>
                                    <a href="/contactus/">Contact</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div><!-- end header-nav-wrap -->

            <div class="secondnavigationbar">
               <nav class="secondary show ">
                    <ul>
                        <div class="wrap">
                                <li class="active parent">                                  
                                                        <a href="/shop/mobile.html">Mobile</a>
                                                        <h3><a href="/shop/mobile.html">Mobile</a></h3>
                                                        <span class="secondary-gradient"></span>
                                                            <ul>
                                                                    <li class="">
                                                                            <a href="/shop/mobile/whyultramobile.html">Why Ultra Mobile</a>
                                                                    </li>
                                                                    <li class="active">
                                                                            <a href="/shop/mobile/phones.html">Phones</a>
                                                                    </li>
                                                                    <li class="">
                                                                            <a href="/shop/mobile/mobile/plansandpricing.html">Plans &amp; pricing</a>
                                                                    </li>
                                                                    <li class="">
                                                                            <a href="/shop/mobile/extras.html">Extras</a>
                                                                    </li>
                                                                    <li class="">
                                                                            <a href="/shop/mobile/internationalroaming.html">Roaming</a>
                                                                    </li>
                                                            </ul>
                                </li>
                                <li class=" parent">                                    
                                                        <a href="/shop/internet/">Internet</a>
                                </li>
                                <li class=" parent">                                    
                                                        <a href="/shop/landline/">Landline</a>
                                </li>
                        </div>
                    </ul>
                </nav>
                
                
                

            </div>
        </div><!-- end header -->
        <!--googleon: index-->



        <div class=" off-canvas hide-extras">
            <!-- <script type="text/javascript" src="http://www.spark.co.nz/etc/clientlibs/granite/jquery/noconflict.js"></script> -->
            <noscript>
            <div class="noscriptmsg">
                <h1>"You don't have JavaScript turned on. This website requires
                JavaScript to function properly."</h1><br>
            </div></noscript>

            <div id="page">
                <!-- Filter start -->

                <div class="filter">
                    <!-- Following component is included by DynamicIncludeFilter (path: http://www.spark.co.nz/content/cms/sample/en/store/tnzProducts/mobile/phones/_jcr_content/filter.nocache.html/tnz/components/shop/filter ) -->
                    <!-- id="filterComponent" you find in filterComponent.jsp /-->
                    <!-- Filter start -->

                    <div class="row" id="filter-row">
                        <div class="twelve columns">
                            <ul class="shop-filter clearfix">
                                <li>
                                    <a class="shop-filter-filter" href="#"
                                    title="Filter">Filter <span class=
                                    "shop-filter-arrow"></span></a>

                                    <div class="filter-container">
                                        <form action="#" class="filter-form"
                                        method="get">
                                            <ul class="filter-options">
                                                <li class=
                                                "filter-options_item filter_brand">
                                                <h3>Brand</h3>
                                                <!-- dialog config -->

                                                    <ul class=
                                                    "shop-filter-submenu">
                                                        <li class="submenu">
                                                            <div class=
                                                            "cbwrapper deco">
                                                                <input class=
                                                                "decoformbutton"
                                                                id=
                                                                "iphone_brand"
                                                                name="iPhone"
                                                                type="checkbox"
                                                                value="iphone">
                                                            </div><label for=
                                                            "iphone_brand">iPhone</label>
                                                        </li>

                                                        <li class="submenu">
                                                            <div class=
                                                            "cbwrapper deco">
                                                                <input class=
                                                                "decoformbutton"
                                                                id=
                                                                "blackberry_brand"
                                                                name=
                                                                "Blackberry"
                                                                type="checkbox"
                                                                value=
                                                                "blackberry">
                                                            </div><label for=
                                                            "blackberry_brand">Blackberry</label>
                                                        </li>

                                                        <li class="submenu">
                                                            <div class=
                                                            "cbwrapper deco">
                                                                <input class=
                                                                "decoformbutton"
                                                                id="htc_brand"
                                                                name="HTC"
                                                                type="checkbox"
                                                                value="htc">
                                                            </div><label for=
                                                            "htc_brand">HTC</label>
                                                        </li>

                                                        <li class="submenu">
                                                            <div class=
                                                            "cbwrapper deco">
                                                                <input class=
                                                                "decoformbutton"
                                                                id=
                                                                "huawei_brand"
                                                                name="Huawei"
                                                                type="checkbox"
                                                                value="huawei">
                                                            </div><label for=
                                                            "huawei_brand">Huawei</label>
                                                        </li>

                                                        <li class="submenu">
                                                            <div class=
                                                            "cbwrapper deco">
                                                                <input class=
                                                                "decoformbutton"
                                                                id="lg_brand"
                                                                name="LG" type=
                                                                "checkbox"
                                                                value="lg">
                                                            </div><label for=
                                                            "lg_brand">LG</label>
                                                        </li>

                                                        <li class="submenu">
                                                            <div class=
                                                            "cbwrapper deco">
                                                                <input class=
                                                                "decoformbutton"
                                                                id=
                                                                "nokia_brand"
                                                                name="Nokia"
                                                                type="checkbox"
                                                                value="nokia">
                                                            </div><label for=
                                                            "nokia_brand">Nokia</label>
                                                        </li>

                                                        <li class="submenu">
                                                            <div class=
                                                            "cbwrapper deco">
                                                                <input class=
                                                                "decoformbutton"
                                                                id=
                                                                "samsung_brand"
                                                                name="Samsung"
                                                                type="checkbox"
                                                                value=
                                                                "samsung">
                                                            </div><label for=
                                                            "samsung_brand">Samsung</label>
                                                        </li>

                                                        <li class="submenu">
                                                            <div class=
                                                            "cbwrapper deco">
                                                                <input class=
                                                                "decoformbutton"
                                                                id="sony_brand"
                                                                name="Sony"
                                                                type="checkbox"
                                                                value="sony">
                                                            </div><label for=
                                                            "sony_brand">Sony</label>
                                                        </li>

                                                        <li class="submenu">
                                                            <div class=
                                                            "cbwrapper deco">
                                                                <input class=
                                                                "decoformbutton"
                                                                id=
                                                                "spark_brand"
                                                                name="Spark"
                                                                type="checkbox"
                                                                value="spark">
                                                            </div><label for=
                                                            "spark_brand">Spark</label>
                                                        </li>

                                                        <li class="submenu">
                                                            <div class=
                                                            "cbwrapper deco">
                                                                <input class=
                                                                "decoformbutton"
                                                                id=
                                                                "microsoft_brand"
                                                                name=
                                                                "Microsoft"
                                                                type="checkbox"
                                                                value=
                                                                "microsoft">
                                                            </div><label for=
                                                            "microsoft_brand">Microsoft</label>
                                                        </li>

                                                        <li class="submenu">
                                                            <div class=
                                                            "cbwrapper deco">
                                                                <input class=
                                                                "decoformbutton"
                                                                id=
                                                                "alcatel_brand"
                                                                name="Alcatel"
                                                                type="checkbox"
                                                                value=
                                                                "alcatel">
                                                            </div><label for=
                                                            "alcatel_brand">Alcatel</label>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li class=
                                                "filter-options_item filter_simcard">
                                                <h3>SIM Card</h3>
                                                <!-- dialog config -->

                                                    <ul class=
                                                    "shop-filter-submenu">
                                                        <li class="submenu">
                                                            <div class=
                                                            "cbwrapper deco">
                                                                <input class=
                                                                "decoformbutton"
                                                                id=
                                                                "trio_simcard"
                                                                name="Trio"
                                                                type="checkbox"
                                                                value="trio">
                                                            </div><label for=
                                                            "trio_simcard">Trio</label>
                                                        </li>

                                                        <li class="submenu">
                                                            <div class=
                                                            "cbwrapper deco">
                                                                <input class=
                                                                "decoformbutton"
                                                                id=
                                                                "semble___________(mobile_wallet_sim)_simcard"
                                                                name=
                                                                "Semble (Mobile Wallet SIM)"
                                                                type="checkbox"
                                                                value=
                                                                "semble___________(mobile_wallet_sim)">
                                                            </div><label for=
                                                            "semble___________(mobile_wallet_sim)_simcard">Semble
                                                            (Mobile Wallet<br>
                                                            SIM)</label>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li class=
                                                "filter-options_item filter_price">
                                                <h3>Price</h3>
                                                <!-- dialog config -->

                                                    <ul class=
                                                    "shop-filter-submenu">
                                                        <li class="submenu">
                                                            <div class=
                                                            "cbwrapper deco">
                                                                <input class=
                                                                "decoformbutton"
                                                                id=
                                                                "0_500_price"
                                                                name="0" type=
                                                                "checkbox"
                                                                value="500">
                                                            </div><label for=
                                                            "0_500_price">$0 -
                                                            $500</label>
                                                        </li>

                                                        <li class="submenu">
                                                            <div class=
                                                            "cbwrapper deco">
                                                                <input class=
                                                                "decoformbutton"
                                                                id=
                                                                "501_1000_price"
                                                                name="501"
                                                                type="checkbox"
                                                                value="1000">
                                                            </div><label for=
                                                            "501_1000_price">$501
                                                            - $1000</label>
                                                        </li>

                                                        <li class="submenu">
                                                            <div class=
                                                            "cbwrapper deco">
                                                                <input class=
                                                                "decoformbutton"
                                                                id="1000_price"
                                                                name="1000"
                                                                type="checkbox"
                                                                value="1000">
                                                            </div><label for=
                                                            "1000_price">over
                                                            $1000</label>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li class=
                                                "filter-options_item filter_os">
                                                <h3>OS</h3>
                                                <!-- dialog config -->

                                                    <ul class=
                                                    "shop-filter-submenu">
                                                        <li class="submenu">
                                                            <div class=
                                                            "cbwrapper deco">
                                                                <input class=
                                                                "decoformbutton"
                                                                id=
                                                                "windows_phone_os"
                                                                name=
                                                                "Windows Phone"
                                                                type="checkbox"
                                                                value=
                                                                "windows_phone">
                                                            </div><label for=
                                                            "windows_phone_os">Windows
                                                            Phone</label>
                                                        </li>

                                                        <li class="submenu">
                                                            <div class=
                                                            "cbwrapper deco">
                                                                <input class=
                                                                "decoformbutton"
                                                                id="android_os"
                                                                name="Android"
                                                                type="checkbox"
                                                                value=
                                                                "android">
                                                            </div><label for=
                                                            "android_os">Android</label>
                                                        </li>

                                                        <li class="submenu">
                                                            <div class=
                                                            "cbwrapper deco">
                                                                <input class=
                                                                "decoformbutton"
                                                                id="ios_os"
                                                                name="iOS"
                                                                type="checkbox"
                                                                value="ios">
                                                            </div><label for=
                                                            "ios_os">iOS</label>
                                                        </li>

                                                        <li class="submenu">
                                                            <div class=
                                                            "cbwrapper deco">
                                                                <input class=
                                                                "decoformbutton"
                                                                id=
                                                                "blackberry_os"
                                                                name=
                                                                "Blackberry"
                                                                type="checkbox"
                                                                value=
                                                                "blackberry">
                                                            </div><label for=
                                                            "blackberry_os">Blackberry</label>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li class=
                                                "filter-options_item filter_deals">
                                                <h3>Deals</h3>
                                                <!-- dialog config -->

                                                    <ul class=
                                                    "shop-filter-submenu">
                                                        <li class="submenu">
                                                            <div class=
                                                            "cbwrapper deco">
                                                                <input class=
                                                                "decoformbutton"
                                                                id="all_deals"
                                                                name="All"
                                                                type="checkbox"
                                                                value="all">
                                                            </div><label for=
                                                            "all_deals">All</label>
                                                        </li>

                                                        <li class="submenu">
                                                            <div class=
                                                            "cbwrapper deco">
                                                                <input class=
                                                                "decoformbutton"
                                                                id=
                                                                "prepaid_deals"
                                                                name="Prepaid"
                                                                type="checkbox"
                                                                value=
                                                                "prepaid">
                                                            </div><label for=
                                                            "prepaid_deals">Prepaid</label>
                                                        </li>

                                                        <li class="submenu">
                                                            <div class=
                                                            "cbwrapper deco">
                                                                <input class=
                                                                "decoformbutton"
                                                                id=
                                                                "postpaid_deals"
                                                                name="Postpaid"
                                                                type="checkbox"
                                                                value=
                                                                "postpaid">
                                                            </div><label for=
                                                            "postpaid_deals">Postpaid</label>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>

                                            <ul class="shop-filter-buttons">
                                                <li><button class="view-phones"
                                                id="viewPhonesButton" title=
                                                "View Phones" type="submit"
                                                value="View Phones">View
                                                Phones</button></li>

                                                <li><button class=
                                                "clear-filter" id=
                                                "reset_button" title=
                                                "Clear all Filters" type=
                                                "reset" value=
                                                "Clear all Filters">Clear all
                                                Filters</button></li>
                                            </ul>
                                        </form>
                                    </div>
                                </li>

                                <li>
                                    <a class="lnk_morePricing_cq" data-link=
                                    "http://www.spark.co.nz/content/cms/sample/en/store/tnzProducts/mobile/phones.choosePlan.html"
                                    href="#" title="More Pricing Options">More
                                    Pricing Options</a>
                                </li>

                                <li class="shop-filter-grid">
                                    <a class="shop-filter-gridview" href=
                                    "#">Grid</a>
                                </li>

                                <li class="shop-filter-list">
                                    <a class="shop-filter-listview" href=
                                    "#">List</a>
                                </li>

                                <li class="shop-filter-cart">
                                    <a class="shop-cart-icon" href="#">(1)</a>