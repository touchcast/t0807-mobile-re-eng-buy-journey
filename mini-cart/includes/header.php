<!DOCTYPE html>

<html class="no-js" lang="en">
<head>
    <title>Mini Cart - Mobile Re-Eng Buy Journey</title>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <link href=
    "http://www.spark.co.nz/etc/designs/telecomcms/favicon/favicon_32.png" rel="icon">
    <link href="http://www.spark.co.nz/etc/designs/telecomcms/favicon/favicon_32.png" rel="icon" sizes="32x32">
    <link href="http://www.spark.co.nz/etc/designs/telecomcms/favicon/favicon_48.png" rel="icon" sizes="48x48">
    <link href="http://www.spark.co.nz/etc/designs/telecomcms/favicon/favicon_64.png" rel="icon" sizes="64x64">
    <link href="http://www.spark.co.nz/etc/designs/telecomcms/favicon/favicon_128.png" rel="icon" sizes="128x128">
    <meta content="#FFFFFF" name="msapplication-TileColor">
    <meta content="http://www.spark.co.nz/etc/designs/telecomcms/favicon/favicon_144.png" name="msapplication-TileImage">
    <link href="http://www.spark.co.nz/etc/designs/telecomcms/clientlib_tnz.css" rel="stylesheet" type="text/css">
    <script src="http://www.spark.co.nz/etc/clientlibs/granite/jquery.js" type="text/javascript"></script>
    <script src="http://www.spark.co.nz/etc/clientlibs/granite/utils.js" type="text/javascript"></script>
    <script src="http://www.spark.co.nz/etc/clientlibs/granite/jquery/granite.js" type="text/javascript"></script>
    <script src="http://www.spark.co.nz/etc/clientlibs/foundation/jquery.js" type="text/javascript"></script>
    <script src="http://www.spark.co.nz/etc/designs/telecomcms/clientlib.js" type="text/javascript"></script>
    <link href="http://www.spark.co.nz/content/dam/telecomcms/css/print.css" media="print" rel="stylesheet" type="text/css">
    <!-- Set the viewport width to device width for mobile -->
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <!-- <title>Samsung Galaxy S6 Edge 32GB Black Sapphire</title>
 -->
    <!--  <link rel="shortcut icon" href="https://static.telecom.co.nz/favicon.ico" type="image/x-icon" >
    <link rel="canonical" href=""/> -->
    <link rel="stylesheet" type="text/css" href="https://www.spark.co.nz/etc/designs/telecomcms/clientlib_tnz.css">
 
    <!-- Font addon remove when on production -->
    <link href="../css/remove-on-prod.css" rel="stylesheet" type="text/css">
    <link href="../css/font-addon.css" rel="stylesheet" type="text/css">

    <!-- Custom CSS -->
    <link href="../css/custom-mini-cart.css" rel="stylesheet" type="text/css">
    
    <!-- Custom JS -->
    <script src="http://www.spark.co.nz/etc/designs/tnz/publish.js" type="text/javascript"></script>
    <script src="../js/mini-cart.js" type="text/javascript"></script>
    
    <script>
    $(document).ready(function(){
         // $('.shop-filter-gridview').click();
        $('.shop-filter-gridview').trigger( "click");
    });
if (window.nc === undefined) {
            window.nc = {};
        }
         
        nc.validLanguages = ['de', 'en', 'fr'];
    </script>
</head><!--[if IE 8]>
<body class="ie lt-ie9">
<![endif]-->
<!--[if IE]>
<body class="ie">
<![endif]-->
<!--[if gt IE 8]>
<body class="ie">
<![endif]-->

<body>