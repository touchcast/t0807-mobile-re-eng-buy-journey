<?php require_once ("../includes/header.php"); ?>
    <?php require_once ("../includes/top_body.php"); ?>

                                    <div class="cart-container">
                                        <div class="cart-listing cart-listing-new">
                                            <div class="cart-listing-item">
                                                <h2 class="cart_heading">CART</h2>

                                                <!-- <h3 class="cart-listing-item-title">Phone with SIM & pay monthly plan -->


                                                    <div class="accordion vertical">
                                                            <ul>
                                                                <li>
                                                                    <input id="checkbox-1" name="checkbox-accordion" type=
                                                                    "checkbox"> <label for="checkbox-1"><div class="item_title">
                                                                            Unlimited ADLS<br>
                                                                            ON A 12 MTH TERM
                                                                        </div></label>

                                                                    <div class="item-heading">
                                                                        
                                                                        <div class="main_total">
                                                                            $89.00 upfront
                                                                        </div>
                                                                    </div><!--item-heading-->    

                                                                    <div class="content no-scrollbar">
                                                                        <ul class="cart-listing-item-meta">
                                                                            <li><span class="cart-meta-title">Connection Fee</span>
                                                                            <span class="cart-meta-desc"><b>Free</b></span></li>

                                                                            <li><span class="cart-meta-title">Modem postage</span>
                                                                            <span class="cart-meta-desc"><b>$9.95</b> upfront </span></li>


                                                                            <li><span class="cart-meta-title">Calling add-ons</span>
                                                                            <span class="cart-meta-desc"><b>$10.00</b> /mth</span></li>

                                                                           <li><span class="cart-meta-title">5 x phone filters</span>
                                                                            <span class="cart-meta-desc"><b>$25.00</b> upfront </span></li>

                                                                            <li><span class="cart-meta-title">Wiring & maintenance</span>
                                                                            <span class="cart-meta-desc"><b>$10.00</b> /mth</span></li> 
                                                                        </ul>
                                                                    </div>
                                                                </li>

                                                            </ul>
                                                        </div><!--End Accordion-->
 

                                            </div>

                                            <div class="cart-listing-buttons">
                                               
                                                <div class="clearer"></div>

                                                <div class=
                                                "blue-bg blue-button-under-card">
                                               

                                                    <div class=
                                                    "col-xs-12 text-center">
                                                        <button class= "slim blue" type=
                                                        "button">View
                                                        Cart</button><br>
                                                        <a class=
                                                        "cart-button-close"
                                                        href="#" title=
                                                        "Close"><button class="slim blue secondary normal" type=
                                                        "button">Close</button></a>
                                                    </div>

                                                    <div class=
                                                    "col-xs-12 text-center">
                                                    </div>

                                                    <div class="clearfix">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
<?php require_once ("../includes/bottom_body.php"); ?>
<?php require_once ("../includes/footer.php"); ?>