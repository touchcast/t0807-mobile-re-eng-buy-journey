<?php require_once ("../includes/header.php"); ?>
    <?php require_once ("../includes/top_body.php"); ?>

                                    <div class="cart-container">
                                        <div class="cart-listing">
                                            <div class="cart-listing-item">
                                                <h2 class="cart_heading">CART</h2>

                                                <!-- <h3 class="cart-listing-item-title">Phone with SIM & pay monthly plan -->


                                                    <div class="accordion vertical">
                                                            <ul>
                                                                <li>
                                                                    <input id="checkbox-1" name="checkbox-accordion" type=
                                                                    "checkbox"> <label for="checkbox-1"><div class="item_title">
                                                                            40GB ADSL<br>
                                                                            Open term
                                                                        </div></label>

                                                                    <div class="item-heading">
                                                                        
                                                                        <div class="main_total">
                                                                            $79.00 upfront
                                                                        </div>
                                                                    </div><!--item-heading-->    

                                                                    <div class="content">
                                                                        <ul class="cart-listing-item-meta">
                                                                            <li><span class="cart-meta-title">Connection Fee</span>
                                                                            <span class="cart-meta-desc"><b>$99.00</b> upfront</span></li>

                                                                            <li><span class="cart-meta-title">Modem postage</span>
                                                                            <span class="cart-meta-desc"><b>$9.95</b> upfront</span></li>

                                                                        </ul>
                                                                    </div>
                                                                </li>

                                                                
                                                            </ul>
                                                        </div><!--End Accordion-->
 

                                            </div>

                                            <div class="cart-listing-buttons">

                                                <div class="clearer"></div>

                                                <div class=
                                                "blue-bg blue-button-under-card">
                                                

                                                    <div class=
                                                    "col-xs-12 text-center">
                                                        <button class= "slim blue" type=
                                                        "button">View
                                                        Cart</button><br>
                                                        <a class=
                                                        "cart-button-close"
                                                        href="#" title=
                                                        "Close"><button class="slim blue secondary normal" type=
                                                        "button">Close</button></a>
                                                    </div>

                                                    <div class=
                                                    "col-xs-12 text-center">
                                                    </div>

                                                    <div class="clearfix">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
    <?php require_once ("../includes/bottom_body.php"); ?>
<?php require_once ("../includes/footer.php"); ?>