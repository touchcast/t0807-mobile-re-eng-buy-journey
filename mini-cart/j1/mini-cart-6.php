<?php require_once ("../includes/header.php"); ?>
    <?php require_once ("../includes/top_body.php"); ?>

                                    <div class="cart-container">
                                        <div class="cart-listing">
                                            <div class="cart-listing-item">
                                                <h2 class="cart_heading">CART</h2>

                                                <!-- <h3 class="cart-listing-item-title">Phone with SIM & pay monthly plan -->


                                                    <div class="accordion vertical">
                                                            <ul>
                                                                <li class="no-sub-item">
                                                                    <div class="item_title">
                                                                            Iphone Lightning <br>car charger
                                                                        </div>

                                                                    <div class="item-heading">
                                                                        
                                                                        <div class="main_total">
                                                                            <b>$35.00</b> upfront
                                                                        </div>
                                                                    </div><!--item-heading-->    

                                                                    
                                                                </li>

                                                                <li class="no-sub-item">
                                                                    <div class="item_title">
                                                                        Beats on-ear<br>
                                                                        Headphones
                                                                    </div>
                                                                    <div class="item-heading">
                                                                        
                                                                        <div class="main_total">
                                                                            <b>$300.00</b> upfront
                                                                        </div>
                                                                    </div><!--item-heading-->    

                                                                    <!-- <div class="content">
                                                                        <ul class="cart-listing-item-meta">
                                                                            <li><span class="cart-meta-title">Ultra mobile $59 - 24 months</span>
                                                                            <span class="cart-meta-desc"><b>$10.38</b> /mth<br></span></li>

                                                                            <li><span class="cart-meta-title">$19 roaming data pack</span>
                                                                            <span class="cart-meta-desc"><b>$19.00</b> upfront<br>
                                                                            <b>$19.00</b> /mth</span></li>

                                                                            

                                                                            <li><span class="cart-meta-title">Spark Trio SIM</span>
                                                                            <span class="cart-meta-desc">FREE<br></span></li>


                                                                        </ul>
                                                                    </div> -->
                                                                </li>

                                                                <li class="no-sub-item">
                                                                    <div class="item_title">
                                                                        Iphone plus <br>leather case
                                                                    </div>

                                                                    <div class="item-heading">
                                                                        
                                                                        <div class="main_total">
                                                                            <b>$70.00</b> upfront
                                                                        </div>
                                                                    </div><!--item-heading-->    

                                                                    
                                                                </li>

                                                                
                                                            </ul>
                                                        </div><!--End Accordion-->
 


                                            </div>

                                            <div class="cart-listing-buttons">
                                               
                                                <div class="clearer"></div>

                                                <div class=
                                                "blue-bg blue-button-under-card">
                                               

                                                    <div class=
                                                    "col-xs-12 text-center">
                                                        <button class= "slim blue" type=
                                                        "button">View
                                                        Cart</button><br>
                                                        <a class=
                                                        "cart-button-close"
                                                        href="#" title=
                                                        "Close"><button class="slim blue secondary normal" type=
                                                        "button">Close</button></a>
                                                    </div>

                                                    <div class=
                                                    "col-xs-12 text-center">
                                                    </div>

                                                    <div class="clearfix">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
<?php require_once ("../includes/bottom_body.php"); ?>
<?php require_once ("../includes/footer.php"); ?>